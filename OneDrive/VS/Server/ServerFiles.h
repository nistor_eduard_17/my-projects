#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <fstream>
#include <sstream>
#include <memory>
#include <stack>
#include <bitset>
#include <cstdint>
#include <filesystem>
#include <WS2tcpip.h>
#include <winsock.h>
#include <winsock2.h>
#include <windows.h>
#include <thread>
#include <regex>
#include "Macros.h"
#pragma comment (lib, "ws2_32.lib")
#pragma comment (lib, "ws2_32.lib")

#ifdef WIN32
#define stat _stat
#endif
namespace fs = std::filesystem;

struct SERVER_API pathHash
{
	std::size_t operator () (const fs::path& myfile) const
	{
		auto h1 = std::hash <std::string>{} (myfile.string());
		return h1;
	}
};

class SERVER_API ServerFiles
{
private:
	fs::path m_path;
public:
	ServerFiles() = default;

	ServerFiles operator =(const ServerFiles& file);
	bool operator ==(const ServerFiles& file) const;
	fs::path Get_Path() const;
};