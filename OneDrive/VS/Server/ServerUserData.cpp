#include "ServerUserData.h"

ServerUserData::ServerUserData(std::string m_user_name, std::string m_user_password, std::unordered_set<ServerFiles, hash> m_user_files_path)
	:m_user_name{ m_user_name }, m_user_password{ m_user_password }, m_user_files_path{ m_user_files_path } {}

void ServerUserData::Set_Username(const std::string user_name)
{
	this->m_user_name = user_name;
}

void ServerUserData::Set_UserPassword(const std::string user_password)
{
	this->m_user_password = user_password;
}

void ServerUserData::Set_Files_Data(fs::path user_files)
{
	std::fstream file(user_files.string() + "\\User_Files_Datas.txt", std::ios::out);
	std::stack<fs::path> stack;
	stack.push(user_files);

	while (stack.size())
	{
		fs::path next = stack.top();
		stack.pop();

		for (const auto& entry : fs::directory_iterator(next))
		{
			if (entry.is_directory())
			{
				stack.push(entry.path());
			}
			std::string new_path = entry.path().string();
			m_files_status[new_path] = true;
			new_path.erase(new_path.begin(), new_path.begin() + user_files.string().size() + 1);

			file << new_path << '\n';
			file << Get_Date(entry) << '\n';
			file << Get_Size(entry) << '\n';
		}
	}
	file.close();
}

template <typename TP>
std::time_t to_time_t(TP tp)
{
	using namespace std::chrono;
	auto sctp = time_point_cast<system_clock::duration>(tp - TP::clock::now() + system_clock::now());
	return system_clock::to_time_t(sctp);
}

std::string ServerUserData::Get_Date(fs::path file_path) const
{
	fs::file_time_type file_time = fs::last_write_time(file_path);

	std::time_t tt = to_time_t(file_time);
	std::tm* gmt = std::gmtime(&tt);
	gmt->tm_hour += 2;
	std::stringstream buffer;
	buffer << std::put_time(gmt, "%d %B %Y %H:%M:%S");

	return buffer.str();
}

uint32_t ServerUserData::Get_Size(auto file_path) const
{
	std::string size = static_cast<std::stringstream>(std::stringstream() << file_path.file_size()).str();
	std::stringstream Integer(size);
	uint32_t size_kb;
	Integer >> size_kb;
	size_kb /= 1024;

	return size_kb;
}

void ServerUserData::CreateFilesData(std::ifstream& file)
{
	std::string data, fileName, dateModified, size, element;
	uint16_t totalLength, currentPosition;
	while (std::getline(file, data))
	{
		totalLength = data.length();
		currentPosition = 0;
		for (const auto& character : data)
		{
			element += character;
			currentPosition++;
		}
		if (!fileName.size() && currentPosition == totalLength)
			fileName = element, element.clear();
		else if (std::regex_match(element, std::regex(R"(\d\d (\w)+ \d\d\d\d \d\d:\d\d:\d\d)")) && currentPosition == totalLength)
		{
			dateModified = element;
			element.clear();
		}
		else if (!size.size() && currentPosition == totalLength)
			size = element;

		if (fileName.size() && dateModified.size() && size.size())
		{
			std::stringstream Integer(size);
			uint16_t fileSize;
			Integer >> fileSize;

			m_files_data[fileName].first = dateModified;
			m_files_data[fileName].second = fileSize;
			fileName.clear();
			dateModified.clear();
			size.clear();
		}
	}
}

std::unordered_map<std::string, std::pair<std::string, uint32_t>> ServerUserData::Get_FilesData() const
{
	return m_files_data;
}

void ServerUserData::SetFilesStatus(std::unordered_map<std::string, bool> filesStatus)
{
	this->m_files_status = filesStatus;
}

std::unordered_map<std::string, bool> ServerUserData::Get_FilesStatus() const
{
	return m_files_status;
}

fs::path ServerUserData::Get_Path()
{
	fs::path return_path = "..\\Users_Files";
	for (const auto& entry : fs::directory_iterator(return_path))
	{
		if (entry.path().filename().string() == m_user_name)
		{
			return_path += "\\" + m_user_name;
			return return_path;
		}
	}
	fs::create_directory("..\\Users_Files\\" + m_user_name);
	fs::create_directory("..\\Users_Files\\" + m_user_name + "\\" + "Trash");
	return_path += "\\" + m_user_name;
	Set_Files_Data(return_path);

	return return_path;
}

std::string ServerUserData::Get_Username() const
{
	return m_user_name;
}

std::string ServerUserData::Get_Password() const
{
	return m_user_password;
}

std::unordered_set<ServerFiles, hash> ServerUserData::Get_UserFiles() const
{
	return m_user_files_path;
}