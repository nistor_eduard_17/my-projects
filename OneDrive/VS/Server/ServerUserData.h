#include "ServerFiles.h"

struct SERVER_API hash
{
	std::size_t operator () (const ServerFiles& myfile) const
	{
		auto h1 = std::hash <std::string>{} (myfile.Get_Path().string());
		return h1;
	}
};

class SERVER_API ServerUserData
{
	std::string m_user_name;
	std::string m_user_password;
	std::unordered_set<ServerFiles, hash> m_user_files_path;
	std::unordered_map<std::string, std::pair<std::string, uint32_t>> m_files_data;
	std::unordered_map<std::string, bool> m_files_status;
public:
	ServerUserData() = default;
	ServerUserData(std::string m_user_name, std::string m_user_password, std::unordered_set<ServerFiles, hash> m_user_files_path);

	void Set_Username(const std::string user_name);
	void Set_UserPassword(const std::string user_password);
	void Set_Files_Data(fs::path user_files);
	void CreateFilesData(std::ifstream& file);
	std::string Get_Date(fs::path file_path) const;
	uint32_t Get_Size(auto file_path) const;

	std::unordered_map<std::string, std::pair<std::string, uint32_t>> Get_FilesData() const;
	fs::path Get_Path();
	std::string Get_Username() const;
	std::string Get_Password() const;
	std::unordered_set<ServerFiles, hash> Get_UserFiles() const;
	void SetFilesStatus(std::unordered_map<std::string, bool> filesStatus);
	std::unordered_map<std::string, bool> Get_FilesStatus() const;
	bool isSync(std::string path);
};