#include "ServerFiles.h"

fs::path ServerFiles::Get_Path() const
{
	return m_path;
}

bool ServerFiles::operator==(const ServerFiles& file) const
{
	return file.m_path == this->m_path;
}

ServerFiles ServerFiles::operator=(const ServerFiles& file)
{
	std::cout << file.m_path;
	for (const auto& entry : fs::directory_iterator(this->m_path))
	{
		std::filesystem::remove(entry);
	}
	std::filesystem::copy(file.m_path, this->m_path, fs::copy_options::overwrite_existing | fs::copy_options::recursive);
	return *this;
}
