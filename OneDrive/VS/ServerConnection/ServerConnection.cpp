#include "ServerConnection.h"
#include <thread>
std::ofstream out("server_binary.txt");

ServerConnection::ServerConnection()
{
	ver = MAKEWORD(2, 2);

	int wsOk = WSAStartup(ver, &wsData);
	if (wsOk != 0)
	{
		std::cerr << "Can't Initialize winsock! Quitting" << std::endl;
		return;
	}

	listening = socket(AF_INET, SOCK_STREAM, 0);
	if (listening == INVALID_SOCKET)
	{
		std::cerr << "Can't create a socket! Quitting" << std::endl;
		return;
	}

	hint.sin_family = AF_INET;
	hint.sin_port = htons(54000);
	hint.sin_addr.S_un.S_addr = INADDR_ANY;

	bind(listening, (sockaddr*)&hint, sizeof(hint));

	listen(listening, SOMAXCONN);

	int clientSize = sizeof(client);

	clientSocket = accept(listening, (sockaddr*)&client, &clientSize);

	char host[NI_MAXHOST];
	char service[NI_MAXSERV];

	ZeroMemory(host, NI_MAXHOST);
	ZeroMemory(service, NI_MAXSERV);

	if (getnameinfo((sockaddr*)&client, sizeof(client), host, NI_MAXHOST, service, NI_MAXSERV, 0) == 0)
	{
		std::cout << host << " connected on port " << service << std::endl;
	}
	else
	{
		inet_ntop(AF_INET, &client.sin_addr, host, NI_MAXHOST);
		std::cout << host << " connected on port " << ntohs(client.sin_port) << std::endl;
	}

	closesocket(listening);
}

void ServerConnection::Connection()
{
	char buf[4096];

	while (true)
	{
		ZeroMemory(buf, 4096);
		int bytesReceived = recv(clientSocket, buf, 4096, 0);
		std::string msg = std::string(buf, 0, bytesReceived);

		if (msg == "EMPTY")
		{
			send(clientSocket, "1", 1, 0);
			Empty();
		}
		else if (!m_user_data.Get_Username().size())
			m_user_data.Set_Username(msg);

		if (bytesReceived == SOCKET_ERROR)
		{
			if (bytesReceived == WSA_IO_PENDING) continue;
		}

		if (msg == "Disconnected")
		{
			std::cout << "Disconnected from server.";
		}
		else if (m_user_data.Get_Username() == msg)
		{
			std::cout << m_user_data.Get_Username() << " connected to server" << std::endl;
			fs::path user_files = m_user_data.Get_Path();

			m_user_data.Set_Files_Data(user_files);
			DFS(user_files);

			std::string a = "STOP";
			send(clientSocket, a.c_str(), 4, 0);
		}
		else if (msg == "RENAME")
		{
			Rename();
		}
		else if (msg == "NEW")
		{
			New();
		}
		else if (msg == "COPY")
		{
			Copy();
		}
		else if (msg == "MOVE")
		{
			Move();
		}
		else if (msg == "SYNC")
		{
			Sync();
		}
		else if (msg == "REFRESH")
		{
			Refresh();
		}
		else if (msg == "DELETE")
		{
			Delete();
		}
		else if (msg == "UPLOAD")
		{
			Upload();
		}
		else if (msg != "" && msg != "EMPTY")
		{
			std::cout << m_user_data.Get_Username() << " connected to server" << std::endl;
			fs::path user_files = "..\\Users_Files\\" + msg;

			DFS(user_files);

			std::string a = "STOP";
			send(clientSocket, a.c_str(), 4, 0);
		}
		memset((char*)buf, 0, 4096);
	}
}

void ServerConnection::Receive_Folder(const std::string& cases, fs::path old_path, fs::path new_path) const
{
	if (cases == "NO_EXISTS")
	{
		new_path += "\\" + old_path.filename().string();
	}
	if (cases == "OVERWRITE")
	{
		new_path += "\\" + old_path.filename().string();
		fs::remove_all(new_path);
	}
	fs::create_directory(new_path);

	char buf[4096];
	ZeroMemory(buf, 4096);
	std::unordered_map<std::string, std::pair<std::string, std::string>> client_files;
	int bytesReceived = 0;
	do
	{
		bytesReceived = recv(clientSocket, buf, 4096, 0);
		if (std::string(buf, 0, bytesReceived) == "STOP") break;

		//path
		fs::path check = std::string(buf, 0, bytesReceived);
		memset((char*)buf, 0, 4096);

		if (check.extension().string().size())
		{
			//size zero
			int size = 0;
			bytesReceived = recv(clientSocket, buf, 4096, 0);
			std::stringstream integer(std::string(buf, 0, bytesReceived));
			integer >> size;
			memset((char*)buf, 0, 4096);

			//array zero
			char* buf2 = new char[size]();
			bytesReceived = recv(clientSocket, buf2, size, 0);
			std::string zero = (std::string(buf2, 0, bytesReceived));
			memset((char*)buf2, 0, size);
			size = 0;

			//size info
			bytesReceived = recv(clientSocket, buf, 4096, 0);
			std::stringstream integer2(std::string(buf, 0, bytesReceived));
			integer2 >> size;
			memset((char*)buf, 0, 4096);

			//info
			buf2 = new char[size]();
			bytesReceived = recv(clientSocket, buf2, size, 0);
			std::string data = (std::string(buf2, 0, bytesReceived));
			memset((char*)buf2, 0, size);

			std::cout << "Received " << bytesReceived << " bytes from client.\n";

			client_files.insert({ check.string(), { zero, data} });
		}
		else
		{
			fs::create_directory(new_path.string() + "\\" + check.string());
		}

	} while (bytesReceived > 0);

	for (auto& i : client_files)
	{
		for (auto& j : i.second.second)
		{
			if (j == '0') j = '\0';
		}
		if (i.second.first != " ")
		{
			int j = 1;
			while (j < i.second.first.size())
			{
				std::string pos = "";
				while (j < i.second.first.size() && i.second.first[j] != ' ')
				{
					pos += i.second.first[j];
					j++;
				}
				j++;

				int position = 0;
				std::stringstream integer(pos);
				integer >> position;
				i.second.second[position] = '0';
			}
		}
		std::ofstream new_file(new_path.string() + "\\" + i.first, std::ios::binary);
		new_file << i.second.second;
		new_file.close();
	}
}

void ServerConnection::Receive_File(const std::string& cases, fs::path old_path, fs::path new_path) const
{
	char buf[4096];
	ZeroMemory(buf, 4096);
	int size = 0;

	//size zero
	int bytesReceived = recv(clientSocket, buf, 4096, 0);
	std::stringstream integer(std::string(buf, 0, bytesReceived));
	integer >> size;
	memset((char*)buf, 0, 4096);

	//array zero
	char* buf2 = new char[size]();
	bytesReceived = recv(clientSocket, buf2, size, 0);
	std::string zero = (std::string(buf2, 0, bytesReceived));
	memset((char*)buf2, 0, size);
	size = 0;

	//size info
	bytesReceived = recv(clientSocket, buf, 4096, 0);
	std::stringstream integer2(std::string(buf, 0, bytesReceived));
	integer2 >> size;
	memset((char*)buf, 0, 4096);

	//info
	buf2 = new char[size]();
	bytesReceived = recv(clientSocket, buf2, size, 0);
	std::string data = (std::string(buf2, 0, bytesReceived));
	memset((char*)buf2, 0, size);

	std::cout << "Received " << bytesReceived << " bytes from client.\n";
	for (auto& j : data)
	{
		if (j == '0') j = '\0';
	}
	if (zero != " ")
	{
		int j = 1;
		while (j < zero.size())
		{
			std::string pos = "";
			while (j < zero.size() && zero[j] != ' ')
			{
				pos += zero[j];
				j++;
			}
			j++;

			int position = 0;
			std::stringstream integer(pos);
			integer >> position;
			data[position] = '0';
		}
	}
	if (cases == "OVERWRITE")
	{
		fs::remove_all(new_path.string() + "\\" + old_path.filename().string());
	}
	if (cases == "NO_OVERWRITE")
	{
		std::ofstream new_file(new_path.string(), std::ios::binary);
		new_file << data;
		new_file.close();
	}
	else
	{
		std::ofstream new_file(new_path.string() + "\\" + old_path.filename().string(), std::ios::binary);
		new_file << data;
		new_file.close();
	}
}

void ServerConnection::Upload() const
{
	char buf[4096];
	ZeroMemory(buf, 4096);

	//case
	int bytesReceived = recv(clientSocket, buf, 4096, 0);
	std::string cases = std::string(buf, 0, bytesReceived);
	memset((char*)buf, 0, 4096);

	//old_path
	bytesReceived = recv(clientSocket, buf, 4096, 0);
	fs::path old_path = std::string(buf, 0, bytesReceived);
	memset((char*)buf, 0, 4096);

	//new_path
	bytesReceived = recv(clientSocket, buf, 4096, 0);
	fs::path new_path = "..\\Users_Files\\" + std::string(buf, 0, bytesReceived);

	if (old_path.extension().string().size())
	{
		Receive_File(cases, old_path, new_path);
	}
	else
	{
		Receive_Folder(cases, old_path, new_path);
	}
}

void ServerConnection::Rename() const
{
	char buf[4096];
	ZeroMemory(buf, 4096);
	int bytesReceived = recv(clientSocket, buf, 4096, 0);

	std::string path = std::string(buf, 0, bytesReceived);
	memset((char*)buf, 0, 4096);

	bytesReceived = recv(clientSocket, buf, 4096, 0);
	std::string new_name = std::string(buf, 0, bytesReceived);

	uint16_t count = path.size() - 1;
	while (path[count] != '\\') count--;

	std::string old_path = path;
	path.erase(path.begin() + count, path.end());

	fs::rename("..\\Users_Files\\" + old_path, "..\\Users_Files\\" + path + "\\" + new_name);
}

void ServerConnection::Delete() const
{
	char buf[4096];
	ZeroMemory(buf, 4096);
	int bytesReceived = recv(clientSocket, buf, 4096, 0);

	std::string path = std::string(buf, 0, bytesReceived);
	memset((char*)buf, 0, 4096);

	bytesReceived = recv(clientSocket, buf, 4096, 0);
	std::string name = std::string(buf, 0, bytesReceived);

	memset((char*)buf, 0, 4096);

	bytesReceived = recv(clientSocket, buf, 4096, 0);
	std::string new_name = std::string(buf, 0, bytesReceived);

	if (new_name == "Trash")
	{
		fs::remove_all("..\\Users_Files\\" + path + "\\" + name);
		return;
	}

	fs::path fileName = "..\\Users_Files\\" + path + '\\' + name;
	fs::rename(fileName, "..\\Users_Files\\" + path + "\\" + new_name);
	fs::path from = "..\\Users_Files\\" + path + "\\" + new_name;
	fs::path to = "..\\Users_Files\\" + m_user_data.Get_Username() + "\\Trash";

	if (fileName.extension().string().size())
	{
		fs::copy(from, to, fs::copy_options::recursive);
		fs::remove_all(from);
	}
	else
	{
		to += "\\" + new_name;
		fs::create_directory("..\\Users_Files\\" + m_user_data.Get_Username() + "\\Trash\\" + new_name);
		fs::copy(from, to, fs::copy_options::update_existing | fs::copy_options::recursive);
		fs::remove_all(from);
	}
}

void ServerConnection::Refresh() const
{
	char buf[4096];
	ZeroMemory(buf, 4096);
	int bytesReceived = recv(clientSocket, buf, 4096, 0);

	std::string path = std::string(buf, 0, bytesReceived);

	send(clientSocket, path.c_str(), path.size(), 0);
	Sleep(20);
	path = "..\\Users_Files\\" + path;
	Files(path);
}

void ServerConnection::Files(fs::path file) const
{
	std::ifstream new_file(file, std::ios::binary);
	std::vector<char> buffer(std::istreambuf_iterator<char>(new_file), {});
	std::string zero = "";

	if (!buffer.size()) buffer.push_back(' ');

	std::cout << "Sent " << buffer.size() << " bytes to client.\n";

	for (int j = 0; j < buffer.size(); j++)
	{
		if (buffer[j] == '0') zero += ' ' + std::to_string(j);
		if (buffer[j] == '\0') buffer[j] = '\\0';
	}
	if (!zero.size()) zero = " ";

	buffer.push_back('\0');
	int size = buffer.size() + 1;

	send(clientSocket, std::to_string(zero.size()).c_str(), std::to_string(zero.size()).size(), 0);
	send(clientSocket, zero.c_str(), zero.size(), 0);
	Sleep(50);
	send(clientSocket, std::to_string(size).c_str(), std::to_string(size).size(), 0);
	Sleep(50);
	send(clientSocket, buffer.data(), size, 0);
}

void ServerConnection::DFS(fs::path user_files) const
{
	std::stack<fs::path> stack;
	stack.push(user_files);

	while (stack.size())
	{
		fs::path next = stack.top();
		stack.pop();

		for (const auto& entry : fs::directory_iterator(next))
		{
			std::string new_path = entry.path().string();
			new_path.erase(new_path.begin(), new_path.begin() + user_files.string().size() + 1);

			Sleep(50);
			send(clientSocket, new_path.c_str(), new_path.size(), 0);

			if (entry.is_directory())
				stack.push(entry.path());
			else
				Files(entry.path());
		}
	}
}

void ServerConnection::Empty()
{
	char buf[4096];
	ZeroMemory(buf, 4096);
	int bytesReceived = recv(clientSocket, buf, 4096, 0);
	std::string user = std::string(buf, 0, bytesReceived);
	memset((char*)buf, 0, 4096);
	m_user_data.Set_Username(user);
	fs::remove_all("..\\Users_Files\\" + user);
	fs::create_directory("..\\Users_Files\\" + user);
	fs::create_directory("..\\Users_Files\\" + user + "\\Trash");
	m_user_data.Set_Files_Data("..\\Users_Files\\" + user);
}

void ServerConnection::New()
{
	char buf[4096];
	ZeroMemory(buf, 4096);
	int bytesReceived = recv(clientSocket, buf, 4096, 0);

	std::string path = std::string(buf, 0, bytesReceived);
	memset((char*)buf, 0, 4096);

	fs::path new_file_path = path;

	if (new_file_path.extension() == "")
	{
		fs::create_directory("..\\Users_Files\\" + path);
	}
	else
	{
		std::ofstream new_file("..\\Users_Files\\" + path);
	}
}

void ServerConnection::Copy()
{
	char buf[4096];
	ZeroMemory(buf, 4096);
	int bytesReceived = recv(clientSocket, buf, 4096, 0);


	fs::path file_path = "..\\Users_Files\\" + std::string(buf, 0, bytesReceived);
	memset((char*)buf, 0, 4096);

	bytesReceived = recv(clientSocket, buf, 4096, 0);
	fs::path folderWhereToCopy = "..\\Users_Files\\" + std::string(buf, 0, bytesReceived);

	if (!file_path.has_extension())
	{
		fs::remove_all(folderWhereToCopy / file_path.filename());
		fs::create_directory(file_path);
		fs::copy(file_path, folderWhereToCopy / file_path.filename(), fs::copy_options::update_existing | fs::copy_options::recursive);
	}
	else
	{
		std::string path = folderWhereToCopy.string() + "\\" + file_path.filename().string();
		fs::remove_all(path);
		fs::copy(file_path, folderWhereToCopy, fs::copy_options::update_existing | fs::copy_options::recursive);
	}
}

void ServerConnection::Move()
{
	char buf[4096];
	ZeroMemory(buf, 4096);
	int bytesReceived = recv(clientSocket, buf, 4096, 0);

	fs::path file_path = "..\\Users_Files\\" + std::string(buf, 0, bytesReceived);
	memset((char*)buf, 0, 4096);

	fs::remove_all(file_path);
}

void ServerConnection::Sync()
{
	char buf[4096];
	ZeroMemory(buf, 4096);
	int size = 0;

	int bytesReceived = recv(clientSocket, buf, 4096, 0);
	std::string old_path = std::string(buf, 0, bytesReceived);
	memset((char*)buf, 0, 4096);

	bytesReceived = recv(clientSocket, buf, 4096, 0);
	fs::path new_path = std::string(buf, 0, bytesReceived);
	memset((char*)buf, 0, 4096);

	bytesReceived = recv(clientSocket, buf, 4096, 0);
	std::stringstream integer(std::string(buf, 0, bytesReceived));
	integer >> size;
	memset((char*)buf, 0, 4096);

	char* buf2 = new char[size]();
	bytesReceived = recv(clientSocket, buf2, size, 0);
	std::string zero = (std::string(buf2, 0, bytesReceived));
	memset((char*)buf2, 0, size);
	size = 0;

	bytesReceived = recv(clientSocket, buf, 4096, 0);
	std::stringstream integer2(std::string(buf, 0, bytesReceived));
	integer2 >> size;
	memset((char*)buf, 0, 4096);

	buf2 = new char[size]();
	bytesReceived = recv(clientSocket, buf2, size, 0);
	std::string data = (std::string(buf2, 0, bytesReceived));
	memset((char*)buf2, 0, size);

	std::cout << "Received " << bytesReceived << " bytes from client.\n";
	for (auto& j : data)
	{
		if (j == '0') j = '\0';
	}
	if (zero != " ")
	{
		int j = 1;
		while (j < zero.size())
		{
			std::string pos = "";
			while (j < zero.size() && zero[j] != ' ')
			{
				pos += zero[j];
				j++;
			}
			j++;

			int position = 0;
			std::stringstream integer(pos);
			integer >> position;
			out << position << ' ';
			data[position] = '0';
		}
	}

	fs::path path = old_path;
	fs::remove_all(new_path.string());

	std::ofstream new_file(new_path.string(), std::ios::binary);
	new_file << data;
	new_file.close();
}

ServerConnection::~ServerConnection()
{
	std::cout << "Client disconnected " << std::endl;

	closesocket(clientSocket);
	WSACleanup();
}