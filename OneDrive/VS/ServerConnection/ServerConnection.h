#pragma once
#include "../Server/ServerUserData.h"

class ServerConnection
{
	ServerUserData m_user_data;
	WSADATA wsData;
	WORD ver;
	SOCKET listening, clientSocket;
	sockaddr_in hint, client;

public:
	ServerConnection();
	~ServerConnection();

	void Rename() const;
	void Delete() const;
	void Refresh() const;
	void New();
	void Copy();
	void Move();
	void Sync();
	void Empty();
	void Upload() const;
	void Files(fs::path file) const;
	void DFS(fs::path user_files) const;
	void Connection();
	void Receive_File(const std::string& cases, fs::path old_path, fs::path new_path) const;
	void Receive_Folder(const std::string& cases, fs::path old_path, fs::path new_path) const;
};
