#pragma once
#include "File.h"
#include "ClientConnection.h"
namespace Ui { class OneDrive; }

class Login;

enum class FileType
{
	FILE,
	FOLDER
};

class OneDrive : public QMainWindow
{
	Q_OBJECT
		Login* logout;
	ServerUserData m_user;
	File files;
	ClientConnection* m_client_to_server;

	bool onMagnifyingGlassClicked, mDoubleClicked;
	std::unordered_set<fs::path, pathHash> foundFiles;
	fs::path clipboard;
	QMenu* menu;
	QAction* firstOptionForUpload;
	QAction* secondOptionForUpload;
	QAction* firstOptionForNew;
	QAction* secondOptionForNew;
	std::unordered_map<std::string, bool> m_files_status;
public:
	OneDrive(QWidget* parent = Q_NULLPTR);
	~OneDrive();

	void DropDownMenu(QPushButton*, QMenu*, QAction*, QAction*);
	void UploadNoOverwrite(FileType type, std::string userFile, fs::path search_file);
	void UploadOverwrite(FileType type, std::string userFile, fs::path search_file);
	void UploadSelectedFile(FileType type, QString add_files);
	void Images(QPushButton* push_button, QPixmap image);
	void Generate_Images();
	void Generate_Files(std::string user);
	void PrintFiles();
	void CreateDocument();
	void CreateFolder();
	void CopyAction(std::string objectName);
	void DeleteFromTrash();
	void MoveToTrash();
	void DownloadAction();
	void DownloadFileAlreadyExist(const auto& file, fs::path file_path, std::string extension, QString pathToDownload);
	void DownloadExistingFiles();
	void UploadLocalFile();
	void ConnectUserToServer(const std::string& message);
	void Set_User(std::string name, std::string pass);
	void UpdateFilesFolders(std::unique_ptr<QTableWidgetItem> icon_item, auto entry, std::unique_ptr<QTableWidgetItem> sync_item, std::string formattedFileTime, std::string type);	ServerUserData Get_User() const;
	std::string FindName(std::string new_name, std::string whereToFind);

private slots:
	void ActionListenerForUpload();
	void ActionListenerForNew();
	void on_Log_out_clicked();
	void on_Download_clicked();
	void on_Copy_to_clicked();
	void on_Move_to_clicked();
	void on_Rename_clicked();
	void on_Delete_clicked();
	void on_Trash_clicked();
	void on_Go_back_clicked();
	void on_Refresh_clicked();
	void on_Magnifying_glass_clicked();
	void on_search_bar_selectionChanged();
	void mouseDoubleClickEvent();
	void on_Sync_clicked();

private:
	Ui::OneDrive ui;
};