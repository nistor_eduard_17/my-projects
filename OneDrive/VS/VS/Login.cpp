#include "Login.h"
#include "ClientConnection.h"

Login::Login(QWidget* parent) : QMainWindow(parent), ui(new Ui::Login)
{
	ui->setupUi(this);
	QPixmap background("Image\\background.jpg");
	background = background.scaled(this->size());
	QPalette palette;
	palette.setBrush(QPalette::Background, background);
	this->setPalette(palette);
	fs::create_directory("Client_Files");
}

bool Check_Login(std::string s1, std::string s2)
{
	std::fstream in("accounts.txt", std::ios::in);
	std::string name, password;
	while (in >> name >> password)
	{
		if (name == s1 && password == s2)
			return true;
	}
	return false;
}

void Login::on_SignIn_clicked()
{
	QString name = ui->Username->text();
	QString password = ui->lineEdit->text();

	if (Check_Login(name.toStdString(), password.toStdString()))
	{
		m_onedrive.Set_User(name.toStdString(), password.toStdString());
		m_onedrive.show();
		this->hide();
		m_onedrive.ConnectUserToServer("login");
		
	}
	else
	{
		QMessageBox msgBox;
		msgBox.setText("Username or password incorrect. Please try again!");
		msgBox.exec();
	}
}

void Login::on_Create_Account_clicked()
{
	m_user.show();
	this->hide();
}

Login::~Login()
{
	delete ui;
}