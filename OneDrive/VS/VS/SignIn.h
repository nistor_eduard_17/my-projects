#ifndef SIGNIN_H
#define SIGNIN_H
#include "OneDrive.h"

namespace Ui { class SignIn; }

class SignIn : public QDialog
{
    Q_OBJECT
    OneDrive m_onedrive;
public:
    explicit SignIn(QWidget *parent = nullptr);
    ~SignIn();

private slots:
    void on_Create_Account_clicked();

private:
    Ui::SignIn *ui;
};

#endif
