#include "File.h"

File::File() :m_path{} {}

File::File(fs::path m_path)
{
	this->m_path = m_path;
}

std::unordered_set<fs::path, pathHash> File::Search(std::string searchedFile, std::string userFolder)
{
	std::unordered_set<fs::path, pathHash> foundFiles;

	for (const auto& entry : fs::directory_iterator(userFolder))
	{
		auto iteratorFile = entry.path().string().find(searchedFile);
		if (iteratorFile != std::string::npos)
		{
			foundFiles.insert(entry);
		}
	}
	return foundFiles;
}

bool File::operator==(const File& file) const
{
	return file.m_path == this->m_path;
}

void File::Set_Path(const fs::path& m_path)
{
	this->m_path = m_path;
}

fs::path File::Get_Path() const
{
	return m_path;
}

bool File::CheckExistingName(std::string file_name, std::string name, std::string whereToFind) const
{
	for (const auto& entry : fs::directory_iterator(whereToFind))
	{
		if (entry.path().filename().string() == file_name)
		{
			return true;
		}
	}
	return false;
}