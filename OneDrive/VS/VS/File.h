#pragma once
#include <qdialog.h>
#include <qinputdialog.h>
#include <qfiledialog.h>
#include <qmessagebox.h>
#include <qtreeview.h>
#include <qlistview.h>
#include <qlistwidget.h>
#include <qtablewidget.h>
#include <qheaderview.h>
#include <qtoolbutton.h>
#include <qaction.h>
#include <qtimer.h>
#include "ui_OneDrive.h"
#include "ui_SignIn.h"
#include "ui_Login.h"
#include <QMouseEvent>
#include <QtWidgets/QMainWindow>
#include "../Server/ServerUserData.h"

class File {
private:
	fs::path m_path;
public:
	File();
	File(fs::path m_path);

	void Set_Path(const fs::path& m_path);
	fs::path Get_Path() const;

	bool operator ==(const File& file) const;
	std::unordered_set<fs::path, pathHash> Search(std::string searchedFile, std::string userFolder);
	bool CheckExistingName(std::string file_name, std::string name, std::string whereToFind) const;

	template <class T>
	T Search(std::string searchedFile, std::string userFolder)
	{
		std::unordered_set<fs::path, pathHash> foundFiles;
		bool foundFile = false;
		for (const auto& entry : fs::directory_iterator(userFolder))
		{
			auto iteratorFile = entry.path().filename().string().find(searchedFile);
			auto iteratorFilename = entry.path().filename().stem().string().find(searchedFile);
			if (iteratorFile != std::string::npos)
			{
				if (entry.path().filename().string().length() == searchedFile.length())
					if constexpr (std::is_same<T, fs::path>::value)
					{
						foundFile = true;
						return entry;
					}
			}
			if (iteratorFilename != std::string::npos)
				foundFiles.insert(entry);
		}
		if constexpr (std::is_same<T, fs::path>::value)
			if (foundFile == false)
			{
				return fs::path(searchedFile);
			}

		if constexpr (std::is_same<T, std::unordered_set<fs::path, pathHash>>::value)
			return foundFiles;
	}
};