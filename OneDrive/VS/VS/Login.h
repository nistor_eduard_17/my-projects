#ifndef LOGIN_H
#define LOGIN_H
#include "SignIn.h"

namespace Ui { class Login; }

class Login : public QMainWindow
{
	Q_OBJECT
	SignIn m_user;
	OneDrive m_onedrive;
public:
	Login(QWidget* parent = Q_NULLPTR);
    ~Login();

private slots:
	void on_SignIn_clicked();
	void on_Create_Account_clicked();
	
private:
	Ui::Login *ui;
};
#endif
