#pragma once
#include <WS2tcpip.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <stack>
#include <unordered_map>
#include <thread>
#include <filesystem>
#include <string>
#pragma comment(lib, "ws2_32.lib")

namespace fs = std::filesystem;

enum class Case
{
	OVERWRITE,
	NO_OVERWRITE,
	NO_EXISTS
};

class ClientConnection
{
	std::string ipAddress;
	int port, wsResult, connResult;
	WSAData data;
	WORD ver;
	SOCKET sock;
	sockaddr_in hint;
	std::unordered_map<std::string, std::pair<std::string, std::string>> client_files;
public:
	ClientConnection();
	~ClientConnection();

	void UploadFiles(fs::path old_path) const;
	void UploadFolder(fs::path old_path) const;
	void Delete(fs::path path, std::string name, std::string new_name) const;
	void Upload(const Case& cases, fs::path old_path, std::string new_path) const;
	void Refresh(fs::path file_path, const std::string& user);
	void Rename(fs::path path, std::string new_name) const;
	void Empty(const std::string& user) const;
	void New(fs::path path) const;
	void Copy(std::string file_path, std::string folderWhereToCopy) const;
	void Move(std::string file_path) const;
	void Sync(std::string file_path) const;
	void Connection(std::string message);
	std::unordered_map < std::string, std::pair<std::string, std::string>> Get_Files() const { return client_files; }
};