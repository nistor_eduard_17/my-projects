#include "SignIn.h"

SignIn::SignIn(QWidget* parent) : QDialog(parent), ui(new Ui::SignIn)
{
	ui->setupUi(this);
	QPixmap background("Image\\background.jpg");
	background = background.scaled(this->size());
	QPalette palette;
	palette.setBrush(QPalette::Background, background);
	this->setPalette(palette);
}

bool Username_Exists(std::string file_name, std::string s1)
{
	std::fstream in("accounts.txt", std::ios::in);
	std::string line;
	while (std::getline(in, line))
	{
		std::string name;
		int i = 0;
		while (line[i] != ' ')
		{
			name.push_back(line[i]);
			i++;
		}
		if (name == s1)
			return 1;
	}
	return 0;
}

void SignIn::on_Create_Account_clicked()
{
	QString name = ui->Username->text();
	QString password = ui->Password->text();

	std::fstream out("accounts.txt", std::ios::out | std::ios::app);
	QMessageBox msgBox;

	if (name == nullptr || password == nullptr)
	{
		msgBox.setText("Invalid Username or password!");
		msgBox.exec();
	}
	else
	{
		if (!Username_Exists("accounts.txt", name.toStdString()))
		{
			out << name.toStdString() << " " << password.toStdString() << '\n';
			out.close();
			msgBox.setText("Account created successfully!");
			msgBox.exec();
			
			m_onedrive.Set_User(name.toStdString(), password.toStdString());
			this->hide();
			m_onedrive.show();
			m_onedrive.ConnectUserToServer("signin");
		}
		else
		{
			msgBox.setText(name + " already exists. Please insert a new Username");
			msgBox.exec();
		}
	}
}

SignIn::~SignIn()
{
	delete ui;
}
