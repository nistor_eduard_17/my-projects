#include "OneDrive.h"
#include "Login.h"

OneDrive::OneDrive(QWidget* parent) : QMainWindow(parent)
{
	ui.setupUi(this);
	this->setPalette(Qt::white);

	QPixmap background("Image\\back_onedrive.jpg");
	background = background.scaled(this->size());
	QPalette palette = ui.Files_Folders->palette();
	palette.setBrush(QPalette::Base, QBrush(background));
	ui.Files_Folders->setPalette(palette);
	connect(ui.Files_Folders, SIGNAL(itemDoubleClicked(QTableWidgetItem*)), this, SLOT(mouseDoubleClickEvent()));
	DropDownMenu(ui.Upload, menu, firstOptionForUpload, secondOptionForUpload);
	DropDownMenu(ui.New, menu, firstOptionForNew, secondOptionForNew);

	QPixmap icon("Image\\search_emoji.png");
	ui.search_emoji->setPixmap(icon.scaled(ui.search_emoji->width(), ui.search_emoji->height(), Qt::KeepAspectRatio));
	ui.search_emoji->hide();
	Generate_Images();
	ui.Files_Folders->setColumnWidth(0, 10);
	ui.Files_Folders->setColumnWidth(1, 230);
	ui.Files_Folders->setColumnWidth(2, 10);
	ui.Files_Folders->setColumnWidth(3, 400);
	ui.Files_Folders->setColumnWidth(4, 300);
	ui.Files_Folders->setColumnWidth(5, 120);
	ui.Info->hide();
	onMagnifyingGlassClicked = false;
}

OneDrive::~OneDrive()
{
	if (m_user.Get_Username().size())
		fs::remove_all("Client_Files\\" + m_user.Get_Username());
}

void OneDrive::Images(QPushButton* push_button, QPixmap image)
{
	QPixmap pixmap(image);
	QIcon ButtonIcon(pixmap);
	push_button->setIcon(ButtonIcon);
	push_button->setIconSize(push_button->size());
}

void OneDrive::Generate_Images()
{
	Images(ui.Upload, { "Image\\upload.png" });
	Images(ui.Copy_to, { "Image\\copy_to.png" });
	Images(ui.Move_to, { "Image\\move_to.png" });
	Images(ui.Delete, { "Image\\delete.png" });
	Images(ui.New, { "Image\\new_folder.png" });
	Images(ui.Download, { "Image\\download.png" });
	Images(ui.Rename, { "Image\\rename.png" });
	Images(ui.Magnifying_glass, { "Image\\magnifying_glass.png" });
	Images(ui.Go_back, { "Image\\go_back.png" });
	Images(ui.Trash, { "Image\\trash.png" });
	Images(ui.Refresh, { "Image\\refresh.png" });
	Images(ui.Sync, { "Image\\Sync.png" });
}

template <typename TP>
std::time_t to_time_t(TP tp)
{
	using namespace std::chrono;
	auto sctp = time_point_cast<system_clock::duration>(tp - TP::clock::now() + system_clock::now());
	return system_clock::to_time_t(sctp);
}

void OneDrive::UpdateFilesFolders(std::unique_ptr<QTableWidgetItem> icon_item, auto entry, std::unique_ptr<QTableWidgetItem> sync_item, std::string formattedFileTime, std::string type)
{
	auto icon = icon_item->clone();
	icon->setFlags(icon->flags() & ~Qt::ItemIsEditable);
	ui.Files_Folders->setItem(ui.Files_Folders->rowCount() - 1, 0, icon);

	//pointer for file's path
	std::unique_ptr<QTableWidgetItem> pointer = std::make_unique<QTableWidgetItem>(QString::fromStdString(entry.path().filename().stem().string()));
	auto path = pointer->clone();
	path->setFlags(path->flags() & ~Qt::ItemIsEditable);
	ui.Files_Folders->setItem(ui.Files_Folders->rowCount() - 1, 1, path);

	auto sync = sync_item->clone();
	sync->setFlags(sync->flags() & ~Qt::ItemIsEditable);
	ui.Files_Folders->setItem(ui.Files_Folders->rowCount() - 1, 2, sync);

	//pointer for file's time
	pointer = std::make_unique<QTableWidgetItem>(QString::fromStdString(formattedFileTime));
	auto time = pointer->clone();
	time->setFlags(time->flags() & ~Qt::ItemIsEditable);
	ui.Files_Folders->setItem(ui.Files_Folders->rowCount() - 1, 3, time);
	ui.Files_Folders->item(ui.Files_Folders->rowCount() - 1, 3)->setTextAlignment(Qt::AlignLeft);

	//pointer for file's type
	pointer = std::make_unique<QTableWidgetItem>(QString::fromStdString(type));
	auto filetype = pointer->clone();
	filetype->setFlags(filetype->flags() & ~Qt::ItemIsEditable);
	ui.Files_Folders->setItem(ui.Files_Folders->rowCount() - 1, 4, filetype);
	ui.Files_Folders->item(ui.Files_Folders->rowCount() - 1, 4)->setTextAlignment(Qt::AlignLeft);

	std::string size = static_cast<std::stringstream>(std::stringstream() << entry.file_size()).str();
	std::stringstream Integer(size);
	int size_kb;
	Integer >> size_kb;
	size_kb /= 1024;

	//pointer for file's size
	pointer = std::make_unique<QTableWidgetItem>(QString::number(size_kb) + "KB");
	auto filesize = pointer->clone();
	filesize->setFlags(filesize->flags() & ~Qt::ItemIsEditable);
	ui.Files_Folders->setItem(ui.Files_Folders->rowCount() - 1, 5, filesize);
	ui.Files_Folders->item(ui.Files_Folders->rowCount() - 1, 5)->setTextAlignment(Qt::AlignRight);
}

std::string ConvertToType(std::string type)
{
	if (type == "TXT File")
		return ".txt";

	if (type == "Microsoft Word Document")
		return ".docx";

	if (type == "File Folder")
		return "";

	if (type == "Microsoft Excel Worksheet")
		return ".xlsx";

	if (type == "JPG Image")
		return ".jpg";

	if (type == "JPEG Image")
		return ".jpeg";

	if (type == "PNG Image")
		return ".png";

	if (type == "Shortcut")
		return ".lnk";

	if (type == "Microsoft PowerPoint Presentation")
		return ".pptx";

	if (type == "Archive")
		return ".rar";

	if (type == "PDF")
		return ".pdf";
}

std::string Get_Type(std::string extension)
{
	if (extension == ".txt")
		return "TXT File";
	if (extension == ".docx")
		return "Microsoft Word Document";
	if (!extension.size())
		return "File Folder";
	if (extension == ".xlsx")
		return "Microsoft Excel Worksheet";
	if (extension == ".jpg")
		return "JPG Image";
	if (extension == ".jpeg")
		return "JPEG Image";
	if (extension == ".png")
		return "PNG Image";
	if (extension == ".lnk")
		return "Shortcut";
	if (extension == ".pptx")
		return "Microsoft PowerPoint Presentation";
	if (extension == ".rar" || extension == ".zip")
		return "Archive";
	if (extension == ".pdf")
		return "PDF";
}

void OneDrive::Generate_Files(std::string user)
{
	bool ok = false;
	ui.Files_Folders->show();
	ui.path->setText(QString::fromStdString(user));
	QList <QString> files;
	ui.Files_Folders->clear();
	ui.Files_Folders->setRowCount(0);
	if (!fs::is_empty(user))
	{
		std::string type = "";
		std::string path_icon = "";
		if (m_files_status.size() == 0)
		{
			m_user.Set_Files_Data("Client_Files\\" + m_user.Get_Username()); //no++

			m_files_status = m_user.Get_FilesStatus();
		}

		for (const auto& entry : fs::directory_iterator(user))
		{
			if (entry.path().filename().string() != "Trash" && entry.path().filename().string() != "User_Files_Datas.txt")
			{
				std::string type = Get_Type(entry.path().extension().string());
				if (type == "JPG Image" || type == "JPEG Image" || type == "PNG Image")
					path_icon = "Image\\Image.png";
				else
					path_icon = "Image\\" + type + ".png";

				std::unique_ptr<QTableWidgetItem> icon_item = std::make_unique<QTableWidgetItem>();
				QIcon icon(path_icon.c_str());

				icon_item->setIcon(icon);
				icon_item->setTextAlignment(Qt::AlignLeft);

				if (m_files_status[entry.path().string()])
				{
					path_icon = "Image\\done.png";
				}
				else
				{
					path_icon = "Image\\cross.png";
				}
				std::unique_ptr<QTableWidgetItem> sync_item = std::make_unique<QTableWidgetItem>();
				QIcon sync(path_icon.c_str());

				sync_item->setIcon(sync);
				sync_item->setTextAlignment(Qt::AlignLeft);

				std::string formattedFileTime = m_user.Get_Date(entry);

				if (onMagnifyingGlassClicked == false)
				{
					ui.Files_Folders->insertRow(ui.Files_Folders->rowCount());
					UpdateFilesFolders(std::move(icon_item), entry, std::move(sync_item), formattedFileTime, type);
				}
				else if (foundFiles.find(entry.path().string()) != foundFiles.end())
				{
					ui.Files_Folders->insertRow(ui.Files_Folders->rowCount());
					UpdateFilesFolders(std::move(icon_item), entry, std::move(sync_item), formattedFileTime, type);
				}
			}
		}

	}
	ui.Files_Folders->setHorizontalHeaderLabels(QStringList{ "Icon","Name","Status","Date modified","Type","Size" });
}

void OneDrive::PrintFiles()
{
	ui.Files_Folders->clear();

	if (foundFiles.empty())
	{
		ui.Files_Folders->hide();
		ui.search_emoji->show();
		ui.not_found_file->setAlignment(Qt::AlignCenter);
		ui.not_found_file->setText("No items match your search! \n Try a different search.");
		ui.not_found_file->show();
		return;
	}
	Generate_Files(ui.path->toPlainText().toStdString());
}

void OneDrive::DropDownMenu(QPushButton* button, QMenu* menu, QAction* firstOption, QAction* secondOption)
{
	menu = new QMenu(this);
	firstOption = new QAction("File", this);
	secondOption = new QAction("Folder", this);
	firstOption->setObjectName(firstOption->text());
	secondOption->setObjectName(secondOption->text());
	menu->addAction(firstOption);
	menu->addAction(secondOption);
	button->setMenu(menu);

	if (button->objectName() == "Upload")
	{
		connect(button, SIGNAL(clicked()), this, SLOT(ActionListenerForUpload()));
		connect(firstOption, SIGNAL(triggered()), this, SLOT(ActionListenerForUpload()));
		connect(secondOption, SIGNAL(triggered()), this, SLOT(ActionListenerForUpload()));
	}
	else
	{
		connect(button, SIGNAL(clicked()), this, SLOT(ActionListenerForNew()));
		connect(firstOption, SIGNAL(triggered()), this, SLOT(ActionListenerForNew()));
		connect(secondOption, SIGNAL(triggered()), this, SLOT(ActionListenerForNew()));
	}
}

void OneDrive::UploadNoOverwrite(FileType type, std::string userFile, fs::path search_file)
{
	bool ok = true;
	std::string new_name = QInputDialog::getText(this, "New Name", QString::fromStdString("Insert a new name"), QLineEdit::Normal, QString::fromStdString(""), &ok).toStdString();

	if (type == FileType::FILE)
	{
		new_name = FindName(new_name + search_file.filename().extension().string(), ui.path->toPlainText().toStdString());

		fs::path parent = userFile;
		fs::copy(search_file, parent.string() + "\\" + new_name, fs::copy_options::recursive);
		m_client_to_server->Upload(Case::NO_OVERWRITE, search_file.string(), parent.string() + "\\" + new_name);
		return;
	}
	if (type == FileType::FOLDER)
	{
		new_name = FindName(new_name, ui.path->toPlainText().toStdString());

		fs::create_directory(userFile + "\\" + new_name);
		fs::copy(search_file, userFile + "\\" + new_name, fs::copy_options::recursive);
		m_client_to_server->Upload(Case::NO_OVERWRITE, search_file.string(), userFile + "\\" + new_name);
		return;
	}
}

void OneDrive::UploadOverwrite(FileType type, std::string userFile, fs::path search_file)
{
	if (type == FileType::FILE)
	{
		fs::remove_all(userFile + "\\" + search_file.filename().string());
		fs::copy(search_file, userFile, fs::copy_options::update_existing | fs::copy_options::recursive);
		m_client_to_server->Upload(Case::OVERWRITE, search_file.string(), userFile);
		return;
	}
	if (type == FileType::FOLDER)
	{
		fs::remove_all(userFile + "\\" + search_file.filename().string());
		fs::copy(search_file, userFile + "\\" + search_file.filename().string(), fs::copy_options::recursive);
		m_client_to_server->Upload(Case::OVERWRITE, search_file.string(), userFile);
		return;
	}
}

void OneDrive::UploadSelectedFile(FileType type, QString add_files)
{
	QMessageBox msgbox;
	msgbox.setText("Are you sure you want to upload this file?");
	msgbox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);

	std::string userFile = ui.path->toPlainText().toStdString();

	if (msgbox.exec() == QMessageBox::Yes)
	{
		bool check = true;
		fs::path search_file = add_files.toStdString();

		for (const auto& it_userFiles : fs::directory_iterator(userFile))
		{
			auto iteratorFile = it_userFiles.path().string().find(search_file.filename().string());
			if (iteratorFile != std::string::npos)
			{
				QMessageBox msgBox;
				msgBox.setText("There is already a file with this name. Would you like to overwrite it?");
				msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);

				if (msgBox.exec() == QMessageBox::Yes)
				{
					UploadOverwrite(type, userFile, search_file);
				}
				else
				{
					UploadNoOverwrite(type, userFile, search_file);
				}
				check = false;
				break;
			}
		}
		if (check)
		{
			if (type == FileType::FILE)
			{
				fs::copy(add_files.toStdString(), userFile, fs::copy_options::skip_existing | fs::copy_options::recursive);
			}
			else if (type == FileType::FOLDER)
			{
				fs::create_directory(userFile + "\\" + search_file.filename().stem().string());
				fs::copy(add_files.toStdString(), userFile + "\\" + search_file.filename().stem().string(), fs::copy_options::recursive);
			}
			m_client_to_server->Upload(Case::NO_EXISTS, add_files.toStdString(), userFile);
		}
		Generate_Files(ui.path->toPlainText().toStdString());
	}
}

void OneDrive::ActionListenerForUpload()
{
	QToolButton* toolButton = qobject_cast<QToolButton*>(sender());
	QAction* act = qobject_cast<QAction*>(sender());

	FileType type;
	if (act->objectName() == "File")
		type = FileType::FILE;
	else if (act->objectName() == "Folder")
		type = FileType::FOLDER;

	if (act)
	{
		QFileDialog dialog;

		if (type == FileType::FILE)
		{
			dialog.setFileMode(QFileDialog::ExistingFiles);
		}

		if (type == FileType::FOLDER)
		{
			dialog.setFileMode(QFileDialog::Directory);
		}

		dialog.setOption(QFileDialog::DontUseNativeDialog, true);
		dialog.setDirectory("C:\\");
		dialog.exec();

		if (!dialog.selectedFiles().empty())
		{
			QString add_files = dialog.selectedFiles().front();
			UploadSelectedFile(type, add_files);
		}
	}
}

void OneDrive::ActionListenerForNew()
{
	QToolButton* toolButton = qobject_cast<QToolButton*>(sender());
	QAction* act = qobject_cast<QAction*>(sender());
	if (act)
	{
		if (act->objectName() == "File")
		{
			CreateDocument();
		}

		if (act->objectName() == "Folder")
		{
			CreateFolder();
		}
	}
}

void OneDrive::CreateDocument()
{
	QInputDialog qDialog;

	QStringList items;
	items << QString("TXT File");
	items << QString("Microsoft Word Document");
	items << QString("Microsoft Excel Worksheet");
	items << QString("Microsoft PowerPoint Presentation");

	qDialog.setOptions(QInputDialog::UseListViewForComboBoxItems);
	qDialog.setComboBoxItems(items);
	qDialog.setWindowTitle("Choose type:");

	QObject::connect(&qDialog, SIGNAL(textValueChanged(const QString&)),
		this, SLOT(onCompute(const QString&)));

	if (qDialog.exec())
	{
		std::string type = qDialog.textValue().toStdString();

		QString get_name;
		bool ok = true;
		get_name = QInputDialog::getText(this, "Name", QString::fromStdString("Insert name"), QLineEdit::Normal, QString::fromStdString(""), &ok);
		std::string new_name = FindName(get_name.toStdString() + ConvertToType(type), ui.path->toPlainText().toStdString());

		std::string path = ui.path->toPlainText().toStdString() + '\\' + new_name;
		std::ofstream new_file(path);

		m_client_to_server->New(path);

		ui.Files_Folders->clear();
		Generate_Files(ui.path->toPlainText().toStdString());
	}
}

void OneDrive::CreateFolder()
{
	QString get_name;
	bool ok = true;
	get_name = QInputDialog::getText(this, "Name", QString::fromStdString("Insert name"), QLineEdit::Normal, QString::fromStdString(""), &ok);
	std::string new_name = FindName(get_name.toStdString(), ui.path->toPlainText().toStdString());

	std::string path = ui.path->toPlainText().toStdString() + '\\' + new_name;
	fs::create_directory(path);

	m_client_to_server->New(path);

	ui.Files_Folders->clear();
	Generate_Files(ui.path->toPlainText().toStdString());
}

std::string OneDrive::FindName(std::string new_name, std::string whereToFind)
{
	bool ok = true;
	QWidget* p = new QWidget();
	fs::path path = files.Search<fs::path>(new_name, whereToFind);
	if (path.string() == new_name) return new_name;

	new_name = path.filename().string();
	while (files.CheckExistingName(new_name, m_user.Get_Username(), whereToFind))
	{
		QString get_name = QInputDialog::getText(p, "New Name", QString::fromStdString(new_name + " already exists. Insert the new name:"), QLineEdit::Normal, QString::fromStdString(""), &ok);
		new_name = get_name.toStdString() + path.extension().string();
	}
	return new_name;
}

ServerUserData OneDrive::Get_User() const
{
	return m_user;
}

void OneDrive::Set_User(std::string name, std::string pass)
{
	m_user.Set_Username(name);
	m_user.Set_UserPassword(pass);
}

void OneDrive::on_Log_out_clicked()
{
	m_client_to_server->Connection({ "Logout" });
	fs::remove_all("Client_Files\\" + m_user.Get_Username());
	this->hide();
	logout = new Login();
	logout->show();
}

void OneDrive::on_Move_to_clicked()
{
	on_Copy_to_clicked();
	if (ui.Info->text().isEmpty())
	{
		m_client_to_server->Move(clipboard.string());
		fs::remove_all(clipboard);
		ui.Files_Folders->clear();
		Generate_Files(ui.path->toPlainText().toStdString());
		clipboard.clear();
	}
}

void OneDrive::on_Rename_clicked()
{
	if (ui.Files_Folders->currentItem())
	{
		const QString& selectedFile = ui.Files_Folders->model()->data(ui.Files_Folders->model()->index(ui.Files_Folders->currentRow(), 1)).toString();
		std::string extension = ConvertToType(ui.Files_Folders->model()->data(ui.Files_Folders->model()->index(ui.Files_Folders->currentRow(), 4)).toString().toStdString());
		fs::path selectedItemPath = files.Search<fs::path>(selectedFile.toStdString() + extension, ui.path->toPlainText().toStdString());

		if (!selectedFile.isEmpty())
		{
			bool ok = true;
			QString get_name = QInputDialog::getText(this, "New Name", QString::fromStdString("Insert a new name"), QLineEdit::Normal, QString::fromStdString(""), &ok);
			std::string new_name = FindName(get_name.toStdString() + extension, ui.path->toPlainText().toStdString());

			fs::rename(ui.path->toPlainText().toStdString() + "/" + selectedItemPath.filename().string(), ui.path->toPlainText().toStdString() + "/" + new_name);
			ui.Files_Folders->clear();
			Generate_Files(ui.path->toPlainText().toStdString());
			m_client_to_server->Rename(selectedItemPath, new_name);
		}
	}
	else
	{
		QMessageBox msgbox;
		msgbox.setText("Please select an item");
		msgbox.exec();
	}
}

void OneDrive::on_Refresh_clicked()
{
	fs::path selected_item = "";
	if (ui.Files_Folders->currentItem() && ui.Files_Folders->currentColumn() == 1)
	{
		std::string extension = ConvertToType(ui.Files_Folders->model()->data(ui.Files_Folders->model()->index(ui.Files_Folders->currentRow(), 4)).toString().toStdString());
		selected_item = ui.path->toPlainText().toStdString() + "\\" + ui.Files_Folders->currentItem()->text().toStdString() + extension;
	}
	m_client_to_server->Refresh(selected_item, m_user.Get_Username());
	Generate_Files("Client_Files\\" + m_user.Get_Username());
}

void FunctionForCopyFiles(fs::path& clipboard, fs::path& file_path, std::string& name)
{
	if (!clipboard.filename().has_extension())
		fs::copy(clipboard, file_path / name, fs::copy_options::update_existing | fs::copy_options::recursive);
	else
	{
		fs::remove_all(file_path.string() + "\\" + name);
		fs::copy(clipboard, file_path.string(), fs::copy_options::update_existing);
	}
}

void OneDrive::CopyAction(std::string objectName)
{
	fs::path whereToCopy = std::filesystem::current_path().string() + "\\" + ui.path->toPlainText().toStdString();
	std::string name = clipboard.filename().string();
	fs::path fileName = files.Search<fs::path>(name, whereToCopy.string());


	if (fileName.string().length() != name.length())  //already exists the file you want to copy
	{
		QMessageBox msgBox;
		msgBox.setText("There is already a file with this name. Would you like to overwrite it?");
		msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		if (msgBox.exec() == QMessageBox::Yes)
		{
			fs::remove_all(whereToCopy / name);
			FunctionForCopyFiles(clipboard, whereToCopy, name);
		}
		else
		{
			std::string new_name = FindName(name, whereToCopy.string());
			if (!clipboard.filename().has_extension())
			{
				fs::create_directory(whereToCopy / new_name);
				fs::copy(clipboard, whereToCopy / new_name, fs::copy_options::update_existing | fs::copy_options::recursive);
			}
			else
			{
				fs::path original_path = clipboard.parent_path();
				fs::rename(original_path / name, original_path / new_name);
				clipboard = original_path.string() + "\\" + new_name;
				fs::copy(clipboard, whereToCopy, fs::copy_options::recursive);
			}
		}
		m_client_to_server->Copy(clipboard.string(), whereToCopy.string());
		if (objectName != "Move_to")
			clipboard.clear();
		ui.Info->clear();
		Generate_Files(ui.path->toPlainText().toStdString());
		return;
	}

	FunctionForCopyFiles(clipboard, whereToCopy, name);
	m_client_to_server->Copy(clipboard.string(), whereToCopy.string());
	Generate_Files(ui.path->toPlainText().toStdString());
	ui.Info->clear();
	if (objectName != "Move_to")
		clipboard.clear();
	return;
}

void OneDrive::on_Copy_to_clicked()
{
	QPushButton* buttonSender = qobject_cast<QPushButton*>(sender());
	if (buttonSender->objectName().toStdString() == "Move_to" && !clipboard.empty())
	{
		auto iterator = ui.Info->text().toStdString().find("Move_to");
		if (iterator == std::string::npos)
		{
			QMessageBox msgBox;
			msgBox.setText("Your current action is Copy_To.Please finish it or cancel it in order to use Move_To.");
			msgBox.exec();
			return;
		}
	}
	if (clipboard.string().length() != 0)
	{
		std::string objectName = buttonSender->objectName().toStdString();
		CopyAction(objectName);
		return;
	}
	if (ui.Files_Folders->currentItem())
	{
		std::string extension = ConvertToType(ui.Files_Folders->model()->data(ui.Files_Folders->model()->index(ui.Files_Folders->currentRow(), 4)).toString().toStdString());
		clipboard = std::filesystem::current_path().string() + "\\" + ui.path->toPlainText().toStdString() + "\\" + ui.Files_Folders->currentItem()->text().toStdString() + extension;
		ui.Info->setText(QString::fromStdString("You have " + clipboard.filename().string() + " on clipboard \n Operation: " + buttonSender->objectName().toStdString()));
		ui.Info->show();
	}
	else
	{
		QMessageBox msgBox;
		msgBox.setText("Please select an item");
		msgBox.exec();
	}
}

void OneDrive::DeleteFromTrash()
{
	std::string extension = ConvertToType(ui.Files_Folders->model()->data(ui.Files_Folders->model()->index(ui.Files_Folders->currentRow(), 3)).toString().toStdString());
	const QString& current_file = ui.Files_Folders->currentItem()->text() + QString::fromStdString(extension);
	if (!current_file.isEmpty())
	{
		fs::remove_all(ui.path->toPlainText().toStdString() + "\\" + current_file.toStdString());
		m_client_to_server->Delete(ui.path->toPlainText().toStdString(), current_file.toStdString(), "Trash");
		ui.Files_Folders->clear();
		Generate_Files(ui.path->toPlainText().toStdString());
	}
}

void OneDrive::MoveToTrash()
{
	std::string extension = ConvertToType(ui.Files_Folders->model()->data(ui.Files_Folders->model()->index(ui.Files_Folders->currentRow(), 4)).toString().toStdString());
	const QString& current_file = ui.Files_Folders->currentItem()->text() + QString::fromStdString(extension);
	if (!current_file.isEmpty())
	{
		fs::path file_path = std::filesystem::current_path().string() + "\\" + "Client_Files\\" + m_user.Get_Username() + "\\Trash";
		std::string name = current_file.toStdString();
		clipboard = std::filesystem::current_path().string() + "\\" + ui.path->toPlainText().toStdString();
		std::string new_name = FindName(name, file_path.string());

		if (extension == "")
		{
			fs::create_directory(file_path / new_name);
			m_client_to_server->Delete(clipboard, name, new_name);
			fs::rename(clipboard / name, clipboard / new_name);
			fs::copy(clipboard / new_name, file_path / new_name, fs::copy_options::update_existing | fs::copy_options::recursive);
			fs::remove_all(clipboard / new_name);
		}
		else
		{
			fs::rename(clipboard / name, clipboard / new_name);
			m_client_to_server->Delete(clipboard, name, new_name);
			clipboard = clipboard.string() + "\\" + new_name;
			fs::copy(clipboard, file_path, fs::copy_options::recursive);
			fs::remove_all(clipboard);

		}
		ui.Files_Folders->clear();
		Generate_Files(ui.path->toPlainText().toStdString());
	}
}

void OneDrive::on_Delete_clicked()
{
	if (ui.Files_Folders->currentItem())
	{
		if (ui.path->toPlainText().toStdString() == "Client_Files\\" + m_user.Get_Username() + "\\Trash")
		{
			DeleteFromTrash();
		}
		else
		{
			MoveToTrash();
		}
	}
	else
	{
		QMessageBox msgbox;
		msgbox.setText("Please select an item");
		msgbox.exec();
	}
}

void OneDrive::on_Trash_clicked()
{
	std::string path = "Client_Files\\" + m_user.Get_Username() + "\\Trash";
	if (ui.path->toPlainText().toStdString() == path)
		Generate_Files("Client_Files\\" + m_user.Get_Username());
	else Generate_Files(path);
}

void OneDrive::on_Go_back_clicked()
{
	std::string path = ui.path->toPlainText().toStdString();

	if (path != "Client_Files\\" + m_user.Get_Username())
	{
		int count = path.size() - 1;
		while (path[count] != '\\')
		{
			count--;
		}
		path.erase(path.begin() + count, path.end());
		ui.path->setText(QString::fromStdString(path));
		Generate_Files(path);
	}
}

void OneDrive::DownloadFileAlreadyExist(const auto& file, fs::path file_path, std::string extension, QString pathToDownload)
{
	QMessageBox fileAlreadyExist;
	fileAlreadyExist.setText("There is already a file with this name. Would you like to overwrite it?");
	fileAlreadyExist.setStandardButtons(QMessageBox::Yes | QMessageBox::No);

	if (fileAlreadyExist.exec() == QMessageBox::Yes)
	{
		fs::copy(file_path, pathToDownload.toStdString() + "\\" + file_path.filename().string(), fs::copy_options::update_existing | fs::copy_options::recursive);
		return;
	}
	else
	{
		bool ok = true;
		std::string new_name = QInputDialog::getText(this, "New Name", QString::fromStdString("Insert a new name"), QLineEdit::Normal, QString::fromStdString(""), &ok).toStdString();
		std::string aux_name = "!";

		new_name = FindName(new_name + extension, pathToDownload.toStdString());
		aux_name += extension;
		fs::rename(file, file.path().parent_path() / aux_name);
		fs::copy(file_path, file, fs::copy_options::recursive);
		fs::rename(file.path().parent_path() / aux_name, file.path().parent_path() / new_name);
	}
}

void OneDrive::DownloadAction()
{
	std::string extension = ConvertToType(ui.Files_Folders->model()->data(ui.Files_Folders->model()->index(ui.Files_Folders->currentRow(), 3)).toString().toStdString());
	fs::path file_path = std::filesystem::current_path().string() + "\\" + ui.path->toPlainText().toStdString() + "\\" + ui.Files_Folders->currentItem()->text().toStdString() + extension;

	QFileDialog dialog = QFileDialog(this);
	dialog.setFileMode(QFileDialog::Directory);
	dialog.setDirectory("C:\\");
	dialog.exec();
	QString pathToDownload = dialog.selectedFiles().front();
	fs::path downloadPath = pathToDownload.toStdString();

	for (const auto& file : fs::directory_iterator(downloadPath))
	{
		if (file.path().filename().string() == file_path.filename().string())
		{
			DownloadFileAlreadyExist(file, file_path, extension, pathToDownload);
			break;
		}
	}
	fs::copy(file_path, pathToDownload.toStdString() + "\\" + file_path.filename().string(), fs::copy_options::update_existing | fs::copy_options::recursive);
}

void OneDrive::on_Download_clicked()
{
	if (ui.Files_Folders->currentItem())
	{
		DownloadAction();
	}
	else
	{
		QMessageBox msgbox;
		msgbox.setText("Please select an item");
		msgbox.exec();
	}
}

void OneDrive::on_Magnifying_glass_clicked()
{
	std::string searchedFile = ui.search_bar->toPlainText().toStdString();
	std::string userFile = ui.path->toPlainText().toStdString();
	foundFiles = files.Search<std::unordered_set<fs::path, pathHash>>(searchedFile, userFile);

	onMagnifyingGlassClicked = true;
	PrintFiles();
	foundFiles.clear();
	onMagnifyingGlassClicked = false;
}

void OneDrive::on_search_bar_selectionChanged()
{
	if (ui.search_bar->toPlainText() == "Search...")
		ui.search_bar->clear();
	if (ui.search_bar->toPlainText() == "")
	{
		ui.search_bar->toPlainText() = "Search...";
		ui.Files_Folders->clear();
		Generate_Files(ui.path->toPlainText().toStdString());
		ui.search_emoji->hide();
		ui.not_found_file->hide();
	}
}

void OneDrive::DownloadExistingFiles()
{
	m_client_to_server = new ClientConnection();
	m_client_to_server->Connection(m_user.Get_Username());

	std::ifstream file("Client_Files\\" + m_user.Get_Username() + "\\User_Files_Datas.txt");
	m_user.CreateFilesData(file);
	Generate_Files("Client_Files\\" + m_user.Get_Username());
}

void OneDrive::UploadLocalFile()
{
	QFileDialog dialog;
	QToolButton* toolButton = qobject_cast<QToolButton*>(sender());
	dialog.setFileMode(QFileDialog::Directory);
	FileType type = FileType::FOLDER;
	dialog.setOption(QFileDialog::DontUseNativeDialog, true);
	dialog.setDirectory("C:\\");
	dialog.exec();

	if (!dialog.selectedFiles().empty())
	{
		QString add_files = dialog.selectedFiles().front();  //path
		std::string userFile = "Client_Files\\" + m_user.Get_Username();

		m_client_to_server->Upload(Case::NO_EXISTS, add_files.toStdString(), userFile);
		m_client_to_server->Connection(m_user.Get_Username());

		std::ifstream file("Client_Files\\" + m_user.Get_Username() + "\\User_Files_Datas.txt");
		m_user.CreateFilesData(file);
		Generate_Files("Client_Files\\" + m_user.Get_Username());
	}
}

void OneDrive::ConnectUserToServer(const std::string& message)
{
	int result = 0;
	if (message == "login")
	{
		QMessageBox::StandardButton msg;
		msg = QMessageBox::question(this, " ", "You want to download existing files?", QMessageBox::Yes | QMessageBox::No);
		if (msg == QMessageBox::Yes)
		{
			DownloadExistingFiles();
		}
		else result = 1;
	}
	else result = 1;

	if (result == 1)
	{
		m_client_to_server = new ClientConnection();
		m_client_to_server->Empty(m_user.Get_Username()); //empty user folder
		ui.path->setText(QString::fromStdString("Client_Files\\" + m_user.Get_Username()));
		QMessageBox::StandardButton reply;
		reply = QMessageBox::question(this, " ", "Upload local file?", QMessageBox::Yes | QMessageBox::No);
		if (reply == QMessageBox::Yes)
		{
			UploadLocalFile();
		}
		else
		{
			m_client_to_server->Connection(m_user.Get_Username());
			std::string userFile = "Client_Files\\" + m_user.Get_Username();
			Generate_Files(userFile);
		}
	}
}

void OneDrive::mouseDoubleClickEvent()
{
	std::string extension = ConvertToType(ui.Files_Folders->model()->data(ui.Files_Folders->model()->index(ui.Files_Folders->currentRow(), 4)).toString().toStdString());
	if (extension != "")
	{
		auto file_path = std::filesystem::current_path().string() + "\\" + ui.path->toPlainText().toStdString() + "\\" + ui.Files_Folders->currentItem()->text().toStdString() + extension;
		ShellExecuteA(NULL, "open", file_path.c_str(), NULL, NULL, SW_SHOW);
		std::string path = ui.path->toPlainText().toStdString() + "\\" + ui.Files_Folders->currentItem()->text().toStdString() + extension;
		m_files_status[path] = false;
		Generate_Files(ui.path->toPlainText().toStdString());
		return;
	}
	fs::path open_item = ui.Files_Folders->currentItem()->text().toStdString();
	if (!open_item.extension().string().size())
	{
		std::string path = ui.path->toPlainText().toStdString();
		Generate_Files(path + '\\' + open_item.string());
		ui.path->setText(QString::fromStdString(path + '\\' + open_item.string()));
	}
}

void OneDrive::on_Sync_clicked()
{
	std::string name = ui.Files_Folders->model()->data(ui.Files_Folders->model()->index(ui.Files_Folders->currentRow(), 1)).toString().toStdString();
	std::string extension = ConvertToType(ui.Files_Folders->model()->data(ui.Files_Folders->model()->index(ui.Files_Folders->currentRow(), 4)).toString().toStdString());
	if (extension != "")
	{
		m_client_to_server->Sync(ui.path->toPlainText().toStdString() + "\\" + name + extension);
		m_files_status[ui.path->toPlainText().toStdString() + "\\" + name + extension] = true;
		Generate_Files(ui.path->toPlainText().toStdString());
	}
}