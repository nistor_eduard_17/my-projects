#include "ClientConnection.h"

ClientConnection::ClientConnection()
{
	ipAddress = "127.0.0.1";
	port = 54000;

	ver = MAKEWORD(2, 2);
	wsResult = WSAStartup(ver, &data);

	if (wsResult != 0)
	{
		std::cerr << "Can't start Winsock, Err #" << wsResult << std::endl;
	}

	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock == INVALID_SOCKET)
	{
		std::cerr << "Can't create socket, Err #" << WSAGetLastError() << std::endl;
		WSACleanup();
	}

	hint.sin_family = AF_INET;
	hint.sin_port = htons(port);
	inet_pton(AF_INET, ipAddress.c_str(), &hint.sin_addr);

	connResult = connect(sock, (sockaddr*)&hint, sizeof(hint));
	if (connResult == SOCKET_ERROR)
	{
		std::cerr << "Can't connect to server, Err #" << WSAGetLastError() << std::endl;
		closesocket(sock);
		WSACleanup();
	}
}

ClientConnection::~ClientConnection()
{
	closesocket(sock);
	WSACleanup();
	return;
}

void ClientConnection::Connection(std::string user)
{
	char buf[4096];
	std::string userInput;
	userInput = user;

	if (user == "Logout")
	{
		int sendResult = send(sock, "Disconnected", 13, 0);
	}
	else if (userInput.size() > 0)
	{
		std::ofstream out("client_receive.txt");
		int sendResult = send(sock, userInput.c_str(), userInput.size() + 1, 0);

		if (sendResult != SOCKET_ERROR)
		{
			fs::create_directory("Client_Files\\" + user);

			int bytesReceived = 0, size = 0;;
			ZeroMemory(buf, 4096);
			do
			{
				bytesReceived = recv(sock, buf, 4096, 0);
				if (std::string(buf, 0, bytesReceived) == "STOP") break;

				//path
				fs::path check = std::string(buf, 0, bytesReceived);
				memset((char*)buf, 0, 4096);

				if (check.extension().string().size())
				{
					//size zero
					bytesReceived = recv(sock, buf, 4096, 0);
					std::stringstream integer(std::string(buf, 0, bytesReceived));
					integer >> size;
					memset((char*)buf, 0, 4096);

					//array zero
					char* buf2 = new char[size]();
					bytesReceived = recv(sock, buf2, size, 0);
					std::string zero = (std::string(buf2, 0, bytesReceived));
					memset((char*)buf2, 0, size);
					size = 0;

					//size info
					bytesReceived = recv(sock, buf, 4096, 0);
					std::stringstream integer2(std::string(buf, 0, bytesReceived));
					integer2 >> size;
					memset((char*)buf, 0, 4096);

					//info
					buf2 = new char[size]();
					bytesReceived = recv(sock, buf2, size, 0);
					std::string data = (std::string(buf2, 0, bytesReceived));
					memset((char*)buf2, 0, size);

					client_files.insert({ check.string(), { zero, data} });
				}
				else
				{
					fs::create_directory("Client_Files\\" + user + "\\" + check.string());
				}

			} while (bytesReceived > 0);
		}
		userInput.clear();

		for (auto& i : client_files)
		{
			for (auto& j : i.second.second)
			{
				if (j == '0') j = '\0';
			}
			if (i.second.first != " ")
			{
				int j = 1;
				while (j < i.second.first.size())
				{
					std::string pos = "";
					while (j < i.second.first.size() && i.second.first[j] != ' ')
					{
						pos += i.second.first[j];
						j++;
					}
					j++;

					int position = 0;
					std::stringstream integer(pos);
					integer >> position;
					out << position << ' ';
					i.second.second[position] = '0';
				}
			}
			std::ofstream new_file("Client_Files\\" + user + "\\" + i.first, std::ios::binary);
			new_file << i.second.second;
			new_file.close();
		}
		out.close();
	}
}

void ClientConnection::Rename(fs::path path, std::string new_name) const
{
	send(sock, "RENAME", 6, 0);

	std::string file = path.string();
	file.erase(file.begin(), file.begin() + static_cast<std::string>("Client_Files\\").size());

	send(sock, file.c_str(), file.size(), 0);
	Sleep(20);
	send(sock, new_name.c_str(), new_name.size(), 0);
}

void ClientConnection::Delete(fs::path path, std::string name, std::string new_name) const
{
	send(sock, "DELETE", 6, 0);
	std::string file = path.string();
	auto searchPosition = file.find(static_cast<std::string>("Client_Files\\"));
	file.erase(file.begin(), file.begin() + searchPosition + static_cast<std::string>("Client_Files\\").size());

	send(sock, file.c_str(), file.size(), 0);
	Sleep(20);
	send(sock, name.c_str(), name.size(), 0);
	Sleep(20);
	send(sock, new_name.c_str(), new_name.size(), 0);
}

void ClientConnection::Refresh(fs::path file_path, const std::string& user)
{
	if (file_path == "")
	{
		Connection(user);
	}
	else if (!file_path.has_extension())
	{
		std::string file = file_path.string();
		file.erase(file.begin(), file.begin() + static_cast<std::string>("Client_Files\\").size());
		client_files.clear();
		Connection(file);
	}
	else
	{
		send(sock, "REFRESH", 7, 0);
		std::string file = file_path.string();
		file.erase(file.begin(), file.begin() + static_cast<std::string>("Client_Files\\").size());
		send(sock, file.c_str(), file.size(), 0);

		int bytesReceived = 0, size = 0;;
		char* buf = new char[4096]();
		ZeroMemory(buf, 4096);
		bytesReceived = recv(sock, buf, 4096, 0);

		//path
		fs::path check = std::string(buf, 0, bytesReceived);
		memset((char*)buf, 0, 4096);

		std::string data, zero;

		//size zero
		bytesReceived = recv(sock, buf, 4096, 0);
		std::stringstream integer(std::string(buf, 0, bytesReceived));
		integer >> size;
		memset((char*)buf, 0, 4096);

		//array zero
		char* buf2 = new char[size]();
		bytesReceived = recv(sock, buf2, size, 0);
		zero = (std::string(buf2, 0, bytesReceived));
		memset((char*)buf2, 0, size);
		size = 0;

		//size info
		bytesReceived = recv(sock, buf, 4096, 0);
		std::stringstream integer2(std::string(buf, 0, bytesReceived));
		integer2 >> size;
		memset((char*)buf, 0, 4096);

		//info
		buf2 = new char[size]();
		bytesReceived = recv(sock, buf2, size, 0);
		data = (std::string(buf2, 0, bytesReceived));
		memset((char*)buf2, 0, size);

		for (auto& j : data)
		{
			if (j == '0') j = '\0';
		}
		if (zero != " ")
		{
			int j = 1;
			while (j < zero.size())
			{
				std::string pos = "";
				while (j < zero.size() && zero[j] != ' ')
				{
					pos += zero[j];
					j++;
				}
				j++;

				int position = 0;
				std::stringstream integer(pos);
				integer >> position;
				data[position] = '0';
			}
		}
		fs::remove(file_path);
		std::ofstream new_file("Client_Files\\" + check.string(), std::ios::binary);
		new_file << data;
		new_file.close();
	}
}

void ClientConnection::Empty(const std::string& user) const
{
	send(sock, "EMPTY", 5, 0);
	char* buf = new char[4096]();
	ZeroMemory(buf, 4096);
	int bytesreceived = recv(sock, buf, 4096, 0);
	if (std::string(buf, 0, bytesreceived) == "1")
	{
		send(sock, user.c_str(), user.size(), 0);
	}
}

void ClientConnection::Upload(const Case& cases, fs::path old_path, std::string new_path) const
{
	new_path.erase(new_path.begin(), new_path.begin() + static_cast<std::string>("Client_Files\\").size());

	send(sock, "UPLOAD", 6, 0);

	if (cases == Case::NO_EXISTS)
	{
		send(sock, "NO_EXISTS", 9, 0);
	}
	else if (cases == Case::OVERWRITE)
	{
		send(sock, "OVERWRITE", 9, 0);
	}
	else if (cases == Case::NO_OVERWRITE)
	{
		send(sock, "NO_OVERWRITE", 12, 0);
	}

	Sleep(20);
	send(sock, old_path.string().c_str(), old_path.string().size(), 0);
	Sleep(20);
	send(sock, new_path.c_str(), new_path.size(), 0);

	if (old_path.extension().string().size())
	{
		UploadFiles(old_path);
	}
	else
	{
		UploadFolder(old_path);
	}
}

void ClientConnection::UploadFolder(fs::path old_path) const
{
	std::stack<fs::path> stack;
	stack.push(old_path);

	while (stack.size())
	{
		fs::path next = stack.top();
		stack.pop();

		for (const auto& entry : fs::directory_iterator(next))
		{
			std::string new_path = entry.path().string();
			new_path.erase(new_path.begin(), new_path.begin() + old_path.string().size());

			Sleep(50);
			send(sock, new_path.c_str(), new_path.size(), 0);

			if (entry.is_directory())
			{
				stack.push(entry.path());
			}
			else
			{
				UploadFiles(entry.path());
			}
		}
	}
	std::string a = "STOP";
	send(sock, a.c_str(), 4, 0);
}

void ClientConnection::UploadFiles(fs::path old_path) const
{
	std::ifstream new_file(old_path, std::ios::binary);
	std::vector<char> buffer(std::istreambuf_iterator<char>(new_file), {});
	std::string zero = "";

	if (!buffer.size()) buffer.push_back(' ');

	for (int j = 0; j < buffer.size(); j++)
	{
		if (buffer[j] == '0') zero += ' ' + std::to_string(j);
		if (buffer[j] == '\0') buffer[j] = '\\0';
	}
	if (!zero.size()) zero = " ";

	buffer.push_back('\0');
	int size = buffer.size() + 1;

	send(sock, std::to_string(zero.size()).c_str(), std::to_string(zero.size()).size(), 0);
	send(sock, zero.c_str(), zero.size(), 0);
	Sleep(50);
	send(sock, std::to_string(size).c_str(), std::to_string(size).size(), 0);
	Sleep(50);
	send(sock, buffer.data(), size, 0);
}

void ClientConnection::New(fs::path path) const
{
	send(sock, "NEW", 3, 0);

	std::string file = path.string();
	file.erase(file.begin(), file.begin() + static_cast<std::string>("Client_Files\\").size());

	send(sock, file.c_str(), file.size(), 0);
}

void ClientConnection::Copy(std::string file_path, std::string folderWhereToCopy) const
{
	send(sock, "COPY", 4, 0);

	std::string file = file_path;
	auto search = file.find("Client_Files\\");
	file.erase(file.begin(), file.begin() + static_cast<std::string>("Client_Files\\").size() + search);

	std::string file_toCopy = folderWhereToCopy;
	auto search_position = file_toCopy.find("Client_Files\\");
	file_toCopy.erase(file_toCopy.begin(), file_toCopy.begin() + static_cast<std::string>("Client_Files\\").size() + search_position);

	send(sock, file.c_str(), file.size(), 0);
	Sleep(20);
	send(sock, file_toCopy.c_str(), file_toCopy.size(), 0);
}

void ClientConnection::Sync(std::string old_path) const
{
	send(sock, "SYNC", 4, 0);

	std::ifstream new_file(old_path, std::ios::binary);
	std::vector<char> buffer(std::istreambuf_iterator<char>(new_file), {});
	std::string zero = "";

	if (!buffer.size()) buffer.push_back(' ');

	for (int j = 0; j < buffer.size(); j++)
	{
		if (buffer[j] == '0') zero += ' ' + std::to_string(j);
		if (buffer[j] == '\0') buffer[j] = '\\0';
	}
	if (!zero.size()) zero = " ";

	buffer.push_back('\0');
	int size = buffer.size() + 1;

	std::string path = old_path;
	path.erase(path.begin(), path.begin() + static_cast<std::string>("Client_Files\\").size());
	std::string new_path = "..\\Users_Files\\" + path;

	send(sock, old_path.c_str(), old_path.size(), 0);
	send(sock, new_path.c_str(), new_path.size(), 0);

	Sleep(50);
	send(sock, std::to_string(zero.size()).c_str(), std::to_string(zero.size()).size(), 0);
	send(sock, zero.c_str(), zero.size(), 0);
	Sleep(50);
	send(sock, std::to_string(size).c_str(), std::to_string(size).size(), 0);
	Sleep(50);
	send(sock, buffer.data(), size, 0);
}

void ClientConnection::Move(std::string file_path) const
{
	send(sock, "MOVE", 4, 0);

	std::string file = file_path;
	auto search = file.find("Client_Files\\");
	file.erase(file.begin(), file.begin() + static_cast<std::string>("Client_Files\\").size() + search);

	send(sock, file.c_str(), file.size(), 0);
}
