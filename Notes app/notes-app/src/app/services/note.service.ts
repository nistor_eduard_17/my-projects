import { Injectable } from '@angular/core';
import { Note } from '../note';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { HomeComponent } from '../home/home.component';


@Injectable({
  providedIn: 'root'
})
export class NoteService {

  color: string[] = [];

  // notes: Note[] = [
  //   {
  //     id: 1,
  //     title: "Your first note!",
  //     description: "Hello there...",
  //     color: "red",
  //     categoryId: "1"
  //   },
  //   {
  //     id: 2,
  //     title: "Second note",
  //     description: "This is the description for the second note",
  //     color: "blue",
  //     categoryId: "2"
  //   },

  //   {
  //     id: 3,
  //     title: "Third note",
  //     description: "This is the description for the third note",
  //     color: "green",
  //     categoryId: "3"
  //   },


  // ];

  readonly baseUrl = "https://localhost:44362/notes";
  readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };


  constructor(private httpClient: HttpClient) { }

  getNotes(): Observable<Note[]> {
    return this.httpClient.get<Note[]>(this.baseUrl, this.httpOptions);

  }

  getFilteredNotes(categId: string) {
    return this.httpClient.get<Note[]>(this.baseUrl , this.httpOptions).pipe(map((notes: Note[]) => {
      console.log(notes.length);
      return notes.filter((note) =>
        note.categoryId === categId
      )
    }));
  }

  addNote(note: Note) {
    return this.httpClient.post(this.baseUrl , note, this.httpOptions);
  }

  removeNote(index: string): Observable<Note[]> {
    return this.httpClient.delete<Note[]>(this.baseUrl+'/{'+index+'}' , this.httpOptions);
  }

  ReceivedNote(index: string) {
    return this.httpClient.get<Note[]>(this.baseUrl, this.httpOptions).pipe(map((notes: Note[]) => {
      return notes.filter((note) =>
        note.id === index
      )
    }));
  }

  UpdateEditedNote(index:string){
      return this.httpClient.put<Note[]>(this.baseUrl+'/{'+index+'}',HomeComponent.rNote,this.httpOptions);
  }

  UpdateNote(index:string,note:Note){
    return this.httpClient.put<Note[]>(this.baseUrl+'/{'+index+'}',note,this.httpOptions);
  }

  serviceCall() {
    console.log("Note service was called");
  }

  serviceCall2() {
    console.log("Note service2 was called");
  }
 
  getNotesWithSearchedContent(content: string) {
    return this.httpClient.get<Note[]>(this.baseUrl , this.httpOptions).pipe(map((notes: Note[]) => {
      console.log(notes.length);
      return notes.filter((note) =>
        note.title.includes(content) || note.description.includes(content)
      )
    }));

  }
}
