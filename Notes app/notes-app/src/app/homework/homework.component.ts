import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-homework',
  templateUrl: './homework.component.html',
  styleUrls: ['./homework.component.scss']
})
export class HomeworkComponent implements OnInit {

   backgroundColor:string="";
   typedColor:string="";
  constructor(private _router: Router, private _activatedRoute: ActivatedRoute) { }

  ngOnInit(): void{
    this._activatedRoute.params.subscribe(parameter=>{
      console.log(parameter)
    })
  }

  set_color()
  {
    const c=new Option().style;
    c.color=this.typedColor;
    if(c.color==this.typedColor.toLowerCase())
      this.backgroundColor=this.typedColor;
      else
      this.backgroundColor="";
  }

}
