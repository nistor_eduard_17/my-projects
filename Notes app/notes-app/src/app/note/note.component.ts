import { DomElementSchemaRegistry } from '@angular/compiler';
import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Note } from '../note';
import { NoteService } from '../services/note.service';
import * as myGlobals from 'globals'
import { EditComponent } from '../edit/edit.component';
import { HomeComponent } from '../home/home.component';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AddNoteComponent } from '../add-note/add-note.component';
import { Router } from '@angular/router';


@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss']
})
export class NoteComponent implements OnInit,OnChanges {

  notes: Note[]=[];
  currentNote:Note;
  comp:EditComponent;
  @Input() selectedCategoryId: string="";
  @Input() searchedContent:string="";

  constructor(private service:NoteService,public dialog:MatDialog, private _router:Router) { }

  ngOnInit(): void {
    this.service.getNotes().subscribe((notes:Note[])=>{
        this.notes=notes;

    })
    ;
  }

  ngOnChanges():void{
    if(this.selectedCategoryId)
      if(this.selectedCategoryId==='1' || this.selectedCategoryId==='2' || this.selectedCategoryId==='3')
    this.service.getFilteredNotes(this.selectedCategoryId).subscribe((notes:Note[])=> this.notes=notes);
    else
    if(this.searchedContent){
      this.service.getNotesWithSearchedContent(this.searchedContent).subscribe((notes:Note[])=> this.notes=notes);
    }
    else
    this.service.getNotes().subscribe((notes:Note[])=> this.notes=notes);
  }
RemoveNote(index:string):void{
   this.service.removeNote(index).subscribe(()=> {
      this.notes.forEach((element,i) => {
        if(element.id===index)
           this.notes.splice(i,1);
      })
    })
}

openDialog(index:string) {

  HomeComponent.rNote=this.notes.find(note=>note.id===index);

  const dialogConfig = new MatDialogConfig();

  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.data={
    title:HomeComponent.rNote.title,
    description:HomeComponent.rNote.description,
    color:HomeComponent.rNote.color,
    categoryId:HomeComponent.rNote.categoryId
  }
  this.dialog.open(EditComponent, dialogConfig);
}

toDo(index:string) //1
{
  console.log("Da")
  let note_index:any=this.notes.findIndex((note)=>note.id===index);
  console.log(note_index);
  this.notes[note_index].categoryId="1";
  console.log(this.notes[note_index].categoryId);
  this.service.UpdateNote(index,this.notes[note_index]).subscribe();
}

doing(index:string) //3
{
  let note_index:any=this.notes.findIndex((note)=>note.id===index);
  this.notes[note_index].categoryId="3";
  this.service.UpdateNote(index,this.notes[note_index]).subscribe();
}

done(index:string) //2
{
  let note_index:any=this.notes.findIndex((note)=>note.id===index);
  this.notes[note_index].categoryId="2";
  this.service.UpdateNote(index,this.notes[note_index]).subscribe();
}


}

