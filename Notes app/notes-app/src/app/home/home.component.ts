import { Component, OnInit } from '@angular/core';
import { Note } from '../note';
import * as myGlobals from 'globals'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public static rNote:Note;
  categoryId: string="";
  searchContent:string="";
  constructor() { }

  ngOnInit(): void {
  }

  receiveCategory(categId: string) {
    this.categoryId = categId;
  }

  receiveContent(content:string){
    this.searchContent=content;
  }

}
