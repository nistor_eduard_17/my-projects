﻿using Microsoft.AspNetCore.Mvc;
using NotesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CategoriesController : Controller
    {
        static List<Category> _categories = new List<Category>
        {
            new Category{Name="To Do", Id=1},
            new Category{Name="Done",Id=2},
            new Category{Name="Doing", Id=3}
        };

        /// <summary>
        /// Get type of categories
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetCategories()
        {
            return Ok(_categories);
        }

        /// <summary>
        /// Type cateogry ID in order to get category type
        /// </summary>
        /// <returns></returns>
        [HttpGet("id")]
        public IActionResult GetCategory(int id)
        {
            if (id <= 0 || id >= 4)
                return NotFound();
            else
                return Ok(_categories[id - 1]);
        }

        ///<summary>
        ///Post another category
        ///</summary>
        [HttpPost]
        public IActionResult AddNewCategory(Category category)
        {
            _categories.Add(category);
            return Ok(_categories);
        }

        ///<summary>
        ///Delete one category
        ///</summary>
        [HttpDelete]
        public IActionResult DeleteOneCategory(int id)
        {
            _categories.RemoveAt(id-1);
            return Ok(_categories);
        }
    }
}
