﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using NotesAPI.Models;
using System.Threading.Tasks;
using NotesAPI.Services;

namespace NotesAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class NotesController : ControllerBase
    {
        INoteCollectionService _noteCollectionService;

        public NotesController(INoteCollectionService noteCollectionService)
        {
            _noteCollectionService = noteCollectionService ?? throw new ArgumentNullException(nameof(noteCollectionService));
        }

        [HttpGet]
        public async Task<IActionResult> GetNotes()
        {
            return Ok(await _noteCollectionService.GetAll());
        }

        [HttpGet("noteId")]
        public async Task<IActionResult> GetNotesById(Guid noteId)
        {
            return Ok(await _noteCollectionService.Get(noteId));
        }

        [HttpGet]
        [Route("owner/{ownerId}")]
        public async Task<IActionResult> GetNotesByOwnerId(Guid ownerId)
        {
            return Ok(await _noteCollectionService.GetNotesByOwnerId(ownerId));
        }

        [HttpPost]
        public async Task<IActionResult> PostNote([FromBody] Note note)
        {
            if (note == null)
                return BadRequest("Note is null");
            await _noteCollectionService.Create(note);
            return Ok(await _noteCollectionService.GetAll());
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateNote(Guid id, [FromBody] Note note)
        {
            if (!await _noteCollectionService.Update(id, note))
            {
                return await PostNote(note);
            }

            return Ok(await _noteCollectionService.GetAll());
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteNote(Guid id)
        {
            if (await _noteCollectionService.Delete(id) == false)
                return NotFound("The note was not found");

            return Ok(await _noteCollectionService.GetAll());
        }

    }
}


