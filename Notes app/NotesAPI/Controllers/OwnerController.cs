﻿using Microsoft.AspNetCore.Mvc;
using NotesAPI.Models;
using NotesAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Controllers
{

    [Route("[controller]")]
    [ApiController]
    public class OwnerController : ControllerBase
    {
        IOwnerCollectionService _ownerCollectionService;

        public OwnerController(IOwnerCollectionService ownerCollectionService)
        {
            _ownerCollectionService = ownerCollectionService ?? throw new ArgumentNullException(nameof(ownerCollectionService));
        }

        /// <summary>
        /// Get owners
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetOwners()
        {
            return Ok(await _ownerCollectionService.GetAll());
        }

        [HttpPost]
        public async Task<IActionResult> AddNewOwner([FromBody] Owner owner)
        {
            if (owner == null)
                return BadRequest("Owner details required");

            await _ownerCollectionService.Create(owner);
            return Ok(await _ownerCollectionService.GetAll());
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateOwner(Guid id, [FromBody] Owner owner)
        {
            if (owner == null)
                return BadRequest("The owner can't be empty");

            if (!await _ownerCollectionService.Update(id, owner))
                return await AddNewOwner(owner);

            return Ok(await _ownerCollectionService.GetAll());
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOwner(Guid id)
        {
            if (await _ownerCollectionService.Delete(id) == false)
                return NotFound("The owner was not found");

            return Ok(await _ownerCollectionService.GetAll());
        }
    }
}
