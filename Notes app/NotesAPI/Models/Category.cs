﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Models
{
    public class Category
    {
        public string Name { get; set; }
        public int Id { get; set; }
    }
}
