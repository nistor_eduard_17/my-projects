﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JocurileCopilariei
{
    public partial class Snake : Form
    {

        SnakeGame game = new SnakeGame();
        public Snake()
        {
            InitializeComponent();
        }

        private void Snake_Load(object sender, EventArgs e)
        {

            gameTimer.Interval = 500 / game.getSpeed();
            gameTimer.Tick += updateScreen;
            gameTimer.Start();
            game.startGame();
            game.setBorderHeight(gameBoard.Height - 10);
            game.setBorderWidth(gameBoard.Width - 10);
        }

        private void updateScreen(object sender,EventArgs e)
        {
            game.movePlayer();
            gameOverText.Visible = false;
            gameBoard.Invalidate();
            
        }

        private void updateGraphics(object sender, PaintEventArgs e)
        {
            Graphics shape = e.Graphics;
            if (game.getGameOver()==false)
            {
                label1.Text = "Score: " + game.getScore();
                Brush SnakeColor;
                int width = game.getWidth();
                int height = game.getHeight();
                for (int i = 0; i < game.Snake.Count; i++)
                {
                    if (i == 0)
                        SnakeColor = Brushes.Black;
                    else
                        SnakeColor = Brushes.DarkGreen;
                    shape.FillEllipse(SnakeColor,
                                        new Rectangle(game.Snake[i].x * width, game.Snake[i].y *height, height, width));
                    shape.FillEllipse(Brushes.Red,
                                        new Rectangle(game.food.x *width, game.food.y * height, height, width));
                }
                return;

            }
            string gameOver = "Game Over ! \n " + "Final Score : " + game.getScore() + " \n Press Enter to restart. \n";
            gameOverText.Text = gameOver;
            gameOverText.Visible = true;
            return;
        }

            private void Snake_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                GameSelection gameSelection = new GameSelection();
                gameSelection.Show();
            }
        }

        private void Snake_KeyDown(object sender, KeyEventArgs e)
        {
            game.pressedKey.updateState(e.KeyCode, true);
            game.updateMoves(e.KeyCode);
        }

        private void Snake_KeyUp(object sender, KeyEventArgs e)
        {
            game.pressedKey.updateState(e.KeyCode, false);
        }

        private void eXITToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gameBoard_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
