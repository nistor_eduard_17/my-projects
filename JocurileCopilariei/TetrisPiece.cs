﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JocurileCopilariei
{
    class TetrisPiece
    {
        char pieceType;
        int length;
        public TetrisPiece()
        {
        }
        enum TypeOfPieces
        {
            Line,
            Square,
            L,
            T,
            Z
        };

        int[,] CreateLine(int rotation)
        {
            int[,] line = new int[,] { { 1, 1, 1, 1 }, { -1, -1, -1, -1 } };
            length = 4;
            pieceType = 'l';
            if (rotation == 1)
            {
                line = new int[,] { { 1, -1 }, { 1, -1 }, { 1, -1 }, { 1, -1 } };
                length = 1;
            }

            return line;
        }

        int[,] CreateSquare()
        {
            int[,] square = new int[,] { { 2, 2 }, { 2, 2 } };
            pieceType = 's';
            length = 2;
            return square;
        }

        int[,] CreateZ(int rotation)
        {
            int[,] Z = new int[,] { { 5, 5, -1 }, { -1, 5, 5 } };
            pieceType = 'Z';
            length = 2;
            if (rotation == 1)
            {
                Z = new int[,] { { -1, 5 }, { 5, 5 }, { 5, -1 } };
                length = 1;

            }
            return Z;
        }

        int[,] CreateT(int rotation)
        {
            int[,] T = new int[,] { { -1, 4, -1 }, { 4, 4, 4 } };
            pieceType = 'T';
            length = 3;
            switch (rotation)
            {
                case 1:
                    T = new int[,] { { 4, -1 }, { 4, 4 }, { 4, -1 } };
                    length = 1;
                    break;
                case 2:
                    T = new int[,] { { 4, 4, 4 }, { -1, 4, -1 } };
                    length = 1;
                    break;
                case 3:
                    T = new int[,] { { -1, 4 }, { 4, 4 }, { -1, 4 } };
                    length = 1;
                    break;
            }
            return T;
        }

        int[,] CreateL(int rotation)
        {
            int[,] L = new int[,] { { 3, -1 }, { 3, -1 }, { 3, 3 } };
            pieceType = 'L';
            length = 2;
            switch (rotation)
            {
                case 1:
                    L = new int[,] { { 3, 3, 3 }, { 3, -1, -1 } };
                    length = 1;
                    break;
                case 2:
                    L = new int[,] { { 3, 3 }, { -1, 3 }, { -1, 3 } };
                    length = 1;
                    break;
                case 3:
                    L = new int[,] { { -1, -1, 3 }, { 3, 3, 3 } };
                    length = 3;
                    break;
            }
            return L;
        }

        public int[,] CreatePiece(int[,] piece, string pieceType, int rotation)
        {
            switch (pieceType)
            {
                case "Line":
                    piece = CreateLine(rotation);
                    break;
                case "Z":
                    piece = CreateZ(rotation);
                    break;
                case "L":
                    piece = CreateL(rotation);
                    break;
                case "Sqare":
                    piece = CreateSquare();
                    break;
                case "T":
                    piece = CreateT(rotation);
                    break;
            }
            return piece;
        }

        public int[,] GeneratePiece()
        {
            int value = RandomNumber.Between(0, 4);

            int[,] matrix = new int[,] { { -1 } };
            switch (value)
            {
                case 0:
                    return CreateLine(0);
                    break;
                case 1:
                    return CreateSquare();
                    break;
                case 2:
                    return CreateL(0);
                    break;
                case 3:
                    return CreateT(0);
                    break;
                case 4:
                    return CreateZ(0);
                    break;

            }
            return matrix;
        }

        public char getPieceType()
        {
            return pieceType;
        }

        public int getLength()
        {
            return length;
        }

        public void setPieceType(char pieceType)
        {
            this.pieceType = pieceType;
        }
    }
}
