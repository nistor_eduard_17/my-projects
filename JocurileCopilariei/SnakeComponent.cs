﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JocurileCopilariei
{
    class SnakeComponent
    {
        public int x;
        public int y;

        public SnakeComponent()
        {
            this.x = 0;
            this.y = 0;
        }

        public SnakeComponent(int x,int y)
        {
            this.x = x;
            this.y = y;
        }
        public void setX(int x) { this.x = x; }
        public void setY(int y) { this.y = y; }
        public int getX() { return x; }
        public int getY() { return y; }
    }
}
