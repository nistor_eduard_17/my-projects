﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JocurileCopilariei
{
    public partial class GameSelection : Form
    {
        public GameSelection()
        {
            InitializeComponent();
        }

        private void GameSelection_Load(object sender, EventArgs e)
        {

        }
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            this.Close();
            mainWindow.Show();
        }

        private void TicTacToe_Click(object sender, EventArgs e)
        {
            TicTacToe ticTacToe = new TicTacToe();
            ticTacToe.Show();
            this.Hide();
        }

        private void Hangman_Click(object sender, EventArgs e)
        {
            Hangman hangman = new Hangman();
            hangman.Show();
            this.Hide();
        }

        private void Snake_Click(object sender, EventArgs e)
        {
            Snake snakeGame = new Snake();
            snakeGame.Show();
            this.Hide();
        }

        private void Tetris_Click(object sender, EventArgs e)
        {
            Tetris tetris = new Tetris();
            tetris.Show();
            this.Hide();
        }

        private void label3_Click(object sender, EventArgs e)
        {
            Tetris tetris = new Tetris();
            tetris.Show();
            this.Hide();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            Snake snakeGame = new Snake();
            snakeGame.Show();
            this.Hide();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Hangman hangman = new Hangman();
            hangman.Show();
            this.Hide();
        }

        private void LabelTicTacToe_Click(object sender, EventArgs e)
        {
            TicTacToe ticTacToe = new TicTacToe();
            ticTacToe.Show();
            this.Hide();
        }
    }
}
