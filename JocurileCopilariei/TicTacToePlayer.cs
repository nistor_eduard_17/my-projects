﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JocurileCopilariei
{
    public class TicTacToePlayer
    {
        private char name;

        public TicTacToePlayer()
        {
            this.name = 'X';
        }
        public char getName()
        {
            return name;
        }

        public void setName(char name)
        {
            this.name = name;
        }

        public void changePlayer(int value)
        {
            if (value % 2 == 0)
            {
                this.name = 'X';
                return;
            }
            this.name = '0';
            return;
        }
    }
}
