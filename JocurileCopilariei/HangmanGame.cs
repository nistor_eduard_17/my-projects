﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JocurileCopilariei
{
    class HangmanGame
    {
        HangmanWords words;
        private string currentWord;
        public int score, mistakes,scoreWasUpdated;
        private char dificulty='e';
        Dictionary<char, int> insertedCharacters;
        HashSet<char> userInsertedCharacters;

        public int getMistakes()
        {
            return mistakes;
        }

        public void setDifficulty(char dif)
        {
            dificulty = dif;
        }

        public char getDifficulty()
        {
            return dificulty;
        }

        public int getScore()
        {
            return score;
        }
        public HangmanGame(string difficulty,int previousScore)
        {
            words = new HangmanWords();
            scoreWasUpdated = 0;
            score = previousScore;
            mistakes = 0;
            insertedCharacters = new Dictionary<char, int>();
            userInsertedCharacters = new HashSet<char>();

        }

        public string getCurrentWord()
        {
            return this.currentWord;
        }

        public HashSet<char> getUserCharacters()
        {
            return userInsertedCharacters;
        }
        public int getNumberOfMistakes()
        {
            return mistakes;
        }
        public void setDificultyAndGenerateWord(string dificulty)
        {
            this.dificulty = dificulty[0];
            char.ToLower(this.dificulty);
            currentWord = words.getWord(this.dificulty);

            foreach (char character in currentWord)
            {
                if (insertedCharacters.ContainsKey(character))
                {

                    insertedCharacters[character]++;
                }
                if (!insertedCharacters.ContainsKey(character))
                {
                    insertedCharacters.Add(character, 1);
                }
            }
            insertedCharacters[currentWord[0]] -= 1;
            insertedCharacters[currentWord[currentWord.Length - 1]] -= 1;
        }

        public bool checkIfTheStringMatchWithProperties(string text)
        {
            if (text.Length > 1 || text.Length == 0)
                return false;
            if (!text.All(char.IsLetter))
                return false;
            if (text == " ")
                return false;
            return true;
        }
        public List<int> getPositionOfFoundChar(char character)
        {
            List<int> position = new List<int>();
            character = char.ToUpper(character);
            for (int index = 0; index < currentWord.Length; ++index)
                if (index != 0 && index != currentWord.Length - 1 && character == currentWord[index])
                    position.Add(index);
            return position;

        }

        public bool checkIfCharacterExistsInWord(char searchedCharacter)
        {

            searchedCharacter = char.ToUpper(searchedCharacter);
            userInsertedCharacters.Add(searchedCharacter);
            foreach (char character in currentWord)
                if (searchedCharacter == character)
                {
                    if (insertedCharacters[searchedCharacter] != 0)
                    {
                        insertedCharacters[searchedCharacter] -= 1;
                    }

                }
            if (insertedCharacters.ContainsKey(searchedCharacter))
                return true;

            updateMistakes(searchedCharacter);
            return false;

        }

        private void updateMistakes(char searchedCharacter)
        {
            if (!insertedCharacters.ContainsKey(searchedCharacter))
            {
                mistakes++;
                insertedCharacters[searchedCharacter] = 1;
                return;
            }

        }

        private bool isWin()
        {
            foreach (KeyValuePair<char, int> pair in insertedCharacters)
                if (pair.Value != 0 && currentWord.Contains(pair.Key))
                    return false;
            if (scoreWasUpdated == 0)
            {
                score++;
                scoreWasUpdated = 1;
            }
            return true;
        }

        public string isGameStillInProgress()
        {
            if (isWin())
            {
                
                return "WIN!";
            }

            if (isGameOver())
            {
                return "Game Over! \n The word was " + currentWord + ".";
            }

            return "";
        }

        private bool isGameOver()
        {
            if (mistakes == 6)
                return true;

            return false;

        }








    }
}
