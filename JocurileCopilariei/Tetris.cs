﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JocurileCopilariei
{
    public partial class Tetris : Form
    {

        TetrisGame game = new TetrisGame();
        TetrisKeyPress key = new TetrisKeyPress();
        Button[,] pieceAsButton;
        int[,] gameBoard;
        public Tetris()
        {
            InitializeComponent();
        }

        private void Tetris_Load(object sender, EventArgs e)
        {
            gameTimer.Interval = game.Speed;
            gameTimer.Tick += updateScreen;
            gameTimer.Start();

        }

        Color SetColor(int value)
        {
            if (value == 1)
                return Color.Red;
            if (value == 2)
                return Color.Green;
            if (value == 3)
                return Color.Orange;
            if (value == 4)
                return Color.Violet;
            if (value == 5)
                return Color.Yellow;
            return Color.Transparent;
        }
        void DisplayPiece(int[,] matrix, PictureBox picture)
        {
            picture.Controls.Clear();
            int lineSize = matrix.GetLength(0);
            int columnSize = matrix.GetLength(1);
            pieceAsButton = new Button[lineSize, columnSize];
            int valueLine = 0, valueColumn = 0;
            //if (picture == nextPiece)
            //    valueLine = valueColumn = 1;

            for (int line = 0; line < lineSize; ++line)
            {
                for (int column = 0; column < columnSize; ++column)
                {
                    if (matrix[line, column] != -1)
                    {
                        Button button = new Button();
                        button.BackColor = SetColor(matrix[line, column]);
                        button.FlatStyle = FlatStyle.Flat;
                        button.FlatAppearance.BorderSize = 1;
                        button.FlatAppearance.BorderColor = Color.Black;
                        button.SetBounds(35 * (valueColumn + column), 35 * (valueLine + line), 35, 35);
                        button.Enabled = false;
                        button.BringToFront();
                        pieceAsButton[line, column] = button;
                        picture.Controls.Add(button);

                    }
                }
            }


        }

        private void updateScreen(object sender, EventArgs e)
        {
            if (game.IsGameOver() == false)
            {
                game.movePiece();

                gameBoard = game.GetGameBoard();
                DisplayPiece(gameBoard, pictureBox1);
                DisplayPiece(game.GetNextPiece(), nextPiece);

                if (game.PieceReachedDestination())
                {
                    Score.Text = "Score:" + game.GetScore();
                }

                return;
            }
            return;
        }

        private void updateGraphics(object sender, PaintEventArgs e)
        {

            if (game.IsGameOver())
            {
                MessageBox.Show("Game Over!");
                gameTimer.Stop();
                pieceAsButton = null;
                return;
            }
            return;

        }

        private void Tetris_KeyPress(object sender, KeyEventArgs e)
        {

            key.updateState(e.KeyCode, true);
            game.updateMoves(e.KeyCode);
        }

        private void Tetris_KeyRelease(object sender, KeyEventArgs e)
        {
            key.updateState(e.KeyCode, false);

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void newGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            game = new TetrisGame();
            game.Reset();
            Score.Text = "Score:" + game.GetScore();
            nextPiece.Controls.Clear();
            pictureBox1.Controls.Clear();
            gameTimer.Start();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Tetris_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                GameSelection gameSelection = new GameSelection();
                gameSelection.Show();
            }
        }
    }
}
