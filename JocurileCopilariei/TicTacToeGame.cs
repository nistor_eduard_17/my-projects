﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JocurileCopilariei
{
    public class TicTacToeGame
    {
        public TicTacToePlayer player = new TicTacToePlayer();
        private int turns;
        private bool existsWin;
        public int x_Score;
        public int o_Score;
        public int draws;
        public int matrixSize;
        public char[,] gameBoard;



        public TicTacToeGame(int size)
        {

            matrixSize = size;
            this.turns = 0;
            this.existsWin = false;
            this.x_Score = 0;
            this.o_Score = 0;
            this.draws = 0;
            gameBoard = new char[size, size];
        }

        public void setSize(int size)
        {
            matrixSize = size;
        }


        public int getSize()
        {
            return matrixSize;
        }

        public void insertIntogameBoard(int line, int column, char c)
        {
            gameBoard[line, column] = c;
        }

        public int getNumberOfDraws()
        {
            return this.draws;
        }

        public int getXScore()
        {
            return x_Score;
        }

        public int getOScore()
        {
            return o_Score;
        }

        public char makeMove()
        {
            this.turns++;
            player.changePlayer(turns);
            return player.getName();
        }

        public bool isDraw(int maxMoves)
        {
            if (this.existsWin)
            {
                return false;
            }

            if (this.turns == maxMoves)
            {
                this.draws++;
                return true;
            }

            return false;
        }


        public void newGameKeepScores()
        {
            player = new TicTacToePlayer();
            turns = 0;
            existsWin = false;
            gameBoard = new char[getSize(), getSize()];
        }
        private bool isRowMatching(char searchedChar, int index)
        {
            for (int column = 0; column < matrixSize; column++)
                if (gameBoard[index, column] != searchedChar)
                    return false;
            modifyScore(searchedChar);
            return true;
        }

        private bool isColumnMatching(char searchedChar, int index)
        {
            for (int line = 0; line < matrixSize; line++)
                if (gameBoard[line, index] != searchedChar)
                    return false;
            modifyScore(searchedChar);
            return true;
        }

        private bool isPrincipalDiagonalMatching(char searchedChar)
        {
            for (int index = 0; index < matrixSize; index++)
                if (gameBoard[index, index] != searchedChar)
                    return false;
            modifyScore(searchedChar);
            return true;
        }

        private bool isSecondDiagonalMatching(char searchedChar)
        {
            for (int index = 0; index < matrixSize; index++)
                if (gameBoard[index, matrixSize - index - 1] != searchedChar)
                    return false;
            modifyScore(searchedChar);
            return true;
        }
        public bool isWin(char searchedChar)
        {
            if (turns >= 2 * matrixSize - 1)
            {
                for (int line = 0; line < matrixSize; line++)
                    if (isRowMatching(searchedChar, line))
                        return true;
                for (int column = 0; column < matrixSize; column++)
                    if (isColumnMatching(searchedChar, column))
                        return true;

                if (isPrincipalDiagonalMatching(searchedChar))
                    return true;
                if (isSecondDiagonalMatching(searchedChar))
                    return true;
            }

            return false;
        }
        private void modifyScore(char cellChar)
        {
            if (cellChar == 'X')
            {
                x_Score++;
                return;
            }

            o_Score++;
            return;


        }
    }
}
