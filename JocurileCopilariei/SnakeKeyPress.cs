﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Threading.Tasks;

namespace JocurileCopilariei
{
    class SnakeKeyPress
    {
        public Dictionary<Keys, bool> keys = new Dictionary<Keys, bool>();

        public bool isKeyPressed(Keys key)
        {
            if (keys.ContainsKey(key)==null)
                return false;
            return true;
        }

        public void updateState(Keys key,bool state)
        {
            keys[key] = state;
        }

    }
}
