﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JocurileCopilariei
{
    class HangmanWords
    {
        
        SortedSet<string> easyWords;
        SortedSet<string> mediumWords;
        SortedSet<string> hardWords;

        public HangmanWords()
        {
            easyWords = new SortedSet<string> { "MERE", "FLOARE","FLUTURE", "COPIL", "CASA", "STRADA", "FOTBAL", "GHIVECI", "ACADEA", "BOMBOANA", "STICLA" };
            mediumWords = new SortedSet<string> { "ALABASTRU", "AMFITEATRU", "CORIANDRU", "IMPORTATOR", "BIBLIOGRAFIE", "METALURGIE", "NEOLOGISM", "SPECTRU", "RADIOLOGIE", "CICLIC" };
            hardWords = new SortedSet<string> { "COMPLEXITATE", "JARGON", "TEMPERAMENT", "XILOFON", "TRANSPLANT", "ELOCVENT", "REGENERATOR", "SINTAXA", "INCAPABIL", "OTORINOLARINGOLOGIE" };
        }

        private int generateRandomValue(char c)
        {
            Random rand = new Random();
            if (c == 'e')
                return rand.Next(0, easyWords.Count);

            if (c == 'm')
                return rand.Next(0, mediumWords.Count);

            return rand.Next(0, hardWords.Count);
        }
        public string getWord(char c)
        {
            if (c == 'e')
                return easyWords.ElementAt(generateRandomValue(c));
            if (c == 'm')
                return mediumWords.ElementAt(generateRandomValue(c));

            return hardWords.ElementAt(generateRandomValue(c));
        }

    }
}
