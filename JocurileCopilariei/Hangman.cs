﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JocurileCopilariei
{
    public partial class Hangman : Form
    {
        Graphics graphics;
        Button[] buttons;
        HangmanGame game;
        int coordinateX, coordinateDisplayChar, score = 0;
        string difficulty = "easy";
        public Hangman()
        {
            InitializeComponent();
            Reset();
            getPlayerChar.BorderStyle = BorderStyle.None;
            
        }

        void DrawHangPost()
        {
            graphics.ResetTransform();
            graphics.DrawLine(new Pen(Color.White, 10), new Point(290, 40), new Point(385, 150)); 
            graphics.DrawLine(new Pen(Color.White, 10), new Point(375, 25), new Point(375, 375)); 
            graphics.DrawLine(new Pen(Color.White, 10), new Point(250, 375), new Point(500, 375));
            graphics.DrawLine(new Pen(Color.White, 10), new Point(100, 40), new Point(400, 40)); 
            graphics.DrawLine(new Pen(Color.White, 10), new Point(114, 40), new Point(114, 140));
        }

        void DrawHead(int mistakes)
        {
            graphics.FillEllipse(new System.Drawing.SolidBrush(System.Drawing.Color.Wheat), new Rectangle(30, 75, 85, 85));// head

            graphics.DrawEllipse(new Pen(Color.Black, 3), new Rectangle(40, 90, 20, 25)); //left eye
            graphics.FillEllipse(new System.Drawing.SolidBrush(System.Drawing.Color.Black), new Rectangle(43, 103, 10, 10)); //middle left eye

            graphics.DrawEllipse(new Pen(Color.Black, 3), new Rectangle(75, 90, 20, 25)); // right eye
            graphics.FillEllipse(new System.Drawing.SolidBrush(System.Drawing.Color.Black), new Rectangle(78, 103, 10, 10)); //middle right eye

            if (mistakes <= 2)
            {
                graphics.DrawArc(new Pen(Color.Black, 3), 45, 65, 45, 75, 55, 65);   // mouth
                return;
            }

            if (mistakes >= 3 && mistakes< 5)
            {
                graphics.DrawLine(new Pen(Color.Black, 3), new Point(58, 132), new Point(80, 132));   // mouth
                return;
            }
            graphics.DrawArc(new Pen(Color.Black, 3), 45, 120, 45, 90, 235, 65);   // mouth            



        }

        void DrawBody()
        {
            graphics.FillEllipse(new System.Drawing.SolidBrush(System.Drawing.Color.Wheat), new Rectangle(67, 135, 75, 165));
            graphics.FillEllipse(new System.Drawing.SolidBrush(System.Drawing.Color.YellowGreen), new Rectangle(67, 145, 78, 165));
        }

        void DrawLeftArm()
        {
            graphics.TranslateTransform(100.0F, 0.0F);
            graphics.RotateTransform(280.0F);
            graphics.FillEllipse(new System.Drawing.SolidBrush(System.Drawing.Color.Wheat), 63, 25, 35, 110);
            graphics.FillEllipse(new System.Drawing.SolidBrush(System.Drawing.Color.YellowGreen), 63, 25, 35, 60);

        }

        void DrawRightArm()
        {

            graphics.TranslateTransform(100.0F, 0.0F);
            graphics.RotateTransform(125.0F);
            graphics.FillEllipse(new System.Drawing.SolidBrush(System.Drawing.Color.Wheat), 90, -210, 35, 125);
            graphics.FillEllipse(new System.Drawing.SolidBrush(System.Drawing.Color.YellowGreen), 90, -150, 35, 60);

        }

        void DrawLeftLeg()
        {
            graphics.TranslateTransform(100.0F, 0.0F);
            graphics.RotateTransform(-35.0F);
            graphics.FillEllipse(new System.Drawing.SolidBrush(System.Drawing.Color.Wheat), -15, 115, 35, 140);
            graphics.FillEllipse(new System.Drawing.SolidBrush(System.Drawing.Color.YellowGreen), -15, 115, 35, 65);

        }

        void DrawRightLeg()
        {
            graphics.TranslateTransform(100.0F, 0.0F);
            graphics.RotateTransform(-25.0F);
            graphics.FillEllipse(new System.Drawing.SolidBrush(System.Drawing.Color.Wheat), -130, 68, 35, 140);
            graphics.FillEllipse(new System.Drawing.SolidBrush(System.Drawing.Color.YellowGreen), -130, 68, 35, 65);
        }
        void Reset()
        {
            coordinateX = 25;
            coordinateDisplayChar = 25;
            getPlayerChar.Enabled = true;
            game = new HangmanGame(difficulty, score);
            game.setDificultyAndGenerateWord(difficulty);
            generateButtons();
            usedChar.Controls.Clear();
            hangmanDrawing.Controls.Clear();
            graphics = hangmanDrawing.CreateGraphics();
        }

        void DisplayInsertedCharacters()
        {
            Button button = new Button();
            coordinateDisplayChar = 25;
            HashSet<char> userCharacters = game.getUserCharacters();
            string currentWord = game.getCurrentWord();
            foreach (char element in userCharacters)
            {
                button.Font = new Font("Lucida Bright", 13, FontStyle.Italic ^ FontStyle.Bold);
                button.BackColor = Color.Gold;
                button.Text = element.ToString();
                button.FlatStyle = FlatStyle.Flat;
                button.FlatAppearance.BorderSize = 0;
                button.SetBounds(coordinateDisplayChar, 0, 50, 50);
                usedChar.Controls.Add(button);
                coordinateDisplayChar += 60;

            }
        }

        private void updateScores()
        {
            numberOfMistakes.Text = "Mistakes: " + game.getNumberOfMistakes();

        }

        private void modifyButton(Button button, string text)
        {
            button.Text = text;
            button.Font = new Font("Lucida Bright", 13, FontStyle.Italic ^ FontStyle.Bold);
            button.FlatStyle = FlatStyle.Flat;
            button.FlatAppearance.BorderSize = 0;
            panel1.Controls.Add(button);
            return;

        }
        private void generateButtons()
        {
            panel1.Controls.Clear();
            int length = game.getCurrentWord().Length;
            buttons = new Button[length];

            for (int index = 0; index < length; ++index)
            {
                buttons[index] = new Button();
                buttons[index].BackColor = Color.Coral;
                buttons[index].FlatStyle = FlatStyle.Flat;
                buttons[index].FlatAppearance.BorderSize = 0;
                buttons[index].SetBounds(coordinateX, 0, 50, 50);

                if (index == 0)
                    modifyButton(buttons[0], game.getCurrentWord().ElementAt(0).ToString());

                if (index == length - 1)
                    modifyButton(buttons[index], game.getCurrentWord().ElementAt(index).ToString());


                if (index != 0 && index != length - 1)
                    modifyButton(buttons[index], "");

                coordinateX += 70;

            }

        }

        private void displayCharactersInButtons(List<int> position)
        {

            foreach (int element in position)
                modifyButton(buttons[element], game.getCurrentWord().ElementAt(element).ToString());
        }

        private void Hangman_Load(object sender, EventArgs e)
        {

        }

        private void isWin()
        {
            if (game.isGameStillInProgress() == "WIN!")
            {
                Score.Text = "Score: " + game.getScore();
                score = game.getScore();

            }
        }

        private void getPlayerChar_EnterKeyPress(object sender, KeyEventArgs e)

        {
            if (game.isGameStillInProgress() != "")
            {
                MessageBox.Show(game.isGameStillInProgress());
                isWin();
                getPlayerChar.Enabled = false;
                return;
            }
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                e.Handled = true;
                //if (game.isGameStillInProgress() != "")
                //{
                //    MessageBox.Show(game.isGameStillInProgress());
                //    isWin();
                //}

                if (game.checkIfTheStringMatchWithProperties(getPlayerChar.Text))
                {

                    if (game.checkIfCharacterExistsInWord(char.Parse(getPlayerChar.Text)))
                        displayCharactersInButtons(game.getPositionOfFoundChar(char.Parse(getPlayerChar.Text)));
                    DisplayInsertedCharacters();

                    getPlayerChar.Clear();
                    updateScores();


                    if (game.isGameStillInProgress() != "")
                    {
                        MessageBox.Show(game.isGameStillInProgress());
                        isWin();
                        getPlayerChar.Enabled = false;
                    }
                    return;


                }
                getPlayerChar.Clear();
                MessageBox.Show("\t\t   The input is not correct! \n  Make sure that you enter only one character and this character:\n\t is not null, \n\t is not blank space, \n\t and is a letter!");
                return;
            }

        }

        private void easyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            difficulty = "easy";
            Reset();
        }

        private void mediumToolStripMenuItem_Click(object sender, EventArgs e)
        {
            difficulty = "medium";
            Reset();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Hangman_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                GameSelection gameSelection = new GameSelection();
                gameSelection.Show();
            }
        }

        private void newGameToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void hardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            difficulty = "hard";
            Reset();
        }

        private void hangmanDrawing_Paint(object sender, PaintEventArgs e)
        {
            int mistakes = game.getMistakes();

            DrawHangPost();
            if (mistakes >= 1)
                DrawHead(mistakes);
            if (mistakes >= 2)
                DrawBody();
            if (mistakes >= 3)
                DrawRightArm();
            if (mistakes >= 4)
                DrawLeftArm();
            if (mistakes >= 5)
                DrawLeftLeg();
            if (mistakes >= 6)
                DrawRightLeg();
        }

    }
}
