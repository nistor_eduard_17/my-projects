﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace JocurileCopilariei
{
    public partial class TicTacToe : Form
    {
        TicTacToeGame game = new TicTacToeGame(0);
        Button[,] buttons;
        public TicTacToe()
        {
            InitializeComponent();

        }

        private void GenerateGameBoard()
        {
            
            panel1.Controls.Clear();
            int tableSize = game.getSize(), coordinateX = 25, coordinateY = 25;
            buttons = new Button[tableSize, tableSize];
            for (int line = 0; line < tableSize; line++)
            {
                for (int column = 0; column < tableSize; column++)
                {
                    buttons[line, column] = new Button();
                    buttons[line, column].Text = "";
                    buttons[line, column].Font = new Font("Lucida Fax", 14);
                    buttons[line, column].Click += new EventHandler(this.buttonClick);
                    buttons[line, column].SetBounds(coordinateX, coordinateY, 87, 87);
                    panel1.Controls.Add(buttons[line, column]);
                    coordinateX += 100;

                }

                coordinateY += 100;
                coordinateX = 25;
            }

        }

        private void Xsi0_Load(object sender, EventArgs e)
        {

            label1.Text = "X :";
            label3.Text = "0 :";
            label2.Text = "Draws :";
            GenerateGameBoard();


        }
        private void buttonClick(object sender, EventArgs e)
        {
            char currentPlayer = '\0';
            Button button = (Button)sender;
            if (button.Text == "")
            {

                currentPlayer = game.makeMove();
                button.Text = currentPlayer.ToString();
                game.insertIntogameBoard((button.Location.Y - 25) / 100, (button.Location.X - 25) / 100, currentPlayer);

            }
            if (game.isWin(currentPlayer))
            {
                MessageBox.Show(currentPlayer + " WON!");
                UpdateScores();
                return;
            }
            if (game.isDraw(game.getSize() * game.getSize()))
            {
                MessageBox.Show("Draw");
                label2.Text = "Draws: " + game.getNumberOfDraws();
                return;
            }


        }
        private void Xsi0_FormClosing_1(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                GameSelection gameSelection = new GameSelection();
                gameSelection.Show();
            }
        }

        private void UpdateScores()
        {
            if (game.player.getName() == 'X')
            {
                label1.Text = "X :" + game.getXScore();
                return;
            }

            label3.Text = "0 :" + game.getOScore();
            return;
        }

        private void x3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            game.setSize(3);
            resetToolStripMenuItem_Click(sender, e);
            GenerateGameBoard();
        }

        private void x4ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            game.setSize(4);
            resetToolStripMenuItem_Click(sender, e);
            GenerateGameBoard();

        }

        private void x5ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            game.setSize(5);
            resetToolStripMenuItem_Click(sender, e);
            GenerateGameBoard();

        }

        private void newGameToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            game.newGameKeepScores();
            foreach (Button button in buttons)
                button.ResetText();
        }

        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            game = new TicTacToeGame(game.getSize());
            label1.Text = "X :";
            label2.Text = "Draws :";
            label3.Text = "0 :";
            foreach (Button button in buttons)
                button.ResetText();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
