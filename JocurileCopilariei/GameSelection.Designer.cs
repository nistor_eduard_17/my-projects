﻿
namespace JocurileCopilariei
{
    partial class GameSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GameSelection));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.TicTacToe = new System.Windows.Forms.PictureBox();
            this.Hangman = new System.Windows.Forms.PictureBox();
            this.Snake = new System.Windows.Forms.PictureBox();
            this.Tetris = new System.Windows.Forms.PictureBox();
            this.LabelTicTacToe = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TicTacToe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Hangman)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Snake)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tetris)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.ErrorImage")));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(1145, 734);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(74, 60);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // TicTacToe
            // 
            this.TicTacToe.BackColor = System.Drawing.Color.Transparent;
            this.TicTacToe.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("TicTacToe.BackgroundImage")));
            this.TicTacToe.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.TicTacToe.ImageLocation = "";
            this.TicTacToe.Location = new System.Drawing.Point(12, 13);
            this.TicTacToe.Name = "TicTacToe";
            this.TicTacToe.Size = new System.Drawing.Size(600, 350);
            this.TicTacToe.TabIndex = 5;
            this.TicTacToe.TabStop = false;
            this.TicTacToe.Click += new System.EventHandler(this.TicTacToe_Click);
            // 
            // Hangman
            // 
            this.Hangman.BackColor = System.Drawing.Color.Transparent;
            this.Hangman.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Hangman.BackgroundImage")));
            this.Hangman.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Hangman.Location = new System.Drawing.Point(619, 13);
            this.Hangman.Name = "Hangman";
            this.Hangman.Size = new System.Drawing.Size(600, 350);
            this.Hangman.TabIndex = 6;
            this.Hangman.TabStop = false;
            this.Hangman.Click += new System.EventHandler(this.Hangman_Click);
            // 
            // Snake
            // 
            this.Snake.BackColor = System.Drawing.Color.Transparent;
            this.Snake.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Snake.BackgroundImage")));
            this.Snake.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Snake.Location = new System.Drawing.Point(12, 369);
            this.Snake.Name = "Snake";
            this.Snake.Size = new System.Drawing.Size(600, 350);
            this.Snake.TabIndex = 7;
            this.Snake.TabStop = false;
            this.Snake.Click += new System.EventHandler(this.Snake_Click);
            // 
            // Tetris
            // 
            this.Tetris.BackColor = System.Drawing.Color.Transparent;
            this.Tetris.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Tetris.BackgroundImage")));
            this.Tetris.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Tetris.Location = new System.Drawing.Point(619, 369);
            this.Tetris.Name = "Tetris";
            this.Tetris.Size = new System.Drawing.Size(600, 350);
            this.Tetris.TabIndex = 8;
            this.Tetris.TabStop = false;
            this.Tetris.Click += new System.EventHandler(this.Tetris_Click);
            // 
            // LabelTicTacToe
            // 
            this.LabelTicTacToe.BackColor = System.Drawing.Color.Transparent;
            this.LabelTicTacToe.Font = new System.Drawing.Font("Lucida Handwriting", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.LabelTicTacToe.Location = new System.Drawing.Point(30, 25);
            this.LabelTicTacToe.Name = "LabelTicTacToe";
            this.LabelTicTacToe.Size = new System.Drawing.Size(41, 304);
            this.LabelTicTacToe.TabIndex = 9;
            this.LabelTicTacToe.Text = "T\r\nI\r\nC\r\n\r\nT\r\nA\r\nC\r\n\r\nT\r\nO\r\nE\r\n";
            this.LabelTicTacToe.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.LabelTicTacToe.Click += new System.EventHandler(this.LabelTicTacToe_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Lucida Handwriting", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(571, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 223);
            this.label1.TabIndex = 10;
            this.label1.Text = "H\r\nA\r\nN\r\nG\r\nM\r\nA\r\nN";
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Lucida Handwriting", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(30, 396);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 223);
            this.label2.TabIndex = 11;
            this.label2.Text = "S\r\nN\r\nA\r\nK\r\nE";
            this.label2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Lucida Handwriting", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(571, 410);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 223);
            this.label3.TabIndex = 12;
            this.label3.Text = "T\r\nE\r\nT\r\nR\r\nI\r\nS";
            this.label3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // GameSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(1242, 811);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LabelTicTacToe);
            this.Controls.Add(this.Tetris);
            this.Controls.Add(this.Snake);
            this.Controls.Add(this.Hangman);
            this.Controls.Add(this.TicTacToe);
            this.Controls.Add(this.pictureBox1);
            this.MaximumSize = new System.Drawing.Size(1258, 850);
            this.MinimumSize = new System.Drawing.Size(1258, 850);
            this.Name = "GameSelection";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GameSelection";
            this.Load += new System.EventHandler(this.GameSelection_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TicTacToe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Hangman)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Snake)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tetris)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox TicTacToe;
        private System.Windows.Forms.PictureBox Hangman;
        private System.Windows.Forms.PictureBox Snake;
        private System.Windows.Forms.PictureBox Tetris;
        private System.Windows.Forms.Label LabelTicTacToe;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}