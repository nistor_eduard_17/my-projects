﻿
namespace JocurileCopilariei
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.label1 = new System.Windows.Forms.Label();
            this.PLAY = new System.Windows.Forms.Button();
            this.EXIT = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Lucida Calligraphy", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(288, 185);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(469, 162);
            this.label1.TabIndex = 4;
            this.label1.Text = "Childhood Games";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PLAY
            // 
            this.PLAY.AutoSize = true;
            this.PLAY.BackColor = System.Drawing.Color.Transparent;
            this.PLAY.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PLAY.Font = new System.Drawing.Font("Ravie", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.PLAY.ForeColor = System.Drawing.Color.Tomato;
            this.PLAY.Location = new System.Drawing.Point(472, 400);
            this.PLAY.Name = "PLAY";
            this.PLAY.Size = new System.Drawing.Size(113, 39);
            this.PLAY.TabIndex = 5;
            this.PLAY.Text = "PLAY";
            this.PLAY.UseVisualStyleBackColor = false;
            this.PLAY.Click += new System.EventHandler(this.PLAY_Click);
            // 
            // EXIT
            // 
            this.EXIT.AutoSize = true;
            this.EXIT.BackColor = System.Drawing.Color.Transparent;
            this.EXIT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.EXIT.Font = new System.Drawing.Font("Ravie", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.EXIT.ForeColor = System.Drawing.Color.Tomato;
            this.EXIT.Location = new System.Drawing.Point(472, 481);
            this.EXIT.Name = "EXIT";
            this.EXIT.Size = new System.Drawing.Size(113, 40);
            this.EXIT.TabIndex = 6;
            this.EXIT.Text = "EXIT";
            this.EXIT.UseVisualStyleBackColor = false;
            this.EXIT.Click += new System.EventHandler(this.EXIT_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1034, 786);
            this.Controls.Add(this.EXIT);
            this.Controls.Add(this.PLAY);
            this.Controls.Add(this.label1);
            this.MaximumSize = new System.Drawing.Size(1050, 825);
            this.MinimumSize = new System.Drawing.Size(1050, 825);
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Childhood Games";
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button PLAY;
        private System.Windows.Forms.Button EXIT;
    }
}