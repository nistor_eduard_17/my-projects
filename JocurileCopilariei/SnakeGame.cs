﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;

namespace JocurileCopilariei
{
    class SnakeGame
    {
        public enum Directions
        {
            Up,
            Down,
            Left,
            Right
        }

        private int height, width, speed, score, points;
        private int borderHeight, borderWidth;
        private bool gameOver;
        private Directions direction;
        public List<SnakeComponent> Snake = new List<SnakeComponent>();
        public SnakeComponent food = new SnakeComponent();
        public SnakeKeyPress pressedKey = new SnakeKeyPress();

        public SnakeGame()
        {
            width = 16;
            height = 16;
            speed = 10;
            score = 0;
            points = 25;
            gameOver = false;
            direction = Directions.Down;
        }

        public int getWidth() { return this.width; }

        public int getHeight() { return this.height; }

        public bool getGameOver() { return gameOver; }
        public int getSpeed() { return speed; }
        public int getScore() { return score; }

        public void setBorderHeight(int height) { this.borderHeight = height / this.height; }
        public void setBorderWidth(int width) { this.borderWidth = width / this.width; }


        private Directions convertStringToDirection(String s)
        {
            if (s == "Down")
                return Directions.Down;

            if (s == "Up")
                return Directions.Up;

            if (s == "Left")
                return Directions.Left;

            return Directions.Right;

        }
        public void updateMoves(Keys key)
        {

            if (pressedKey.isKeyPressed(key))
            {
                if (gameOver == true)
                {
                    if (key == Keys.Enter)
                    {
                        startGame();
                        return;
                    }
                }

                direction = convertStringToDirection(key.ToString());
                movePlayer();
                return;
            }

            return;
        }

        public bool isLostGame()
        {
            if (Snake[0].x < 0 || Snake[0].y < 0 || Snake[0].x > borderWidth || Snake[0].y > borderHeight)
            {
                die();
                return true;
            }

            for (int j = 1; j < Snake.Count; j++)
            {
                if (Snake[0].x == Snake[j].x && Snake[0].y == Snake[j].y)
                {
                    die();
                    return true;
                }

            }

            return false;
        }

        public void moveSnakeHead()
        {
            switch (direction)
            {
                case Directions.Right:
                    Snake[0].x++;
                    break;
                case Directions.Left:
                    Snake[0].x--;
                    break;
                case Directions.Up:
                    Snake[0].y--;
                    break;
                case Directions.Down:
                    Snake[0].y++;
                    break;

            }

            if (isLostGame())
                return;
            if (Snake[0].x == food.x && Snake[0].y == food.y)
                eatFood();
        }

        public void movePlayer()
        {
            for (int i = Snake.Count - 1; i >= 1; i--)
            {
                Snake[i].x = Snake[i - 1].x;
                Snake[i].y = Snake[i - 1].y;
            }
            moveSnakeHead();

        }

        public void eatFood()
        {
            int x = Snake[Snake.Count - 1].x, y = Snake[Snake.Count - 1].y;
            SnakeComponent body = new SnakeComponent(x, y);
            score += points;
            Snake.Add(body);
            generateFood();
        }
        public void startGame()
        {
            Snake.Clear();
            SnakeComponent head = new SnakeComponent(10, 5);
            Snake.Add(head);
            gameOver = false;
            score = 0;
            generateFood();
        }

        public void generateFood()
        {
            Random rand = new Random();
            int x = rand.Next(0, borderWidth);
            int y = rand.Next(0, borderHeight);

            food = new SnakeComponent(x, y);
        }
        public void die()
        {
            this.gameOver = true;
        }








    }
}
