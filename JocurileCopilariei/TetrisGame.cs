﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace JocurileCopilariei
{
    class TetrisGame
    {
        TetrisPiece tetrisPiece = new TetrisPiece();
        TetrisKeyPress pressedKey = new TetrisKeyPress();
        int[,] piece, initialPiece, nextPiece;
        int[,] gameBoard;
        int height = 21, width = 10, score = 0, startLine, startColumn;
        int lineRotation, ZRotation, LRotation, TRotation;
        char pieceType, nextPieceType;
        bool pieceReachedDest;
        Dictionary<KeyValuePair<int, int>, int> positionOfPiecesOnTheBoard;
        Movement currentMovement;

        enum Movement
        {
            None,
            Down,
            Left,
            Right,
            Rotate
        };

        public int Speed
        {
            get;
            set;
        }
        public TetrisGame()
        {
            pieceReachedDest = false;
            gameBoard = new int[height, width];
            Speed = 600;
            positionOfPiecesOnTheBoard = new Dictionary<KeyValuePair<int, int>, int>();
            lineRotation = ZRotation = LRotation = TRotation = 0;
            for (int line = 0; line < height; ++line)
                for (int column = 0; column < width; ++column)
                {

                    gameBoard[line, column] = -1;
                    if (line == height - 1)
                    {
                        positionOfPiecesOnTheBoard[new KeyValuePair<int, int>(line, column)] = 0;

                    }
                }
            for (int line = 0; line < height; line++)
                positionOfPiecesOnTheBoard[new KeyValuePair<int, int>(line, -1)] = 0;

            piece = tetrisPiece.GeneratePiece();
            currentMovement = Movement.Down;
            startLine = 0;
            startColumn = 3;
            SetPieceOnGameBoard();
            pieceType = tetrisPiece.getPieceType();
            nextPiece = GenerateNextPiece();
            nextPieceType = tetrisPiece.getPieceType();
        }
        private void ChangeLineRotation() { lineRotation = lineRotation == 1 ? lineRotation = 0 : ++lineRotation; }
        private void ChangeZRotation() { ZRotation = ZRotation == 1 ? ZRotation = 0 : ++ZRotation; }
        private void ChangeLRotation() { LRotation = LRotation == 3 ? LRotation = 0 : ++LRotation; }
        private void ChangeTRotation() { TRotation = TRotation == 3 ? TRotation = 0 : ++TRotation; }
        public int[,] GetGameBoard()
        {
            return gameBoard;
        }

        private void UpdatePiecePositionOnTheBoard(int line)
        {
            for (int column = 0; column < width; ++column)
                if (gameBoard[line, column] == -1 && positionOfPiecesOnTheBoard.ContainsKey(new KeyValuePair<int, int>(line, column)) && positionOfPiecesOnTheBoard[new KeyValuePair<int, int>(line, column)] == 0)
                    positionOfPiecesOnTheBoard.Remove(new KeyValuePair<int, int>(line, column));

        }

        public int[,] GetPiece()
        {
            return piece;
        }

        public int[,] GetNextPiece()
        {
            return nextPiece;
        }

        public int[,] GenerateNextPiece()
        {
            nextPiece = tetrisPiece.GeneratePiece();
            return nextPiece;

        }
        private Movement convertStringToDirection(String s)
        {
            if (s == "Left")
                return Movement.Left;

            if (s == "Right")
                return Movement.Right;

            if (s == "R")
                return Movement.Rotate;

            return Movement.Down;

        }

        public void updateMoves(Keys key)
        {

            if (pressedKey.isKeyPressed(key))
            {
                currentMovement = convertStringToDirection(key.ToString());
                movePiece();
                return;
            }

            return;
        }

        private void updateMatrix(int lineBegin, int columnBegin, int lineSize, int columnSize)
        {
            int lineIndex = 0;
            if (lineSize != 0)
            {
                for (int line = lineBegin; line < lineSize; ++line)
                {
                    for (int column = columnBegin; column < columnSize; ++column)
                        gameBoard[line, column] = piece[line - lineBegin, column - columnBegin];
                }
                lineIndex++;
                return;
            }
            for (int column = columnBegin; column < columnSize; ++column)
                gameBoard[lineBegin, column] = piece[lineBegin, column - columnBegin];
        }

        private void SetPieceOnGameBoard()
        {
            int numberOfRows = piece.GetLength(0);
            int numberOfColumns = piece.Length / numberOfRows;
            //int startLine = 0, startColumn = 3;
            //for(int line=0;line<height;++line)
            //    for(int column=0;column<width;++column)
            //    {
            //        if(gameBoard[line,column]!=-1 && !positionOfPiecesOnTheBoard.ContainsKey(new KeyValuePair<int, int>(line, column)))
            //        {
            //            startLine = line;
            //            startColumn = column;
            //            break;
            //        }
            //    }
            updateMatrix(startLine, startColumn, startLine + numberOfRows, startColumn + numberOfColumns);

        }
        private bool ExistsAPieceUnderTheCurrentPiece(int line, int column)
        {
            int value = column;
            if (line <= 0)
                return false;

            while (!positionOfPiecesOnTheBoard.ContainsKey(new KeyValuePair<int, int>(line, column)))
            {
                if (positionOfPiecesOnTheBoard.ContainsKey(new KeyValuePair<int, int>(line + 1, column)))
                    return true;
                if (column >= 0 && column <= width - 1)
                    if ((tetrisPiece.getPieceType() == 'Z' && positionOfPiecesOnTheBoard.ContainsKey(new KeyValuePair<int, int>(line, column + 1)) && ZRotation == 1 && gameBoard[line - 2, column + 1] == gameBoard[line, column]) ||
                          (tetrisPiece.getPieceType() == 'T' && positionOfPiecesOnTheBoard.ContainsKey(new KeyValuePair<int, int>(line, column + 1)) && TRotation == 1 && gameBoard[line - 1, column + 1] == gameBoard[line, column]) ||
                          (tetrisPiece.getPieceType() == 'L' && positionOfPiecesOnTheBoard.ContainsKey(new KeyValuePair<int, int>(line + 1, column + 1)) && gameBoard[line - 2, column + 1] == gameBoard[line, column]) ||
                          (tetrisPiece.getPieceType() == 'L' && positionOfPiecesOnTheBoard.ContainsKey(new KeyValuePair<int, int>(line, column + 2)) && gameBoard[line - 1, column + 2] == gameBoard[line, column]) ||
                          (column >= 2 && tetrisPiece.getPieceType() == 'Z' && positionOfPiecesOnTheBoard.ContainsKey(new KeyValuePair<int, int>(line, column - 2)) && ZRotation == 0 && gameBoard[line - 1, column - 2] == gameBoard[line, column]) ||
                          (column >= 1 && tetrisPiece.getPieceType() == 'T' && positionOfPiecesOnTheBoard.ContainsKey(new KeyValuePair<int, int>(line, column - 1)) && TRotation == 3 && gameBoard[line - 1, column - 1] == gameBoard[line, column]) ||
                          (column >= 1 && tetrisPiece.getPieceType() == 'L' && positionOfPiecesOnTheBoard.ContainsKey(new KeyValuePair<int, int>(line - 1, column - 1)) && LRotation == 2 && gameBoard[line - 2, column - 1] == gameBoard[line, column]))
                        return true;

                if (column >= 1)
                    if (gameBoard[line, column - 1] == -1)
                        break;
                column--;
                if (column == -1)
                    break;

            }
            return false;
        }
        private bool PieceReachedBorder(int line, int column)
        {
            if (ExistsAPieceUnderTheCurrentPiece(line - 1, column))
                return true;

            return false;
        }
        private void UpdateOccupiedPositions()
        {
            for (int line = height - 2; line >= 0; --line)
            {

                for (int column = width - 1; column >= 0; --column)
                {
                    if (gameBoard[line, column] != -1)
                    {
                        positionOfPiecesOnTheBoard[new KeyValuePair<int, int>(line, column)] = 0;
                    }

                }
            }
        }

        private bool IsFullLine(int lineIndex)
        {
            for (int column = 0; column < width; column++)
                if (gameBoard[lineIndex, column] == -1)
                    return false;
            return true;
        }

        private void MoveLines(int startLine)
        {
            for (int line = startLine; line >= 1; --line)
            {
                for (int column = 0; column < width; ++column)
                {
                    gameBoard[line, column] = gameBoard[line - 1, column];
                }
                UpdatePiecePositionOnTheBoard(line);
            }
            for (int column = 0; column < 10; ++column)
                gameBoard[0, column] = -1;
        }

        private void EraseFullLine()
        {
            for (int line = height - 1; line >= 0; --line)
                if (IsFullLine(line))
                { MoveLines(line); line++; score += 200; };

        }

        private void EmptyBoard()
        {
            if (positionOfPiecesOnTheBoard.Count() == 0)
                score += 1000;
        }
        private void ExistsPieceInMargin(int line, ref int valueForMoveColumn)
        {
            if (line >= 1)
            {
                if (valueForMoveColumn == 1 && (gameBoard[line - 1, width - 1] != -1 || gameBoard[line, width - 1] != -1))
                    valueForMoveColumn = 0;
            }
            if (line >= 2)
                if (valueForMoveColumn == -1 && (gameBoard[line, 0] != -1 || gameBoard[line - 2, 0] != -1 || gameBoard[line - 1, 0] != -1))
                    valueForMoveColumn = 0;

        }
        private void findPieceAndMove(int valueForMoveLine, int valueForMoveColumn)
        {
            if (pieceReachedDest == true && IsGameOver() == false)
            {
                currentMovement = Movement.None;
            }
            int newColumnIndex;
            for (int line = height - 2; line >= 0; --line)
            {
                for (int column = width - 1; column >= 0; --column)
                {

                    if (gameBoard[line, column] != -1 && !positionOfPiecesOnTheBoard.ContainsKey(new KeyValuePair<int, int>(line, column)))
                    {
                        ExistsPieceInMargin(line, ref valueForMoveColumn);

                        newColumnIndex = column + valueForMoveColumn;

                        if (PieceReachedBorder(line + valueForMoveLine, newColumnIndex))
                        {
                            EraseFullLine();
                            currentMovement = Movement.None;
                            UpdateOccupiedPositions();
                            score += 4 * tetrisPiece.getLength();
                            if (score % 1000 == 0)
                                Speed -= 5;
                            EmptyBoard();
                            pieceReachedDest = true;
                            return;
                        }


                        gameBoard[line + valueForMoveLine, newColumnIndex] = gameBoard[line, column];
                        gameBoard[line, column] = -1;
                    }
                }
            }

        }

        private void DeletePreviousPiece()
        {
            for (int line = height - 2; line >= 0; --line)
                for (int column = width - 1; column >= 0; --column)
                    if (gameBoard[line, column] != -1 && !positionOfPiecesOnTheBoard.ContainsKey(new KeyValuePair<int, int>(line, column)))
                    {
                        gameBoard[line, column] = -1;
                        startLine = line;
                        startColumn = column;
                    }
        }

        private void RotatePiece()
        {
            DeletePreviousPiece();
            switch (pieceType)
            {
                case 's':
                    break;
                case 'l':
                    ChangeLineRotation();
                    piece = tetrisPiece.CreatePiece(piece, "Line", lineRotation);
                    break;

                case 'L':
                    ChangeLRotation();
                    piece = tetrisPiece.CreatePiece(piece, "L", LRotation);
                    break;

                case 'T':
                    ChangeTRotation();
                    piece = tetrisPiece.CreatePiece(piece, "T", TRotation);
                    break;

                case 'Z':
                    ChangeZRotation();
                    piece = tetrisPiece.CreatePiece(piece, "Z", ZRotation);
                    break;
            }
            SetPieceOnGameBoard();
        }

        public void movePiece()
        {

            switch (currentMovement)
            {
                case Movement.None:
                    PieceReachedDestination();
                    break;
                case Movement.Down:
                    findPieceAndMove(1, 0);
                    break;
                case Movement.Left:
                    findPieceAndMove(1, -1);
                    currentMovement = Movement.Down;
                    break;
                case Movement.Right:
                    findPieceAndMove(1, 1);
                    currentMovement = Movement.Down;
                    break;
                case Movement.Rotate:
                    RotatePiece();
                    currentMovement = Movement.Down;
                    break;
            };
        }

        public bool PieceReachedDestination()
        {

            if (currentMovement == Movement.None)
            {
                ZRotation = LRotation = lineRotation = TRotation = 0;
                piece = nextPiece;
                pieceType = nextPieceType;
                nextPiece = GenerateNextPiece();
                nextPieceType = tetrisPiece.getPieceType();
                currentMovement = Movement.Down;
                startLine = 0;
                startColumn = 3;
                SetPieceOnGameBoard();
                pieceReachedDest = false;
                return true;
            }
            return false;

        }

        public bool IsGameOver()
        {
            for (int column = 0; column < width; ++column)
                if (positionOfPiecesOnTheBoard.ContainsKey(new KeyValuePair<int, int>(1, column)))
                {
                    piece = null;
                    gameBoard = new int[height, width];
                    positionOfPiecesOnTheBoard = new Dictionary<KeyValuePair<int, int>, int>();
                    return true;
                }
            return false;
        }

        public int GetScore()
        {
            return score;
        }

        public void Reset()
        {
            score = 0;
        }
    }
}
