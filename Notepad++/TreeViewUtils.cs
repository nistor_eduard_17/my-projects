﻿using Notepad__.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notepad__
{
    class TreeViewUtils : IGenerate
    {
        public List<Tuple<string, string>> GenerateDirectories(string directoryPath)
        {
            List<Tuple<string, string>> TagAndHeader = new List<Tuple<string, string>>();
            try
            {
                var dirs = Directory.GetDirectories(directoryPath);
                foreach (var dir in dirs)
                {
                    string name = Path.GetFileName(dir);
                    if (name[0] != '$')
                    {
                        TagAndHeader.Add(new Tuple<string, string>(dir, name));
                    }
                }
            }
            catch (Exception)
            {

            }
            return TagAndHeader;
        }
        public List<Tuple<string, string>> GenerateFiles(string directoryPath)
        {
            List<Tuple<string, string>> TagAndHeader = new List<Tuple<string, string>>();
            try
            {
                var files = Directory.GetFiles(directoryPath);
                foreach (var file in files)
                {
                    string name = Path.GetFileName(file);
                    if (name[0] != '$')
                    {
                        TagAndHeader.Add(new Tuple<string, string>(file, name));
                    }
                }
            }
            catch (Exception)
            {

            }
            return TagAndHeader;
        }
    }
}

