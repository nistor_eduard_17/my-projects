﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notepad__.Interfaces
{
    interface IPreviousSession
    {
        void GetPreviousNewFilesSession(ObservableCollection<MyFile> Files);
        void GetPreviousSession(ObservableCollection<MyFile> Files);
    }
}
