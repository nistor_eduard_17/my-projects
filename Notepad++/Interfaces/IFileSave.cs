﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notepad__.Interfaces
{
    interface IFileSave
    {
        void SaveOpenedFiles(ObservableCollection<MyFile> Files);
        void SaveNewFiles(ObservableCollection<MyFile> Files);

    }
}
