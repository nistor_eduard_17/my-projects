﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notepad__.Interfaces
{
    interface IGenerate
    {
        List<Tuple<string, string>> GenerateDirectories(string directoryPath);
        List<Tuple<string, string>> GenerateFiles(string directoryPath);
    }
}
