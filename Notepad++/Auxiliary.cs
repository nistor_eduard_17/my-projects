﻿using System;
using System.IO;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Windows;
using System.Runtime.InteropServices;

namespace Notepad__
{
    public class Auxiliary : FileOperations
    {
        public string ButtonContent { get; set; }
        private bool Changed { get; set; }
        public int TabPositionForFind { get; set; }
        public int LineNumber { get; set; }

        public List<int> Positions = new List<int>();
        public string WhatToFind { get; set; }
        public int Position { get; set; }

        public Auxiliary()
        {
            Position = 0;
            LineNumber = -1;
            Changed = false;
        }

        public bool AlreadyExists(ObservableCollection<MyFile> Files, string path)
        {
            foreach (MyFile file in Files)
                if (file.Path == path)
                    return true;
            return false;
        }

        public void WordPositionInText(string text, string word, bool Erase)
        {
            if (Erase == true)
                Positions.Clear();
            foreach (Match m in Regex.Matches(text, word))
            {
                Positions.Add(m.Index);
            }

        }
        public void SelectionChanged(FindWindow findWindow, string contentOfFile)
        {
            if (findWindow.WhatToFind != null)
            {
                WhatToFind = findWindow.WhatToFind;
                Positions.Clear();
                WordPositionInText(contentOfFile, findWindow.WhatToFind, findWindow.Erase);
            }
        }
        public void ButtonPressed(System.Windows.Controls.TextBox textBox)
        {
            switch (ButtonContent)
            {
                case "cut":
                    Clipboard.Clear();
                    textBox.Cut();
                    break;
                case "copy":
                    Clipboard.Clear();
                    textBox.Copy();
                    break;
                case "paste":
                    textBox.Paste();
                    break;
                case "removeEmptyLines":
                    {
                        textBox.Text = Regex.Replace(textBox.Text, "\\s+\r\n", "\r\n");
                        textBox.Text = Regex.Replace(textBox.Text, "\n+", "\n");
                        break;
                    }
                case "uppercase":
                    textBox.SelectedText = textBox.SelectedText.ToUpper();
                    break;
                case "lowercase":
                    textBox.SelectedText = textBox.SelectedText.ToLower();
                    break;

                case "readOnly":
                    if (textBox.IsReadOnly == false)
                        textBox.IsReadOnly = true;
                    else
                        textBox.IsReadOnly = false;
                    break;

            }
            ButtonContent = "";
        }
        public bool VerifyNumber(int number, int maxlimit)
        {
            if (number < 0)
            {
                MessageBox.Show("The number is invalid!");
                return false;
            }
            if (number >= maxlimit)
            {
                MessageBox.Show("There are only " + maxlimit + " lines!");
                return false;
            }
            return true;
        }
        public bool ExistsWordInContent()
        {
            if (Positions.Count == 0)
                return false;
            return true;
        }
        public void ReplaceFunction(ReplaceWindow replaceWindow, ObservableCollection<MyFile> Files, [Optional] int indexOfFile)
        {
            if (replaceWindow != null)
            {
                if (replaceWindow.WhichOptionOfReplace == "REPLACE ALL")
                {
                    if (replaceWindow.Option == 1)
                    {
                        Files[indexOfFile].Content = Files[indexOfFile].Content.Replace(replaceWindow.WordToReplace, replaceWindow.ReplaceWith);
                        Files[indexOfFile].UpdateFile("*");
                        replaceWindow.Option = 0;
                    }
                    else
                    if (replaceWindow.Option == 2)
                    {
                        foreach (MyFile file in Files)
                        {
                            file.Content = file.Content.Replace(replaceWindow.WordToReplace, replaceWindow.ReplaceWith);
                            file.UpdateFile("*");

                        }
                    }
                    return;
                }

                var regex = new Regex(Regex.Escape(replaceWindow.WordToReplace));
                if (replaceWindow.Option == 1)
                {
                    replaceWindow.Option = 0;
                    if (replaceWindow.ReplaceWith != null)
                    {
                        Files[indexOfFile].Content = regex.Replace(Files[indexOfFile].Content, replaceWindow.ReplaceWith, 1);
                        Files[indexOfFile].UpdateFile("*");
                    }
                    replaceWindow.Option = 1;
                    replaceWindow.ReplaceWith = null;
                }
                else
                   if (replaceWindow.Option == 2)
                {
                    foreach (MyFile file in Files)
                    {
                        replaceWindow.Option = 0;
                        file.Content = regex.Replace(file.Content, replaceWindow.ReplaceWith, 1);
                        file.UpdateFile("*");

                    }
                }

            }

        }
        public void SetTabForFind(ObservableCollection<MyFile> Files, FindWindow findWindow, int currentDocumentNumber, int maximumNumberOfFiles)
        {
            TabPositionForFind = currentDocumentNumber;

            if (findWindow.Option == 2 && Changed == false)
            {
                if (findWindow.Index == Positions.Count)
                {
                    if (currentDocumentNumber < maximumNumberOfFiles)
                    {
                        for (int index = TabPositionForFind + 1; index < Files.Count; ++index)
                            if (Files[index].Content.Contains(findWindow.WhatToFind))
                            {
                                findWindow.Index = 0;
                                TabPositionForFind = index;
                                break;
                            }
                        Changed = true;
                    }
                    else
                    {
                        findWindow.Index = 0;
                        TabPositionForFind = 0;
                        Changed = true;
                    }
                }

                if (findWindow.Index == -1 && Changed == false)
                {
                    if (currentDocumentNumber == 0)
                    {
                        TabPositionForFind = maximumNumberOfFiles;
                        Changed = true;
                    }
                    else
                    {
                        for (int index = TabPositionForFind - 1; index >= 0; --index)
                            if (Files[index].Content.Contains(findWindow.WhatToFind))
                            {
                                TabPositionForFind = index;
                                break;
                            }
                        Changed = true;
                    }
                }
            }
        }
        public int StartPositionForFinding(FindWindow findWindow)
        {
            Changed = false;
            if (findWindow.Index == -1)
                findWindow.Index = Positions.Count - 1;

            if (findWindow.Index >= Positions.Count)
                findWindow.Index = 0;
            findWindow.Erase = false;
            if (Positions.Count == 0)
                return -1;
            return Positions.ElementAt(findWindow.Index);
        }

    }
}
