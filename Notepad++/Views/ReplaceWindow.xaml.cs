﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Notepad__
{
    /// <summary>
    /// Interaction logic for ReplaceWindow.xaml
    /// </summary>
    public partial class ReplaceWindow : Window
    {
        public string WhichOptionOfReplace { get; set; }
        public string WordToReplace { get; set; }
        public string ReplaceWith { get; set; }
        public int Option { get; set; }
        public ReplaceWindow(string WhichOptionOfReplace)
        {
            InitializeComponent();
            replace.Content = WhichOptionOfReplace;
            this.WhichOptionOfReplace = WhichOptionOfReplace;
        }
        private void Replace_Click(object sender, RoutedEventArgs e)
        {
            WordToReplace = wordToReplace.Text;
            ReplaceWith = replaceWith.Text;
        }
        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            Option = 1;
        }
        private void RadioButton_Checked_1(object sender, RoutedEventArgs e)
        {
            Option = 2;
        }
    }
}
