﻿using Microsoft.Win32;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Notepad__
{
    public partial class MainWindow : Window
    {
        Auxiliary aux = new Auxiliary();
        FindWindow findWindow = new FindWindow();
        TreeView tree;
        LineNumber lineNumber;
        ReplaceWindow replaceWindow;
        public ObservableCollection<MyFile> Files { get; set; } = new ObservableCollection<MyFile> { };
        public MainWindow()
        {
            InitializeComponent();
            aux.GetPreviousSession(Files);
            DataContext = this;
        }
        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            if (tree != null && tree.FileToBeOpened != null)
            {
                aux.InsertElement(Files, System.IO.Path.GetFileName(tree.FileToBeOpened), File.ReadAllText(tree.FileToBeOpened), tree.FileToBeOpened);
                tree.FileToBeOpened = null;
            }
        }
        private void CloseTabItem_Click(object sender, RoutedEventArgs e)
        {
            if (Control.SelectedIndex != -1)
            {
                bool option = false;
                Files.ElementAt(Control.SelectedIndex).ClosingFile(Files.ElementAt(Control.SelectedIndex).Path, Files.ElementAt(Control.SelectedIndex).Filename, ref option);
                if (option == true)
                    Files.Remove(Files.ElementAt(Control.SelectedIndex));
            }
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            aux.SaveOpenedFiles(Files);
            aux.SaveNewFiles(Files);
        }
        private void ContentBox_SelectionChanged(object sender, RoutedEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (findWindow.WhatToFind != null)
            {
                aux.SelectionChanged(findWindow, Files.ElementAt(Control.SelectedIndex).Content);

                if (aux.ExistsWordInContent() == false)
                    MessageBox.Show(findWindow.WhatToFind + " does not exist in current document");
                else
                {
                    aux.SetTabForFind(Files, findWindow, Control.SelectedIndex, Control.Items.Count);
                    Control.SelectedIndex = aux.TabPositionForFind;
                    aux.SelectionChanged(findWindow, Files.ElementAt(Control.SelectedIndex).Content);
                    findWindow.WhatToFind = null;
                    if (aux.StartPositionForFinding(findWindow) != -1)
                    {
                        if (findWindow.Index == aux.Positions.Count - 1 || findWindow.Index == 0)
                            MessageBox.Show("Word found succesfully");
                        textBox.SelectionStart = aux.StartPositionForFinding(findWindow);
                        textBox.SelectionLength = aux.WhatToFind.Length;
                    }
                }
            }
            aux.ReplaceFunction(replaceWindow, Files, Control.SelectedIndex);

            if (aux.LineNumber != -1 && aux.VerifyNumber(lineNumber.Number, textBox.LineCount))
            {
                aux.LineNumber = -1;
                textBox.SelectionStart = textBox.GetCharacterIndexFromLineIndex(lineNumber.Number);
                textBox.SelectionLength = textBox.GetLineLength(lineNumber.Number);
            }
            aux.LineNumber = -1;
        }
        private void Uppercase_Click(object sender, RoutedEventArgs e)
        {
            aux.ButtonContent = "uppercase";
        }
        private void Paste_Click(object sender, RoutedEventArgs e)
        {
            aux.ButtonContent = "paste";
        }
        private void Copy_Click(object sender, RoutedEventArgs e)
        {
            aux.ButtonContent = "copy";
        }
        private void Cut_Click(object sender, RoutedEventArgs e)
        {
            aux.ButtonContent = "cut";
        }
        private void Lowercase_Click(object sender, RoutedEventArgs e)
        {
            aux.ButtonContent = "lowercase";
        }
        private void GoToLineNumber_Click(object sender, RoutedEventArgs e)
        {
            if (Control.SelectedIndex != -1)
            {
                lineNumber = new LineNumber();
                lineNumber.Show();
                aux.LineNumber = 0;
                return;
            }
            MessageBox.Show("Please select one tab!");
        }
        private void ContentBox_MouseMove(object sender, MouseEventArgs e)
        {
            aux.ButtonPressed(sender as TextBox);
        }
        private void RemoveEmptyLines_Click(object sender, RoutedEventArgs e)
        {
            aux.ButtonContent = "removeEmptyLines";
        }
        private void ReadOnly_Click(object sender, RoutedEventArgs e)
        {
            aux.ButtonContent = "readOnly";
        }
        private void TreeView_Click(object sender, RoutedEventArgs e)
        {
            tree = new TreeView();
            tree.Show();
        }
        private void New_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            aux.InsertElement(Files, "", "", "");
        }
        private void Open_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                if (aux.AlreadyExists(Files, openFileDialog.FileName) == false)
                    aux.InsertElement(Files, openFileDialog.SafeFileName, File.ReadAllText(openFileDialog.FileName), openFileDialog.FileName);
                else
                    MessageBox.Show("File is already opened!");
            }
        }
        private void Save_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog()
            {
                Filter = "Text Files | *.txt | C# file |*.cs",
            };
            if (!Files[Control.SelectedIndex].Filename.Contains("."))
            {
                saveFileDialog.ShowDialog();
                Files[Control.SelectedIndex].SaveFile(saveFileDialog.FileName, saveFileDialog.SafeFileName);
                return;
            }
            if (Files[Control.SelectedIndex].Path != "" && Files[Control.SelectedIndex].Filename != "")
                Files[Control.SelectedIndex].SaveFile(Files[Control.SelectedIndex].Path, Files[Control.SelectedIndex].Filename);
        }
        private void SaveAs_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog()
            {
                Filter = "Text Files | *.txt | C# file |*.cs",
            };
            saveFileDialog.ShowDialog();
            Files[Control.SelectedIndex].SaveFile(saveFileDialog.FileName, saveFileDialog.SafeFileName);
        }
        private void Exit_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            aux.SaveOpenedFiles(Files);
            Close();
        }
        private void Find_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            findWindow = new FindWindow();
            findWindow.Show();
        }
        private void Replace_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            replaceWindow = new ReplaceWindow("REPLACE");
            replaceWindow.Show();
        }
        private void ReplaceAll_Click(object sender, RoutedEventArgs e)
        {
            replaceWindow = new ReplaceWindow("REPLACE ALL");
            replaceWindow.Show();
        }
        private void About_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            About about = new About();
            about.Show();
        }
    }
}
