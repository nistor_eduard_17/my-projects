﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Notepad__
{
    /// <summary>
    /// Interaction logic for FindWindow.xaml
    /// </summary>
    public partial class FindWindow : Window
    {
        public int Index { get; set; }
        public bool Erase { get; set; }
        public string WhatToFind { get; set; }
        public int Option { get; set; }

        public FindWindow()
        {
            InitializeComponent();
            Option = 0;
            Index = 0;
        }
        private void Find_Click(object sender, RoutedEventArgs e)
        {
            if (Option != 0)
            {
                Hide();
                WhatToFind = textBox.Text;
                Index = 0;
                Erase = true;
                return;
            }
            MessageBox.Show("Choose where to find");
        }
        private void CurrentDocument_Checked(object sender, RoutedEventArgs e)
        {
            Option = 1;
        }
        private void AllDocuments_Checked(object sender, RoutedEventArgs e)
        {
            Option = 2;
        }
        private void FindNext_Click(object sender, RoutedEventArgs e)
        {
            WhatToFind = textBox.Text;
            ++Index;
        }
        private void Previous_Click(object sender, RoutedEventArgs e)
        {
            WhatToFind = textBox.Text;
            --Index;
        }
    }
}
