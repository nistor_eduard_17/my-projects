﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Notepad__
{
    /// <summary>
    /// Interaction logic for TreeView.xaml
    /// </summary>
    public partial class TreeView : Window
    {
        readonly TreeViewUtils utils = new TreeViewUtils();
        public string FileToBeOpened { get; set; }
        public TreeView()
        {
            InitializeComponent();
            LoadDrivers();
        }
        private void LoadDrivers()
        {
            var drivers = DriveInfo.GetDrives();
            foreach (var drive in drivers)
            {
                TreeViewItem item = new TreeViewItem();
                item.Header = drive.Name;
                item.Tag = drive.Name;
                treeView.Items.Add(item);
            }
        }
        private void TreeView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (System.IO.Path.HasExtension((treeView.SelectedItem as TreeViewItem).Tag.ToString()))
                FileToBeOpened = (treeView.SelectedItem as TreeViewItem).Tag.ToString();
            Close();
        }
        private void TreeView_Selected(object sender, RoutedEventArgs e)
        {
            TreeViewItem selected = treeView.SelectedItem as TreeViewItem;
            List<Tuple<string, string>> directories = utils.GenerateDirectories(selected.Tag.ToString());
            foreach (var element in directories)
            {
                TreeViewItem directory = new TreeViewItem
                {
                    Tag = element.Item1,
                    Header = element.Item2
                };
                selected.Items.Add(directory);
            }
            List<Tuple<string, string>> files = utils.GenerateFiles(selected.Tag.ToString());
            foreach (var element in files)
            {
                TreeViewItem fileItem = new TreeViewItem
                {
                    Tag = element.Item1,
                    Header = element.Item2
                };
                selected.Items.Add(fileItem);
            }
        }
    }
}
