﻿using Notepad__.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notepad__
{
    public class FileOperations : IFileSave, IPreviousSession
    {
        public void GetPreviousNewFilesSession(ObservableCollection<MyFile> Files)
        {
            StreamReader sr = new StreamReader("../../new_history.txt");

            string line, content = "", fileName = "";
            while ((line = sr.ReadLine()) != null)
            {
                if (line.Contains("!@#Title$%"))
                {
                    if (content != "")
                    {
                        InsertElement(Files, fileName, content, "");
                        fileName = "";
                        content = "";
                    }
                    fileName = line.Replace("!@#Title$%", "");
                }
                else
                {
                    content += line + '\n';
                }
            }

            if (content != "" && fileName != "")
                InsertElement(Files, fileName, content, "");
            sr.Close();
        }
        public void GetPreviousSession(ObservableCollection<MyFile> Files)
        {
            StreamReader sr = new StreamReader("../../history.txt");

            string line, content = "", fileName = "";
            bool unsavedFile = false;
            while ((line = sr.ReadLine()) != null)
            {
                if (line.Contains(@":\") && !line.Contains("*") && File.Exists(line))
                    InsertElement(Files, Path.GetFileName(line), File.ReadAllText(line), line);
                else
                {
                    if (fileName == "")
                        fileName = line;
                    if (line.Contains("*") && unsavedFile == false)
                    {
                        unsavedFile = true;
                        content = "";
                    }
                    else
                    if (!line.Contains(@":\") && !line.Contains("*"))
                    {
                        content += line + '\n';
                    }
                    else

                    if (line.Contains(@":\") && line.Contains("*"))
                    {
                        unsavedFile = false;
                        InsertElement(Files, Path.GetFileName(fileName), content, fileName.Replace("*", ""));
                        fileName = line;
                        content = "";
                    }
                }
            }

            if (fileName != "" && content != "")
            {
                InsertElement(Files, Path.GetFileName(fileName), content, fileName.Replace("*", ""));
            }

            GetPreviousNewFilesSession(Files);
            sr.Close();
        }
        public void SaveNewFiles(ObservableCollection<MyFile> Files)
        {
            using (StreamWriter sw = new StreamWriter("../../new_history.txt"))
            {
                sw.Write("");
            }
            foreach (MyFile file in Files)
            {
                using (StreamWriter sw = File.AppendText("../../new_history.txt"))
                {
                    if (file.Filename.Contains("*") && file.Filename.Contains("new") && !file.Filename.Contains("."))
                    {
                        sw.WriteLine("!@#Title$%" + file.Filename);
                        sw.WriteLine(file.Content);
                    }

                }
            }
        }
        public void SaveOpenedFiles(ObservableCollection<MyFile> Files)
        {
            using (StreamWriter sw = new StreamWriter("../../history.txt"))
            {
                sw.Write("");
            }

            foreach (MyFile file in Files)
            {
                using (StreamWriter sw = File.AppendText("../../history.txt"))
                {
                    if (file.Path.Length != 0)
                    {
                        if (file.Filename.Contains("*"))
                        {
                            sw.WriteLine(file.Path + "*");
                            sw.WriteLine(file.Content);
                        }
                        else
                            sw.WriteLine(file.Path);
                    }
                }
            }
        }
        public void InsertElement(ObservableCollection<MyFile> Files, string header, string text, string file_path)
        {
            MyFile file = new MyFile(GetPosition(Files))
            {
                Path = file_path,
                Filename = header,
                Content = text
            };

            if (header == "")
            {
                file.Filename = "new " + GetPosition(Files).ToString();
            }

            Files.Add(file);
        }
        private int GetPosition(ObservableCollection<MyFile> Files)
        {
            int foundPosition = 0, position = 0;

            for (int index = 1; index < 50; index++)
            {
                foundPosition = 0;

                foreach (MyFile file in Files)
                {
                    if (file.Filename.Contains(index.ToString()) && file.Filename.Contains("new") && !file.Filename.Contains("."))
                    {
                        foundPosition = index;
                        break;
                    }
                }

                if (foundPosition == 0)
                {
                    position = index;
                    break;
                }

            }
            return position;
        }
    }
}
