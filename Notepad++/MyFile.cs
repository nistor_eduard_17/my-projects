﻿using System;
using System.Windows;
using Microsoft.Win32;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Notepad__
{
    public class MyFile : INotifyPropertyChanged
    {
        private string filename, content;
        public bool ContinueFinding { get; set; }
        public string Path { get; set; }
        public int OpenedFiles { get; set; }
        public string Filename
        {
            get
            {
                return filename;
            }
            set
            {
                filename = value;
                NotifyPropertyChanged("Filename");
            }
        }
        public string Content
        {
            get
            {
                ContinueFinding = true;
                return content;
            }
            set
            {
                content = value;
                NotifyPropertyChanged("Content");
            }
        }

        public MyFile(int index)
        {
            OpenedFiles = index;
            Path = "";
            Filename = "";
            Content = "";

        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                if (propertyName == "Content")
                {
                    UpdateFile("*  ");
                }
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public void UpdateFile(string filename)
        {
            if (Filename.Contains("*"))
            {
                return;
            }
            Filename += filename;
        }
        public void SaveFile(string path, string name)
        {
            if (path != "")
            {
                Path = path;
                Filename = name;
                File.WriteAllText(Path, Content);
                if (Filename.Contains("*"))
                {
                    Filename = Filename.Replace("*", "");
                }
            }
        }
        public void ClosingFile(string path, string name, ref bool option)
        {
            if (Filename.Contains("*"))
            {
                MessageBoxResult result = MessageBox.Show("This file is not saved. Would you like to save it?", "Confirmation", MessageBoxButton.YesNoCancel);

                if (result == MessageBoxResult.Yes)
                {
                    SaveFile(path, name);
                    return;
                }
                if (result == MessageBoxResult.No)
                {
                    option = true;
                    return;
                }
                if (result == MessageBoxResult.Cancel)
                {
                    option = false;
                    return;
                }
            }

            option = true;
        }
    }
}
