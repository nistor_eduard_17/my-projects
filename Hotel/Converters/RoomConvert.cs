﻿using Hotel.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using Type = Hotel.Models.EntityLayer.Type;

namespace Hotel.Converters
{
    public class RoomConvert : IMultiValueConverter
    {
        public object Convert(object[] values, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] != null && values[1] != null && values[2] != null)
            {
                Room r =
                 new Room()
                 {
                     RoomType = (values[0] as Type).RoomType,
                     RoomTypeName = (values[0] as Type).TypeName,
                     Options = (ObservableCollection<Option>)values[1],
                     Images = (ObservableCollection<string>)values[2]
                 };
                return r;
            }
            return null;
        }
        public object[] ConvertBack(object value, System.Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
