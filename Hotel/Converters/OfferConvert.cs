﻿using Hotel.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using Type = Hotel.Models.EntityLayer.Type;

namespace Hotel.Converters
{
    public class OfferConvert : IMultiValueConverter
    {
        public object Convert(object[] values, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            if (values[0] != null && values[1] != null && values[2] != null && values[3] != null && values[4] != null && values[6] != null)
                if (values[1].ToString() != "")
                {
                    int val = 1;
                    if (values[5] != null)
                    {
                        Offer o = (Offer)values[5];
                        val = o.OfferID;
                    }

                    return new Offer()
                    {
                        RoomType = (Type)values[0],
                        Price = Int32.Parse(values[1].ToString()),
                        StartDate = (DateTime)values[2],
                        EndDate = (DateTime)values[3],
                        Services = (ObservableCollection<Service>)values[4],
                        OfferName = values[6].ToString(),
                        OfferID = val
                    };
                }
            return null;
        }
        public object[] ConvertBack(object value, System.Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
