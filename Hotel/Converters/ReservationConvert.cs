﻿using Hotel.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Hotel.Converters
{
    public class ReservationConvert : IMultiValueConverter
    {
        public object Convert(object[] values, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] != null && values[1] != null && values[2] != null && values[3] != null)
            {
                return new Reservation()
                {
                    StartDate = (DateTime)values[0],
                    EndDate = (DateTime)values[1],
                    RoomTypes = (ObservableCollection<ReservationRoomType>)values[2],
                    Services = (ObservableCollection<Service>)values[3],
                    Status = "Active"
                };

            }
            return null;
        }
        public object[] ConvertBack(object value, System.Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
