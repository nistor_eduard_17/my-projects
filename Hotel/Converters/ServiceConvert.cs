﻿using Hotel.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Hotel.Converters
{
    public class ServiceConvert : IMultiValueConverter
    {
        public object Convert(object[] values, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] != null && values[1] != null)
                if (values[0] != "" && values[1] != "")
                {
                    return new Service()
                    {
                        ServiceID = 1,
                        ServiceType = values[0].ToString(),
                        Price = Int32.Parse(values[1].ToString())
                    };

                }
            return null;
        }
        public object[] ConvertBack(object value, System.Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
