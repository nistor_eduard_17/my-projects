﻿using Hotel.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using Type = Hotel.Models.EntityLayer.Type;

namespace Hotel.Converters
{
    public class TypePriceConvert : IMultiValueConverter
    {
        public object Convert(object[] values, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] != null && values[1] != null && values[2] != null && values[3] != null)
                if (values[1] != "")
                {
                    return new RoomTypePrice()
                    {
                        CurrentType = (Type)values[0],
                        Price = Int32.Parse(values[1].ToString()),
                        StartDate = (DateTime)(values[2]),
                        EndDate = (DateTime)(values[3])
                    };

                }
            return null;
        }
        public object[] ConvertBack(object value, System.Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
