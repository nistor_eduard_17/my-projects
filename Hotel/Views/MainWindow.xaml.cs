﻿using Hotel.Models.EntityLayer;
using Hotel.Views;
using System.Windows;

namespace Hotel
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Registered_Click(object sender, RoutedEventArgs e)
        {
            LoginOrCreateAccount login = new LoginOrCreateAccount();
            login.Show();
            this.Close();
        }

        private void Visitor_Click(object sender, RoutedEventArgs e)
        {
            HotelMainWindow hotelMainWindow = new HotelMainWindow(new User() { Username = "Visitor", UserType = "Click here to create account and unlock extra features!" });
            hotelMainWindow.Show();
            this.Close();
        }
    }
}
