﻿using Hotel.Models.EntityLayer;
using Hotel.ViewModels;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Hotel.Views
{
    /// <summary>
    /// Interaction logic for HotelMainWindow.xaml
    /// </summary>
    public partial class HotelMainWindow : Window
    {
        HotelMainWindowVM hotel;
        public HotelMainWindow(User user)
        {
            InitializeComponent();
            hotel = new HotelMainWindowVM(user);
            DataContext = hotel;
        }

        private void UserTypeLabel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if ((sender as Label).Content.ToString().Contains(" "))
            {
                LoginOrCreateAccount createAccount = new LoginOrCreateAccount();
                createAccount.Show();
                this.Close();
            }
        }

        private void Logout_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }

        private void AdminBtn_Click(object sender, RoutedEventArgs e)
        {
            AdminWindowChoice adminWindow = new AdminWindowChoice();
            adminWindow.Show();


        }

        private void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            StartDate.DisplayDateStart = System.DateTime.Today;
            if (StartDate.SelectedDate.Value < System.DateTime.Today)
            {
                StartDate.SelectedDate = System.DateTime.Today;
            }
            StartDate.DisplayDateEnd = hotel.LastDate;
            DateTime lastDateMinusOneDay = new DateTime(hotel.LastDate.Year, hotel.LastDate.Month, hotel.LastDate.Day - 1);
            if (StartDate.SelectedDate.Value > lastDateMinusOneDay)
            {
                MessageBox.Show("Invalid date!");
                StartDate.SelectedDate = lastDateMinusOneDay;
            }
            EndDate.SelectedDate = null;
        }

        private void EndDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (EndDate.SelectedDate == null && StartDate.SelectedDate != null)
            {
                EndDate.DisplayDateStart = StartDate.SelectedDate.Value.AddDays(1);
                EndDate.DisplayDateEnd = hotel.LastDate;
            }
            if (StartDate.SelectedDate == null && EndDate.SelectedDate != null)
            {
                MessageBox.Show("Please pick check-in date first!");
                EndDate.SelectedDate = null;
                return;
            }
            if (EndDate.SelectedDate != null)
            {
                EndDate.DisplayDateStart = StartDate.SelectedDate.Value.AddDays(1);
                if (EndDate.SelectedDate.Value < StartDate.SelectedDate.Value.AddDays(1))
                {
                    EndDate.SelectedDate = StartDate.SelectedDate.Value.AddDays(1);
                }
                EndDate.DisplayDateEnd = hotel.LastDate;
                if (EndDate.SelectedDate.Value > hotel.LastDate)
                {
                    MessageBox.Show("Invalid date!");
                    EndDate.SelectedDate = hotel.LastDate;
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            hotel.ButtonPressed();
        }
        private void Reserve_Click(object sender, RoutedEventArgs e)
        {
            UserWindowReservation reservation = new UserWindowReservation(new Offer(),new User() { Username = hotel.Username, UserType = hotel.UserType });
            reservation.Show();
            this.Close();
        }

        private void History_Click(object sender, RoutedEventArgs e)
        {
            UserHistoryWindow history = new UserHistoryWindow(new User() { Username=hotel.Username, UserType=hotel.UserType});
            history.Show();
            this.Close();
        }

        private void ViewOffers_Click(object sender, RoutedEventArgs e)
        {
            OffersWindow offersWindow = new OffersWindow(new User() { Username=hotel.Username, UserType=hotel.UserType});
            offersWindow.Show();
        }

        private void EmployeeButton_Click(object sender, RoutedEventArgs e)
        {
            EmployeeReservationWindow employeeReservation = new EmployeeReservationWindow(new User() { Username=hotel.Username, UserType=hotel.UserType});
            employeeReservation.Show();
            this.Close();
        }
    }
}
