﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hotel.Views
{
    /// <summary>
    /// Interaction logic for AdminWindow.xaml
    /// </summary>
    public partial class AdminWindowChoice : Window
    {
        public AdminWindowChoice()
        {
            InitializeComponent();
        }

        private void Room_Click(object sender, RoutedEventArgs e)
        {
            AdminWindowRooms rooms = new AdminWindowRooms();
            rooms.Show();
            this.Close();
        }

        private void Option_Click(object sender, RoutedEventArgs e)
        {
            AdminWindowOptions options = new AdminWindowOptions();
            options.Show();
            this.Close();
        }

        private void Service_Click(object sender, RoutedEventArgs e)
        {
            AdminWindowServices services = new AdminWindowServices();
            services.Show();
            this.Close();
        }

        private void Price_Click(object sender, RoutedEventArgs e)
        {
            AdminWindowPrices prices = new AdminWindowPrices();
            prices.Show();
            this.Close();
        }

        private void Offer_Click(object sender, RoutedEventArgs e)
        {
            AdminWindowOffers offers = new AdminWindowOffers();
            offers.Show();
            this.Close();
        }
    }
}
