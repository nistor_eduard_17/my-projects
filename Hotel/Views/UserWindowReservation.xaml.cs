﻿using Hotel.Models.EntityLayer;
using Hotel.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hotel.Views
{
    /// <summary>
    /// Interaction logic for UserWindowReservation.xaml
    /// </summary>
    public partial class UserWindowReservation : Window
    {
        UserReservationVM userReservation;
        public UserWindowReservation(Offer offer,User user)
        {
            InitializeComponent();
            userReservation = new UserReservationVM(offer,user);
            this.DataContext = userReservation;
        }

        private void StartDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            StartDate.DisplayDateStart = System.DateTime.Today;
            if (StartDate.SelectedDate.Value < System.DateTime.Today)
            {
                StartDate.SelectedDate = System.DateTime.Today;
            }
            StartDate.DisplayDateEnd = userReservation.LastDate;
            DateTime lastDateMinusOneDay = new DateTime(userReservation.LastDate.Year, userReservation.LastDate.Month, userReservation.LastDate.Day - 1);
            if (StartDate.SelectedDate.Value > lastDateMinusOneDay)
            {
                MessageBox.Show("Invalid date!");
                StartDate.SelectedDate = lastDateMinusOneDay;
            }
            EndDate.SelectedDate = null;
        }

        private void EndDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (EndDate.SelectedDate == null && StartDate.SelectedDate != null)
            {
                EndDate.DisplayDateStart = StartDate.SelectedDate.Value.AddDays(1);
                EndDate.DisplayDateEnd = userReservation.LastDate;
            }
            if (StartDate.SelectedDate == null && EndDate.SelectedDate != null)
            {
                MessageBox.Show("Please pick check-in date first!");
                EndDate.SelectedDate = null;
                return;
            }
            if (EndDate.SelectedDate != null)
            {
                EndDate.DisplayDateStart = StartDate.SelectedDate.Value.AddDays(1);
                if (EndDate.SelectedDate.Value < StartDate.SelectedDate.Value.AddDays(1))
                {
                    EndDate.SelectedDate = StartDate.SelectedDate.Value.AddDays(1);
                }
                EndDate.DisplayDateEnd = userReservation.LastDate;
                if (EndDate.SelectedDate.Value > userReservation.LastDate)
                {
                    MessageBox.Show("Invalid date!");
                    EndDate.SelectedDate = userReservation.LastDate;
                }
            }
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            userReservation.AddReservationRoomType();
        }

        private void Finish_Click(object sender, RoutedEventArgs e)
        {
            HotelMainWindow mainWindow = new HotelMainWindow(new User() { Username = userReservation.Username, UserType = userReservation.UserType });
            MessageBox.Show("Reservation paied! Enjoy it!");
            mainWindow.Show();
            this.Close();     
        }
    }
}
