﻿using Hotel.Models.EntityLayer;
using System.Windows;
using System.Windows.Input;

namespace Hotel.Views
{
    /// <summary>
    /// Interaction logic for LoginOrCreateAccount.xaml
    /// </summary>
    public partial class LoginOrCreateAccount : Window
    {
        public LoginOrCreateAccount()
        {
            InitializeComponent();
            Hidden.Visibility = Visibility.Hidden;
        }

        private void Label_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ToHide.Visibility = Visibility.Hidden;
            Title.Content = "SIGN IN";
            ContinueBtn.Content = "LOGIN";
            UserType.SelectedItem = "-1";
            UserType.Visibility = Visibility.Hidden;
            UserTypeLabel.Visibility = Visibility.Hidden;
        }
        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow();
            main.Show();
            this.Close();
        }

        private void Password_PasswordChanged(object sender, RoutedEventArgs e)
        {
            Hidden.Text = Password.Password.ToString();
        }

        private void ContinueBtn_Click(object sender, RoutedEventArgs e)
        {
            if (UserType.SelectionBoxItem.ToString() != "")
            {
                HotelMainWindow hotelMainWindow = new HotelMainWindow(new User() { Username = Username.Text, UserType = UserType.SelectionBoxItem.ToString() });
                hotelMainWindow.Show();
                this.Close();
            }
        }
    }
}
