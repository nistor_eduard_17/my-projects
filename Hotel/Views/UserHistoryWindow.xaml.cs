﻿using Hotel.Models.EntityLayer;
using Hotel.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hotel.Views
{
    /// <summary>
    /// Interaction logic for UserHistoryWindow.xaml
    /// </summary>
    public partial class UserHistoryWindow : Window
    {
        UserHistoryVM userHistory;
        public UserHistoryWindow(User user)
        {
            userHistory = new UserHistoryVM(user);
            InitializeComponent();
            this.DataContext = userHistory;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            HotelMainWindow mainWindow = new HotelMainWindow(userHistory.User);
            mainWindow.Show();
        }
    }
}
