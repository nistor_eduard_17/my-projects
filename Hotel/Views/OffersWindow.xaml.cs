﻿using Hotel.Models.EntityLayer;
using Hotel.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hotel.Views
{
    /// <summary>
    /// Interaction logic for OffersWindow.xaml
    /// </summary>
    public partial class OffersWindow : Window
    {
        OffersWindowVM OffersVM;
        public OffersWindow(User user)
        {
            OffersVM = new OffersWindowVM(user);
            InitializeComponent();
            this.DataContext = OffersVM;
        }
        private void Reserve_Click(object sender, RoutedEventArgs e)
        {
            UserWindowReservation reservation = new UserWindowReservation(OffersVM.SelectedOffer, OffersVM.User);
            reservation.Show();
            this.Close();
        }
    }
}
