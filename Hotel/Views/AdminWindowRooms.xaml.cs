﻿using Hotel.ViewModels;
using Microsoft.Win32;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Hotel.Models.EntityLayer;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hotel.Views
{
    /// <summary>
    /// Interaction logic for AdminWindowRooms.xaml
    /// </summary>
    public partial class AdminWindowRooms : Window
    {
        AdminRoomVM AdminRoomVM;
        public AdminWindowRooms()
        {
            InitializeComponent();
            AdminRoomVM = new AdminRoomVM();
            this.DataContext = AdminRoomVM;
        }

        private void AddImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = true;
            openFileDialog.Filter = "All files (*.*)|*.*";
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            if (openFileDialog.ShowDialog() == true)
            {
                foreach (string filename in openFileDialog.FileNames)
                    AdminRoomVM.AddImages(System.IO.Path.GetFullPath(filename));
            }
        }
    }
}
