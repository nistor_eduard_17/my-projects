﻿using Hotel.Models.EntityLayer;
using Hotel.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hotel.Views
{
    /// <summary>
    /// Interaction logic for EmployeeReservationWindow.xaml
    /// </summary>
    public partial class EmployeeReservationWindow : Window
    {
        EmployeeReservationVM employeeReservatioVM;
        public EmployeeReservationWindow(User user)
        {
            employeeReservatioVM = new EmployeeReservationVM(user);
            InitializeComponent();
            this.DataContext= employeeReservatioVM;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            HotelMainWindow mainWindow = new HotelMainWindow(new User() { Username = employeeReservatioVM.User.Username, UserType = employeeReservatioVM.User.UserType });
            mainWindow.Show();
        }
    }
}
