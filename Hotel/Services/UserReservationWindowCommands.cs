﻿using Hotel.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Type = Hotel.Models.EntityLayer.Type;

namespace Hotel.Services
{
    public class UserReservationWindowCommands : ServicePropertyChanged
    {
        public ObservableCollection<ReservationRoomType> RoomTypes;
        public ObservableCollection<Bill> Bill;
        public ObservableCollection<Service> AddedServices;
        public ObservableCollection<int> Sum;
        public ObservableCollection<bool> CanPay;
        public Offer SelectedOffer;
        public UserReservationWindowCommands(ObservableCollection<ReservationRoomType> roomTypes, ObservableCollection<Bill> bill,
            ObservableCollection<Service> services, ObservableCollection<int> sum, ObservableCollection<bool> canPay)
        {
            RoomTypes = roomTypes;
            Bill = bill;
            AddedServices = services;
            Sum = sum;
            CanPay = new ObservableCollection<bool>() { false };
        }
        public void DeleteRoomType(ReservationRoomType roomType)
        {
            Sum[0] -= (Bill.Where(p => p.Name == roomType.RoomType.TypeName).Single()).TotalPrice;
            RoomTypes.Remove(roomType);
            Bill.Remove(Bill.Where(p => p.Name == roomType.RoomType.TypeName).Single());
            if (Bill.Count == 0)
                CanPay[0] = false;
            else
            {
                bool valid = false;
                foreach (var elem in Bill)
                    foreach (var rt in RoomTypes)
                        if (elem.Name == rt.RoomType.TypeName)
                        {
                            valid = true;
                            CanPay[0] = true;
                            break;
                        }
                if (valid == false)
                    CanPay[0] = false;
            }
        }
        public void DeleteServices(Service s)
        {
            if (SelectedOffer == null || (SelectedOffer.OfferID != 0 && SelectedOffer.Services.FirstOrDefault(p => p.ServiceID == s.ServiceID) == null) || SelectedOffer.OfferID == 0)
            {
                Sum[0] -= (Bill.Where(p => p.Name == s.ServiceType).Single()).TotalPrice;
                AddedServices.Remove(s);
                Bill.Remove(Bill.Where(p => p.Name == s.ServiceType).Single());
                if (Bill.Count == 0)
                    CanPay[0] = false;
            }

        }
        public int CalculatePrice(DateTime CheckIn, DateTime CheckOut, ObservableCollection<RoomTypePrice> Prices)
        {
            TimeSpan diff = CheckOut.Date - CheckIn.Date;
            int totalDays;
            int days = (int)diff.TotalDays;
            totalDays = days;
            int price;
            if (CheckIn.Date.Month == CheckOut.Date.Month)
            {
                price = (days) * Prices[0].Price;
            }
            else
            {
                diff = Prices[0].EndDate - CheckIn.Date;
                price = 0;
                if (Prices.Count == 2)
                    price = ((int)diff.TotalDays) * Prices[0].Price + (days - (int)diff.TotalDays - 1) * Prices[1].Price;
                else
                {
                    price = ((int)diff.TotalDays) * Prices[0].Price;
                    days -= (int)diff.TotalDays;
                    for (int index = 1; index < Prices.Count - 1; ++index)
                    {
                        diff = Prices[index].EndDate - Prices[index].StartDate;
                        price += (((int)diff.TotalDays) * Prices[index].Price);
                        days -= (int)diff.TotalDays;
                    }
                    price += ((days - 1) * Prices[Prices.Count - 1].Price);

                }
            }
            return price;
        }
        public void AddReservationRoomType(DateTime CheckIn, DateTime CheckOut, ReservationRoomType type, ObservableCollection<RoomTypePrice> Prices,
            int Number, Type SelectedType)
        {
            int price = CalculatePrice(CheckIn, CheckOut, Prices);
            TimeSpan diff = CheckOut.Date - CheckIn.Date;
            int totalDays = (int)diff.TotalDays;

            if (RoomTypes.FirstOrDefault(p => p.RoomType == type.RoomType) == null)
            {
                RoomTypes.Add(type);
                if (CheckIn.Date.Month == CheckOut.Date.Month)
                    Bill.Add(new Bill() { Name = SelectedType.TypeName, Quantity = Number, NumberOfNights = totalDays, TotalPrice = price * Number });
                else
                    Bill.Add(new Bill()
                    {
                        Name = SelectedType.TypeName,
                        Quantity = Number,
                        NumberOfNights = totalDays,
                        TotalPrice = price * Number
                    });

            }
            else
            {
                for (int index = 0; index < Bill.Count; ++index)
                {
                    if (Bill[index].Name == SelectedType.TypeName)
                    {
                        Bill[index].Quantity += Number;
                        Bill[index].TotalPrice *= Bill[index].Quantity;
                        break;
                    }
                }
                for (int index = 0; index < RoomTypes.Count; ++index)
                    if (RoomTypes[index].RoomType == type.RoomType)
                    {
                        RoomTypes[index].Number += Number;
                        break;
                    }
            }
            foreach (var elem in Bill)
                foreach (var rt in RoomTypes)
                    if (elem.Name == rt.RoomType.TypeName)
                    {
                        CanPay[0] = true;
                        break;
                    }
        }
    }
}
