﻿using Hotel.Models.BusinessLogicLayer;
using Hotel.Models.DataAccesLayer;
using Hotel.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Services
{
    public class RoomWindowCommands:ViewModels.VMPropertyChanged
    {
        RoomBLL room;
        public Room CurrentRoom;
        private ObservableCollection<bool> isEnable;
        public ObservableCollection<bool> IsEnable { get => isEnable; set { isEnable = value;NotifyPropertyChanged("IsEnable"); } }
        private ObservableCollection<Option> selectedOptions;
        public ObservableCollection<Option> SelectedOptions { get=>selectedOptions; set { selectedOptions = value;NotifyPropertyChanged("SelectedOptions"); } }
        public ObservableCollection<string> SelectedImages { get; set; }
        public ObservableCollection<Room> AvailableRooms { get; set; }

        public RoomWindowCommands(Room currentRoom, ObservableCollection<Option>  selectedOptions, ObservableCollection<string> selectedImages,
            ObservableCollection<Room> rooms, RoomBLL room)
        {
            SelectedOptions = selectedOptions;
            SelectedImages = selectedImages;
            AvailableRooms = rooms;
            this.room = room;
            CurrentRoom = currentRoom;
            IsEnable = new ObservableCollection<bool>() { Convert.ToBoolean(0) };
        }
        public void AddImages(ObservableCollection<string> images)
        {
            SelectedImages = images;
        }
        public void DeleteImage(string image)
        {
            SelectedImages.Remove(image);
            if (SelectedImages.Count == 0)
                IsEnable[0] = false;
            room.DeleteImage(CurrentRoom, image);
        }

        public void DeleteOption(Option option)
        {
            SelectedOptions.Remove(SelectedOptions.Where(p => p.OptionID == option.OptionID).SingleOrDefault());
            if (SelectedOptions.Count == 0)
                IsEnable[0] = false;
        }
        public void DeleteRoom(Room room)
        {
            AvailableRooms.FirstOrDefault(p => p.RoomID == room.RoomID).Deleted = true;
            AvailableRooms.Remove(AvailableRooms.Where(p => p.RoomID == room.RoomID).SingleOrDefault());
            SelectedImages.Clear();
            SelectedOptions.Clear();
            this.room.DeleteRoom(room);
            IsEnable[0] = false;
        }
        public void AddRoom(Room r)
        {
            AvailableRooms.Add(r);
            room.AddRoom(r);
        }
    }
}
