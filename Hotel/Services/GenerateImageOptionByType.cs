﻿using Hotel.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Type = Hotel.Models.EntityLayer.Type;

namespace Hotel.Services
{
    public class GenerateImageOptionByType
    {
        private Type RoomType{ get; set; }
        ObservableCollection<Option> AvailableOptions { get; set; }
        public ObservableCollection<Option> Options { get; set; }
        public ObservableCollection<string> Images { get; set; }
        public GenerateImageOptionByType(Type roomType,ObservableCollection<Option> options)
        {
            this.AvailableOptions = options;
            //this.AvailableImages = images;
            this.RoomType = roomType;
            Options = new ObservableCollection<Option>();
            Images = new ObservableCollection<string>();

        }

        private ObservableCollection<Option> GetOptionsById(params int[] id)
        {
            ObservableCollection<Option> result = new ObservableCollection<Option>();
            if (RoomType.TypeName!=null)
            foreach (int i in id)
                    if(AvailableOptions.FirstOrDefault(p => p.OptionID == i)!=null)
                result.Add(AvailableOptions.FirstOrDefault(p => p.OptionID == i));
            return result;
        }

        private ObservableCollection<string> GetImagsById(params int[] id)
        {
            ObservableCollection<string> result = new ObservableCollection<string>();
            if (RoomType.TypeName != null)
            {
                string path_2 = @"C:\Users\Edy\Desktop\Hotel";
                string path = path_2 + @"\Images\" + RoomType.TypeName + @"\";
                foreach (int i in id)
                    result.Add(path + Convert.ToString(i) + ".jpg");
            }
            return result;
        }
        public void Generate()
        {
            Images = GetImagsById(1, 2, 3);
            switch (RoomType.TypeName)
            {
                case "Single":
                    Options = GetOptionsById(1, 2, 3);
                    break;
                case "Double":
                    Options = GetOptionsById(1, 2, 3, 4);
                    break;
                case "Triple":
                    Options = GetOptionsById(1, 2, 3, 4,5);
                    break;
                case "Queen":
                    Options = GetOptionsById(1, 2, 3, 4, 5);
                    break;
                case "King":
                    Options = GetOptionsById(1, 2, 3, 4, 5);
                    break;
            }
        }
    }
}
