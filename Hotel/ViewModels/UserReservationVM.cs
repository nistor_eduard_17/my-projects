﻿using Hotel.Models.BusinessLogicLayer;
using Hotel.Models.EntityLayer;
using Hotel.Services;
using Hotel.ViewModels.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Type = Hotel.Models.EntityLayer.Type;

namespace Hotel.ViewModels
{
    public class UserReservationVM : VMPropertyChanged
    {
        RoomTypePriceBLL roomTypePriceBLL = new RoomTypePriceBLL();
        ReservationBLL reservationBLL = new ReservationBLL();
        RoomBLL roomBLL = new RoomBLL();
        ServiceBLL serviceBLL = new ServiceBLL();
        UserReservationWindowCommands userWindow;
        private bool canModifyValues;
        private bool canModifyDates;
        public Offer SelectedOffer { get; set; }
        public bool CanModifyValues { get => canModifyValues; set { canModifyValues = value; NotifyPropertyChanged("CanModifyValues"); } }
        public bool CanModifyDates { get => canModifyDates; set { canModifyDates = value;NotifyPropertyChanged("CanModifyDates"); } }
        public UserReservationVM(Offer offer, User user)
        {
            CanPay = new ObservableCollection<bool>();
            CanModifyValues = true;
            CanPay.Add(Convert.ToBoolean(0));
            RoomTypes = new ObservableCollection<ReservationRoomType>();
            Bill = new ObservableCollection<Bill>();
            AddedServices = new ObservableCollection<Service>();
            userWindow = new UserReservationWindowCommands(RoomTypes, Bill, AddedServices, Sum, CanPay);
            Username = user.Username;
            UserType = user.UserType;
            reservationBLL.Username = Username;
            AllReservations = reservationBLL.GetAllReservations();
            Numbers = new ObservableCollection<int>();
            AvailableServices = serviceBLL.GetAllServices();
            Prices = new ObservableCollection<RoomTypePrice>();
            SelectedOffer = offer;
            if (offer.OfferID != 0)
            {
                InitializationForOffer();
                userWindow.SelectedOffer = SelectedOffer;
                reservationBLL.SelectedOffer = SelectedOffer;
                CanModifyDates = false;
            }
            else
            {
                CanModifyValues = true;
                CanModifyDates = true;
            }
        }
        public void InitializationForOffer()
        {
            CanModifyValues = true;
            CheckIn = SelectedOffer.StartDate;
            CheckOut = SelectedOffer.EndDate;
            AvailableTypes.Clear();
            AvailableTypes.Add(SelectedOffer.RoomType);
            foreach (Service s in SelectedOffer.Services)
                AddedServices.Add(new Service() { ServiceID = s.ServiceID, Deleted = s.Deleted, ServiceType = s.ServiceType, Price = 0 });
        }
        public ObservableCollection<Bill> Bill
        {
            get => userWindow == null ? new ObservableCollection<Bill>() : userWindow.Bill;
            set
            {
                if (userWindow != null)
                {
                    userWindow.Bill = value;
                }
                NotifyPropertyChanged("Bill");
            }
        }
        public string Username { get; set; }
        public string UserType { get; set; }
        private DateTime checkIn { get; set; }
        private Type selectedType;
        private DateTime checkOut { get; set; }
        private ObservableCollection<Type> availableTypes;
        private int numberOfAvailableRooms;
        private int number;
        private bool isEnabled;
        public ObservableCollection<bool> CanPay
        {
            get => userWindow == null ? new ObservableCollection<bool>() : userWindow.CanPay;
            set
            {
                if (userWindow != null)
                    userWindow.CanPay = value;
                NotifyPropertyChanged("CanPay");
            }
        }
        private Service selectedService;
        public ObservableCollection<int> Sum
        {
            get => userWindow == null ? new ObservableCollection<int>() : userWindow.Sum; set
            {
                userWindow.Sum = value;
                NotifyPropertyChanged("Sum");
            }
        }
        public bool IsEnabled { get => isEnabled; set { isEnabled = value; NotifyPropertyChanged("IsEnabled"); } }
        private ObservableCollection<int> numbers;
        public ObservableCollection<RoomTypePrice> Prices { get; set; }
        public ObservableCollection<Service> AddedServices
        {
            get => userWindow == null ? new ObservableCollection<Service>() : userWindow.AddedServices;
            set
            {
                if (userWindow != null)
                    userWindow.AddedServices = value;

                NotifyPropertyChanged("AddedServices");
            }
        }
        public ObservableCollection<Service> AvailableServices { get => serviceBLL.ServicesList; set { serviceBLL.ServicesList = value; NotifyPropertyChanged("AvailableServices"); } }
        public ObservableCollection<Reservation> AllReservations { get => reservationBLL.ReservationList; set { reservationBLL.ReservationList = value; NotifyPropertyChanged("AllReservations"); } }
        public ObservableCollection<Type> AvailableTypes
        {
            get => availableTypes;
            set
            {
                if (value != null)
                {
                    availableTypes = value;
                }
                NotifyPropertyChanged("AvailableTypes");
            }
        }
        public ObservableCollection<ReservationRoomType> RoomTypes
        {
            get
            {
                if (userWindow != null)
                    return userWindow.RoomTypes;
                return new ObservableCollection<ReservationRoomType>();
            }
            set
            {
                if (userWindow != null)
                    userWindow.RoomTypes = value;
                NotifyPropertyChanged("RoomTypes");
            }
        }
        public ObservableCollection<int> Numbers
        {
            get => numbers;
            set
            {
                numbers = value;
                NotifyPropertyChanged("Numbers");
            }
        }
        public DateTime LastDate { get => roomTypePriceBLL.GetLastAvailableDate(); }
        public DateTime CheckIn
        {
            get => checkIn;
            set
            {
                if (value != null)
                {
                    if (AvailableTypes != null && AvailableTypes.Count != 0)
                    {
                        AvailableTypes.Clear();
                        RoomTypes.Clear();
                        RoomTypes.Clear();
                        Bill.Clear();
                        Prices.Clear();
                        CheckOut = new DateTime();
                    }
                    checkIn = value;
                    if (CheckOut.Date != new DateTime())
                        AvailableTypes = reservationBLL.AvailableTypes(CheckIn, CheckOut);
                }

                NotifyPropertyChanged("CheckIn");
            }
        }
        public DateTime CheckOut
        {
            get => checkOut;
            set
            {
                if (value != null)
                {
                    checkOut = value;
                    if (checkIn.Date != new DateTime() && value.Date != new DateTime())
                        AvailableTypes = reservationBLL.AvailableTypes(CheckIn, CheckOut);
                }
                NotifyPropertyChanged("CheckOut");
            }
        }
        public int Number
        {
            get => number;
            set
            {
                if (value == 0)
                    IsEnabled = false;
                number = value;
                if (value != 0)
                {
                    IsEnabled = true;
                    if (SelectedOffer.OfferID != 0)
                        CanModifyValues = false;
                }
                NotifyPropertyChanged("Number");
            }
        }
        public Type SelectedType
        {
            get => selectedType;
            set
            {
                if (value != null)
                {
                    selectedType = value;
                    Numbers.Clear();
                    numberOfAvailableRooms = reservationBLL.GetNumberOfAvailableRoomType(CheckIn, CheckOut, value.RoomType);
                    if (RoomTypes != null)
                        if (RoomTypes.FirstOrDefault(p => p.RoomType.RoomType == value.RoomType) != null)
                            numberOfAvailableRooms -= RoomTypes.FirstOrDefault(p => p.RoomType.RoomType == value.RoomType).Number;
                    for (int index = 1; index <= numberOfAvailableRooms; ++index)
                        Numbers.Add(index);
                    if (SelectedOffer.OfferID != 0)
                    {
                        if (numberOfAvailableRooms != 0)
                        {
                            Numbers.Clear();
                            Numbers.Add(1);
                        }
                    }

                }
                NotifyPropertyChanged("SelectedType");
            }
        }
        public Service SelectedService
        {
            get => selectedService;
            set
            {
                if (value != null)
                {
                    selectedService = value;
                    if (AddedServices.FirstOrDefault(p => p.ServiceID == value.ServiceID) == null)
                    {
                        AddedServices.Add(value);
                        Bill.Add(new Bill() { Name = value.ServiceType, Quantity = 1, TotalPrice = value.Price });
                        Sum = new ObservableCollection<int>() { 0 };
                        foreach (var elem in Bill)
                            Sum[0] += elem.TotalPrice;
                    }
                }
                NotifyPropertyChanged("SelectedService");
            }
        }
        public void AddReservationRoomType()
        {
            ReservationRoomType type = new ReservationRoomType() { Number = Number, RoomType = SelectedType }; ;
            if (SelectedOffer.OfferID == 0)
            {
                Prices = roomTypePriceBLL.GetRoomTypePriceForPeriod(CheckIn, CheckOut, SelectedType.RoomType);
                userWindow.AddReservationRoomType(CheckIn, CheckOut, type, Prices, Number, SelectedType);
                numberOfAvailableRooms -= (RoomTypes.Where(p => p.RoomType == SelectedType).Single()).Number;
                Numbers.Clear();
                for (int index = 1; index <= numberOfAvailableRooms; ++index)
                    Numbers.Add(index);
                Sum = new ObservableCollection<int>() { 0 };
                foreach (var elem in Bill)
                    Sum[0] += elem.TotalPrice;
                Number = 0;
                return;
            }
            RoomTypes.Add(type);
            CanPay[0] = true;
            Bill.Add(new Bill() { Name = SelectedOffer.OfferName, NumberOfNights = (int)(CheckOut.Date - CheckIn.Date).TotalDays, Quantity = 1, TotalPrice = SelectedOffer.Price });
            Sum = new ObservableCollection<int>() { 0 };
            foreach (var elem in Bill)
                Sum[0] += elem.TotalPrice;


        }

        private ICommand deleteRoomOptionCommand;
        public ICommand DeleteRoomOptionCommand
        {
            get
            {
                if (deleteRoomOptionCommand == null)
                {
                    deleteRoomOptionCommand = new RelayCommand<ReservationRoomType>(userWindow.DeleteRoomType);
                }
                return deleteRoomOptionCommand;
            }
        }

        private ICommand deleteServiceCommand;
        public ICommand DeleteServiceCommand
        {
            get
            {
                if (deleteServiceCommand == null)
                {
                    deleteServiceCommand = new RelayCommand<Service>(userWindow.DeleteServices);
                }
                return deleteServiceCommand;
            }
        }

        private ICommand addReservationCommand;
        public ICommand AddReservationCommand
        {
            get
            {
                if (addReservationCommand == null)
                {
                    addReservationCommand = new RelayCommand<Reservation>(reservationBLL.AddReservation);
                }
                return addReservationCommand;
            }
        }
    }
}
