﻿using Hotel.Models.BusinessLogicLayer;
using Hotel.Models.EntityLayer;
using Hotel.ViewModels.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Type = Hotel.Models.EntityLayer.Type;

namespace Hotel.ViewModels
{
    public class AdminOfferVM : VMPropertyChanged
    {
        OfferBLL offerBLL = new OfferBLL();
        TypeBLL type = new TypeBLL();
        ServiceBLL serviceBLL = new ServiceBLL();
        private Type selectedType;
        private Service selectedService;
        private Offer selectedOffer;
        public ObservableCollection<Offer> AvailableOffers { get => offerBLL.OffersList; set { offerBLL.OffersList = value; NotifyPropertyChanged("AvailableOffers"); } }
        public ObservableCollection<Type> Types { get => type.TypesList; set { type.TypesList = value; NotifyPropertyChanged("Types"); } }
        public ObservableCollection<Service> Services { get => serviceBLL.ServicesList; set { serviceBLL.ServicesList = value; NotifyPropertyChanged("Services"); } }
        public ObservableCollection<Service> SelectedServices { get => offerBLL.InterfaceServices; set { offerBLL.InterfaceServices = value; NotifyPropertyChanged("SelectedServices"); } }

        public AdminOfferVM()
        {
            AvailableOffers = offerBLL.GetAllOffers();
            Types = type.GetAllTypes();
            Services = serviceBLL.GetAllServices();
        }

        public Offer SelectedOffer
        {
            get => selectedOffer;
            set
            {
                selectedOffer = value;
                if (value != null)
                    SelectedServices = value.Services;
                NotifyPropertyChanged("SelectedOffer");
            }
        }
        public Service SelectedService
        {
            get => selectedService;
            set
            {
                selectedService = value;
                if (value != null)
                    if (SelectedServices.FirstOrDefault(p => p.ServiceID == value.ServiceID) == null)
                        SelectedServices.Add(selectedService);
                NotifyPropertyChanged("SelectedService");
            }
        }
        public Type SelectedType
        {
            get => selectedType;
            set
            {
                selectedType = value;
                NotifyPropertyChanged("SelectedType");
            }
        }

        private ICommand addOfferCommand;
        public ICommand AddOfferCommand
        {
            get
            {
                if (addOfferCommand == null)
                {
                    addOfferCommand = new RelayCommand<Offer>(offerBLL.AddOffer);
                }

                return addOfferCommand;
            }
        }

        private ICommand modifyOfferCommand;
        public ICommand ModifyOfferCommand
        {
            get
            {
                if (modifyOfferCommand == null)
                {
                    modifyOfferCommand = new RelayCommand<Offer>(offerBLL.ModifyOffer);
                }

                return modifyOfferCommand;
            }
        }

        private ICommand deleteOfferCommand;
        public ICommand DeleteOfferCommand
        {
            get
            {
                if (deleteOfferCommand == null)
                {
                    deleteOfferCommand = new RelayCommand<Offer>(offerBLL.DeleteOffer);
                }

                return deleteOfferCommand;
            }
        }

        private ICommand deleteOfferServiceCommand;
        public ICommand DeleteOfferServiceCommand
        {
            get
            {
                if (deleteOfferServiceCommand == null)
                {
                    deleteOfferServiceCommand = new RelayCommand<Service>(offerBLL.RemoveServiceFromInterface);
                }

                return deleteOfferServiceCommand;
            }
        }
    }
}
