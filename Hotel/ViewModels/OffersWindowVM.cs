﻿using Hotel.Models.BusinessLogicLayer;
using Hotel.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Type = Hotel.Models.EntityLayer.Type;

namespace Hotel.ViewModels
{
    public class OffersWindowVM : VMPropertyChanged
    {
        OfferBLL offerBLL = new OfferBLL();
        TypeBLL type = new TypeBLL();
        ServiceBLL serviceBLL = new ServiceBLL();
        ReservationBLL reservationBLL = new ReservationBLL();
        public User User { get; set; }
        public ObservableCollection<Offer> AvailableOffers { get => offerBLL.OffersList; set { offerBLL.OffersList = value; NotifyPropertyChanged("AvailableOffers"); } }
        public ObservableCollection<Type> Types { get => type.TypesList; set { type.TypesList = value; NotifyPropertyChanged("Types"); } }
        public ObservableCollection<Service> Services { get => serviceBLL.ServicesList; set { serviceBLL.ServicesList = value; NotifyPropertyChanged("Services"); } }
        public ObservableCollection<Service> SelectedServices { get => offerBLL.InterfaceServices; set { offerBLL.InterfaceServices = value; NotifyPropertyChanged("SelectedServices"); } }

        private Offer selectedOffer;
        public Offer SelectedOffer { get => selectedOffer; set { selectedOffer = value; NotifyPropertyChanged("SelectedOffer"); } }
        public OffersWindowVM(User user)
        {
            AvailableOffers = offerBLL.GetAllOffers();
            var availableOffers = AvailableOffers.Where(p => p.StartDate >= DateTime.Today).ToList();
            AvailableOffers.Clear();
            foreach (var elem in availableOffers)
            {
                if (reservationBLL.GetNumberOfAvailableRoomType(elem.StartDate, elem.EndDate, elem.RoomType.RoomType) != 0)
                    AvailableOffers.Add(elem);
            }
            Types = type.GetAllTypes();
            Services = serviceBLL.GetAllServices();
            User = user;
        }
    }
}
