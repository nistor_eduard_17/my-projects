﻿using Hotel.Models.BusinessLogicLayer;
using Hotel.Models.EntityLayer;
using Hotel.ViewModels.Commands;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace Hotel.ViewModels
{
    public class UserVM : VMPropertyChanged
    {
        UserBLL userBLL = new UserBLL();
        public UserVM()
        {
            UsersList = userBLL.GetAllUsers();
        }
        public ObservableCollection<User> UsersList
        {
            get => userBLL.UsersList;
            set
            {
                userBLL.UsersList = value;
                NotifyPropertyChanged("UsersList");

            }
        }
        private string username;
        private string userType;
        private string password;
        public bool CanExecute { get; set; }
        public string Username
        {
            get => username;
            set
            {
                username = value;
                if (Password != null)
                    UserType = userBLL.GetUserType(userBLL.GetUser(new User() { Password = Password, Username = Username }));
                NotifyPropertyChanged("Username");
            }
        }
        public string UserType
        {
            get => userType;
            set
            {
                userType = value;
                NotifyPropertyChanged("UserType");
            }
        }
        public string Password
        {
            get => password;
            set
            {
                password = value;
                if (Username != null)
                    UserType = userBLL.GetUserType(userBLL.GetUser(new User() { Password = Password, Username = Username }));
                NotifyPropertyChanged("Password");
            }
        }



        private ICommand addCommand;
        public ICommand AddCommand
        {
            get
            {
                if (addCommand == null)
                {
                    addCommand = new RelayCommand<User>(userBLL.AddUser, param => MyValidator.CanExecuteOperation(UsersList, Username, UserType, Password));
                }
                return addCommand;
            }
        }
    }
}
