﻿using Hotel.Models.BusinessLogicLayer;
using Hotel.Models.EntityLayer;
using Hotel.Services;
using Hotel.ViewModels.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Hotel.ViewModels
{
    public class UserHistoryVM : VMPropertyChanged
    {
        public User User { get; set; }
        ReservationBLL reservationBLL = new ReservationBLL();
        UserReservationWindowCommands userReservation;
        RoomTypePriceBLL roomTypePriceBLL = new RoomTypePriceBLL();
        OfferBLL offerBLL = new OfferBLL();
        private ObservableCollection<string> allStatuses;
        public ObservableCollection<string> AllStatuses { get => allStatuses; set { allStatuses = value; NotifyPropertyChanged("AllStatuses"); } }
        public ObservableCollection<Reservation> Reservations { get => reservationBLL.ReservationList; set { reservationBLL.ReservationList = value; NotifyPropertyChanged("Resevations"); } }
        public UserHistoryVM(User user)
        {
            User = user;
            UpdateReservations(user);
        }
        public void UpdateReservations(User user)
        {
            Reservations = reservationBLL.GetAllReservationsForUser(reservationBLL.GetUserIDByName(user.Username));
            userReservation = new UserReservationWindowCommands(new ObservableCollection<ReservationRoomType>(), new ObservableCollection<Bill>(), new ObservableCollection<Service>(), new ObservableCollection<int>(), new ObservableCollection<bool>());
            for (int index = 0; index < Reservations.Count; ++index)
            {
                int totalPrice = 0, numberOfRooms = 0;
                ObservableCollection<RoomTypePrice> Prices;
                ObservableCollection<Service> offerServices;
                if (Reservations[index].OfferID > 0)
                {
                    Reservations[index].TotalPrice = reservationBLL.GetOfferPriceFromReservation(Reservations[index].ReservationID);
                    Reservations[index].NumberOfRooms = 1;
                    offerServices = offerBLL.GetOfferServices(Reservations[index].OfferID);
                    foreach (var s in Reservations[index].Services)
                        if (offerServices.FirstOrDefault(p => p.ServiceID == s.ServiceID) == null)
                            Reservations[index].TotalPrice += s.Price;

                }
                else
                {
                    foreach (var roomTypePrice in Reservations[index].RoomTypes)
                    {
                        Prices = roomTypePriceBLL.GetRoomTypePriceForPeriod(Reservations[index].StartDate, Reservations[index].EndDate, roomTypePrice.RoomType.RoomType);
                        totalPrice = userReservation.CalculatePrice(Reservations[index].StartDate, Reservations[index].EndDate, Prices);
                        totalPrice *= roomTypePrice.Number;
                        numberOfRooms += roomTypePrice.Number;
                    }

                    foreach (var service in Reservations[index].Services)
                        totalPrice += service.Price;
                    Reservations[index].TotalPrice = totalPrice;
                    Reservations[index].NumberOfRooms = numberOfRooms;
                }
                if (Reservations[index].Status == "Canceled" || Reservations[index].Status == "Paid")
                    Reservations[index].AllStatuses = new ObservableCollection<string>() { Reservations[index].Status };
                else
                if (Reservations[index].StartDate <= DateTime.Today && Reservations[index].Status == "Active")
                {
                    Reservations[index].AllStatuses = new ObservableCollection<string> { "Paid" };
                    Reservations[index].Status = "Paid";
                    reservationBLL.UpdateStatus(Reservations[index]);
                }
                else
                    Reservations[index].AllStatuses = new ObservableCollection<string>() { "Active", "Canceled" };
            }
        }

        private ICommand updateReservationCommand;
        public ICommand UpdateReservationCommand
        {
            get
            {
                if (updateReservationCommand == null)
                {
                    updateReservationCommand = new RelayCommand<Reservation>(reservationBLL.UpdateStatus);
                }

                return updateReservationCommand;
            }
        }
    }
}
