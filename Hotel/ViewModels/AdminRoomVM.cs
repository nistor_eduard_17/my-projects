﻿using Hotel.Models.BusinessLogicLayer;
using Hotel.Models.EntityLayer;
using Hotel.Services;
using Hotel.ViewModels.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Type = Hotel.Models.EntityLayer.Type;

namespace Hotel.ViewModels
{
    public class AdminRoomVM : VMPropertyChanged
    {
        public AdminRoomVM()
        {
            AvailableOptions = option.GetAllOptions();
            AvailableRooms = room.GetAllRooms();
            AvailableTypes = type.GetAllTypes();
            roomWindow = new RoomWindowCommands(new Room(), new ObservableCollection<Option>(), new ObservableCollection<string>(), AvailableRooms, room);
            //SelectedOptions = new ObservableCollection<Option>();
            //SelectedImages = new ObservableCollection<string>();
            SelectedRoom = new Room();
            foreach (Room r in AvailableRooms)
                r.RoomTypeName = (AvailableTypes.FirstOrDefault(p => p.RoomType == r.RoomType).TypeName);
            IsEnable[0] = false;

        }
        public ObservableCollection<bool> IsEnable
        {
            get => roomWindow == null ? new ObservableCollection<bool> ():roomWindow.IsEnable; set
            {
                if (roomWindow != null)
                    roomWindow.IsEnable = value;
                NotifyPropertyChanged("IsEnable");
            }
        }
        private ObservableCollection<Option> selectedOptions;
        private ObservableCollection<string> selectedImages;
        public ObservableCollection<Option> AvailableOptions { get; set; }
        public ObservableCollection<Type> AvailableTypes { get; set; }
        public ObservableCollection<Room> AvailableRooms { get; set; }
        public ObservableCollection<Option> SelectedOptions
        {
            get => roomWindow == null ? new ObservableCollection<Option>() : roomWindow.SelectedOptions;
            set
            {
                if (roomWindow != null)
                {
                    roomWindow.SelectedOptions = value;
                    if (roomWindow.SelectedOptions.Count != 0)
                        IsEnable[0] = true;
 
                }
                NotifyPropertyChanged("SelectedOptions");
            }
        }

        RoomWindowCommands roomWindow;
        OptionBLL option = new OptionBLL();
        ServiceBLL service = new ServiceBLL();
        RoomBLL room = new RoomBLL();
        TypeBLL type = new TypeBLL();
        GenerateImageOptionByType generate;
        public ObservableCollection<string> SelectedImages
        {
            get => selectedImages; set
            {
                selectedImages = value;
                CanExecuteAddRoomCommand = MyValidator.CanAddRoom(SelectedOptions, SelectedImages, SelectedType);
                IsEnable[0] = CanExecuteAddRoomCommand;
                if (roomWindow != null)
                    roomWindow.SelectedImages = value;
                NotifyPropertyChanged("SelectedImages");
            }
        }
        public ObservableCollection<Type> RoomsTypes { get; set; }

        private string selectedImage;
        private Room selectedRoom;
        private Option selectedOption;
        private Type selectedType;

        public bool CanExecuteAddOptionCommand { get; set; }
        public bool CanExecuteAddRoomCommand { get; set; }

        public string SelectedImage
        {
            get => selectedImage;
            set
            {
                selectedImage = value;
                NotifyPropertyChanged("SelectedImage");
            }
        }
        public Room SelectedRoom
        {
            get => selectedRoom;
            set
            {
                selectedRoom = value;
                if (value != null)
                    if (selectedRoom.RoomID != 0)
                    {
                        SelectedImages = selectedRoom.Images;
                        SelectedOptions = selectedRoom.Options;
                        if (roomWindow != null)
                        { roomWindow.SelectedOptions = SelectedOptions; roomWindow.SelectedImages = SelectedImages; roomWindow.CurrentRoom = selectedRoom; }
                        SelectedType = new Type { RoomType = SelectedRoom.RoomType, TypeName = SelectedRoom.RoomTypeName };
                    }
                NotifyPropertyChanged("SelectedRoom");
            }
        }
        public Option SelectedOption
        {
            get => selectedOption;
            set
            {
                if (value != null)
                {
                    CanExecuteAddOptionCommand = true;
                    CanExecuteAddRoomCommand = MyValidator.CanAddRoom(SelectedOptions, SelectedImages, SelectedType);
                    IsEnable[0] = CanExecuteAddRoomCommand;
                }
                selectedOption = value;
                if (SelectedRoom.Options.FirstOrDefault(p => p.OptionID == selectedOption.OptionID) == null)
                    SelectedRoom.Options.Add(value);
                //else
                if (SelectedOptions.FirstOrDefault(p => p.OptionID == selectedOption.OptionID) == null)
                    SelectedOptions.Add(value);
                NotifyPropertyChanged("SelectedOption");
            }
        }
        public Type SelectedType
        {
            get => selectedType;
            set
            {

                if (value != null && value.TypeName != null)
                {
                    selectedType = value;
                    CanExecuteAddRoomCommand = MyValidator.CanAddRoom(SelectedOptions, SelectedImages, SelectedType);
                    IsEnable[0] = CanExecuteAddRoomCommand;
                    generate = new GenerateImageOptionByType(value, AvailableOptions);
                    generate.Generate();
                    SelectedOptions = generate.Options;
                    SelectedImages = generate.Images;
                    roomWindow.SelectedOptions = SelectedOptions; roomWindow.SelectedImages = SelectedImages;
                    if (SelectedRoom != null && SelectedRoom.RoomID != 0)
                    {
                        SelectedRoom.RoomType = SelectedType.RoomType;
                        selectedRoom.RoomTypeName = SelectedType.TypeName;
                        SelectedOptions = SelectedRoom.Options; SelectedImages = SelectedRoom.Images;
                        roomWindow.SelectedOptions = SelectedOptions; roomWindow.SelectedImages = SelectedImages;
                    }

                }
                NotifyPropertyChanged("SelectedType");
            }
        }

        private ICommand addImagesCommand;
        public ICommand AddImagesCommand
        {
            get
            {
                if (addImagesCommand == null)
                {
                    addImagesCommand = new RelayCommand<ObservableCollection<string>>(roomWindow.AddImages);
                }
                return addImagesCommand;
            }
        }

        private ICommand deleteImagesCommand;
        public ICommand DeleteImagesCommand
        {
            get
            {
                if (deleteImagesCommand == null)
                {
                    deleteImagesCommand = new RelayCommand<string>(roomWindow.DeleteImage);
                }
                else
                    SelectedImages = roomWindow.SelectedImages;
                return deleteImagesCommand;
            }
        }

        private ICommand deleteOptionCommand;
        public ICommand DeleteOptionCommand
        {
            get
            {
                if (deleteOptionCommand == null)
                {
                    deleteOptionCommand = new RelayCommand<Option>(roomWindow.DeleteOption);
                }
                else
                    SelectedOptions = roomWindow.SelectedOptions;

                return deleteOptionCommand;
            }
        }

        private ICommand deleteRoomCommand;
        public ICommand DeleteRoomCommand
        {
            get
            {
                if (deleteRoomCommand == null)
                {
                    deleteRoomCommand = new RelayCommand<Room>(roomWindow.DeleteRoom);
                }
                return deleteRoomCommand;
            }
        }

        private ICommand addRoomCommand;
        public ICommand AddRoomCommand
        {
            get
            {
                if (addRoomCommand == null)
                {
                    addRoomCommand = new RelayCommand<Room>(roomWindow.AddRoom);
                }
                return addRoomCommand;
            }
        }

        private ICommand modifyRoomCommand;
        public ICommand ModifyRoomCommand
        {
            get
            {
                if (modifyRoomCommand == null)
                {
                    modifyRoomCommand = new RelayCommand<Room>(room.ModifyRoom);
                }
                return modifyRoomCommand;
            }
        }
        public void AddImages(string s)
        {
            if (!SelectedImages.Contains(s))
                SelectedImages.Add(s);
            CanExecuteAddRoomCommand = MyValidator.CanAddRoom(SelectedOptions, SelectedImages, SelectedType);
        }

    }
}
