﻿using Hotel.Models.EntityLayer;
using System.Collections.ObjectModel;
using System.Linq;

namespace Hotel.ViewModels
{
    static class MyValidator
    {
        public static bool CanExecuteOperation(ObservableCollection<User> users, string username, string userType, string password)
        {
            if (username != null && userType != null && password != null)
                if (users.FirstOrDefault(p => p.Username == username && p.Password == password) != null)
                    return true;

            if (username != null && userType != null && password != null && users.FirstOrDefault(p => p.Username == username) == null)
                return true;

            return false;
        }

        public static bool CanAddRoom(ObservableCollection<Option> options, ObservableCollection<string> image,
            Type type)
        {
            if (options != null && image != null)
                if (options.Count >= 1 && image.Count >= 1 && type != null)
                    return true;
            return false;
        }
    }
}
