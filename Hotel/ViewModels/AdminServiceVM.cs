﻿using Hotel.Models.BusinessLogicLayer;
using Hotel.Models.EntityLayer;
using Hotel.ViewModels.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Hotel.ViewModels
{
    public class AdminServiceVM:BasePropertyChanged
    {
        private ServiceBLL serviceBLL = new ServiceBLL();
        public string ServiceName { get => NewService.ServiceType; set { NewService.ServiceType = value; NotifyPropertyChanged("ServiceName"); } }
        public int ServicePrice { get => NewService.Price; set { NewService.Price = value; NotifyPropertyChanged("ServiceName"); } }
        public ObservableCollection<Service> AllServices { get => serviceBLL.ServicesList; set { serviceBLL.ServicesList = value; NotifyPropertyChanged("AllServices"); } }

        private Service selectedService;
        private Service newService;

        public Service SelectedService
        {
            get => selectedService;
            set
            {
                selectedService = value;
                NotifyPropertyChanged("SelectedService");
            }
        }
        public Service NewService
        {
            get => newService;
            set
            {
                newService = value;
                NotifyPropertyChanged("NewOption");
            }
        }

        public AdminServiceVM()
        {
            AllServices = serviceBLL.GetAllServices();
            NewService = new Service();
        }

        private ICommand addServiceCommand;
        public ICommand AddServiceCommand
        {
            get
            {
                if (addServiceCommand == null)
                {
                    addServiceCommand = new RelayCommand<Service>(serviceBLL.AddService);
                }

                return addServiceCommand;
            }
        }

        private ICommand modifyServiceCommand;
        public ICommand ModifyServiceCommand
        {
            get
            {
                if (modifyServiceCommand == null)
                {
                    modifyServiceCommand = new RelayCommand<Service>(serviceBLL.ModifyService);
                }

                return modifyServiceCommand;
            }
        }

        private ICommand deleteServiceCommand;
        public ICommand DeleteServiceCommand
        {
            get
            {
                if (deleteServiceCommand == null)
                {
                    deleteServiceCommand = new RelayCommand<Service>(serviceBLL.DeleteService);
                }

                return deleteServiceCommand;
            }
        }
    }
}
