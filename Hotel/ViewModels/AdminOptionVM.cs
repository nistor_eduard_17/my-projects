﻿using Hotel.Models.BusinessLogicLayer;
using Hotel.Models.EntityLayer;
using Hotel.ViewModels.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Hotel.ViewModels
{
    public class AdminOptionVM : BasePropertyChanged
    {
        private OptionBLL optionBLL = new OptionBLL();
        public string OptionName { get => NewOption.OptionType; set { NewOption.OptionType = value; NotifyPropertyChanged("OptionName"); } }
        public ObservableCollection<Option> AllOptions { get => optionBLL.OptionsList; set { optionBLL.OptionsList = value; NotifyPropertyChanged("AllOptions"); } }

        public List<Option> VisibleOptions { get => AllOptions.ToList(); }
        private Option selectedOption;
        private Option newOption;

        public Option SelectedOption
        {
            get => selectedOption;
            set
            {
                selectedOption = value;
                NotifyPropertyChanged("SelectedOption");
            }
        }
        public Option NewOption
        {
            get => newOption;
            set
            {
                newOption = value;
                NotifyPropertyChanged("NewOption");
            }
        }

        public AdminOptionVM()
        {
            AllOptions = optionBLL.GetAllOptions();
            NewOption = new Option();
        }

        private ICommand addOptionCommand;
        public ICommand AddOptionCommand
        {
            get
            {
                if (addOptionCommand == null)
                {
                    addOptionCommand = new RelayCommand<Option>(optionBLL.AddOption);
                }

                return addOptionCommand;
            }
        }

        private ICommand modifyOptionCommand;
        public ICommand ModifyOptionCommand
        {
            get
            {
                if (modifyOptionCommand == null)
                {
                    modifyOptionCommand = new RelayCommand<Option>(optionBLL.ModifyOption);
                }

                return modifyOptionCommand;
            }
        }

        private ICommand deleteOptionCommand;
        public ICommand DeleteOptionCommand
        {
            get
            {
                if (deleteOptionCommand == null)
                {
                    deleteOptionCommand = new RelayCommand<Option>(optionBLL.DeleteOption);
                }

                return deleteOptionCommand;
            }
        }
    }
}
