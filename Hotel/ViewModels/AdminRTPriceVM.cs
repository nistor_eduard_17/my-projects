﻿using Hotel.Models.BusinessLogicLayer;
using Hotel.Models.EntityLayer;
using Hotel.ViewModels.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Type = Hotel.Models.EntityLayer.Type;

namespace Hotel.ViewModels
{
    public class AdminRTPriceVM : VMPropertyChanged
    {
        private Type selectedType;
        public ObservableCollection<RoomTypePrice> Prices { get => price.RTPriceList; set { price.RTPriceList = value; NotifyPropertyChanged("Prices"); } }
        public ObservableCollection<Type> Types { get => type.TypesList; set { type.TypesList = value; NotifyPropertyChanged("Types"); } }
        RoomTypePriceBLL price = new RoomTypePriceBLL();
        TypeBLL type = new TypeBLL();

        public AdminRTPriceVM()
        {
            Prices = price.GetAllPrices();
            Types = type.GetAllTypes();
        }
        public Type SelectedType
        {
            get => selectedType;
            set
            {
                if (value != null)
                    selectedType = value;
                NotifyPropertyChanged("SelectedType");
            }
        }

        private ICommand addPriceCommand;
        public ICommand AddPriceCommand
        {
            get
            {
                if (addPriceCommand == null)
                {
                    addPriceCommand = new RelayCommand<RoomTypePrice>(price.AddRoomTypePrice);
                }

                return addPriceCommand;
            }
        }

        private ICommand modifyPriceCommand;
        public ICommand ModifyPriceCommand
        {
            get
            {
                if (modifyPriceCommand == null)
                {
                    modifyPriceCommand = new RelayCommand<RoomTypePrice>(price.ModifyRoomTypePrice);
                }

                return modifyPriceCommand;
            }
        }

        private ICommand deletePriceCommand;
        public ICommand DeletePriceCommand
        {
            get
            {
                if (deletePriceCommand == null)
                {
                    deletePriceCommand = new RelayCommand<RoomTypePrice>(price.DeleteRoomTypePrice);
                }

                return deletePriceCommand;
            }
        }
    }

}
