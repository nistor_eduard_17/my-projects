﻿using Hotel.Models.BusinessLogicLayer;
using Hotel.Models.EntityLayer;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using Type = Hotel.Models.EntityLayer.Type;

namespace Hotel.ViewModels
{
    public class HotelMainWindowVM : VMPropertyChanged
    {
        RoomBLL roomBLL = new RoomBLL();
        RoomTypePriceBLL roomTypePriceBLL = new RoomTypePriceBLL();
        TypeBLL types = new TypeBLL();
        public string Username { get; set; }
        private DateTime checkIn { get; set; }
        private DateTime checkOut { get; set; }
        public string UserType { get; set; }
        public DateTime LastDate { get => roomTypePriceBLL.GetLastAvailableDate(); }
        private string isEnabledAdminBtn;
        private string isEnabledHistoryBtn;
        public string IsEnabledAdminBtn { get=>isEnabledAdminBtn; set { isEnabledAdminBtn = value;NotifyPropertyChanged("IsEnabledAdminBtn"); } }
        public string IsEnabledHistoryBtn { get=>isEnabledHistoryBtn; set { isEnabledHistoryBtn = value;NotifyPropertyChanged("IsEnabledHistoryBtn"); } }
        private string enable;
        public string Enable { get => enable; set { enable = value;NotifyPropertyChanged("Enable"); } }
        public DateTime CheckIn
        {
            get => checkIn;
            set
            {
                if (value != null)
                    checkIn = value;
                NotifyPropertyChanged("CheckIn");
            }
        }
        public DateTime CheckOut
        {
            get => checkOut;
            set
            {
                if (value != null)
                    checkOut = value;
                NotifyPropertyChanged("CheckOut");
            }
        }
        public HotelMainWindowVM(User user)
        {
            IsEnabledAdminBtn = "Hidden";
            IsEnabledHistoryBtn = "Hidden";
            Enable = "Hidden";
            Username = user.Username;
            UserType = user.UserType;
            if (UserType == "Admin")
                IsEnabledAdminBtn = "Visible";
            if (UserType == "Client")
                IsEnabledHistoryBtn = "Visible";
            if (UserType == "Employee")
                Enable = "Visible";
            AllRooms = roomBLL.GetAllRooms();
        }
        public ObservableCollection<Room> AllRooms { get => roomBLL.RoomsList; set { roomBLL.RoomsList = value; NotifyPropertyChanged("AllRooms"); } }
        public void ButtonPressed()
        {
            TimeSpan diff = CheckOut.Date - CheckIn.Date;
            int days = (int)diff.TotalDays;
            if (CheckIn.Date != new DateTime() && CheckOut.Date != new DateTime())
            {
                AllRooms = roomBLL.GetAllAvailableRooms(CheckIn, CheckOut);
                ObservableCollection<Type> AllTypes = types.GetAllTypes();
                foreach (Type t in AllTypes)
                {
                    ObservableCollection<RoomTypePrice> prices = roomTypePriceBLL.GetRoomTypePriceForPeriod(CheckIn, CheckOut, t.RoomType);
                    if (CheckIn.Date.Month == CheckOut.Date.Month)
                    {
                        int price = (days) * prices[0].Price;
                        for (int index = 0; index < AllRooms.Count; ++index)
                            if (AllRooms[index].RoomType == t.RoomType)
                                AllRooms[index].TodayPrice = price;
                    }
                    else
                    {
                        diff = prices[0].EndDate - CheckIn.Date;
                        int price = 0;
                        if (prices.Count == 2)
                            price = ((int)diff.TotalDays) * prices[0].Price + (days - (int)diff.TotalDays - 1) * prices[1].Price;
                        else
                        {
                            price = ((int)diff.TotalDays) * prices[0].Price;
                            days -= (int)diff.TotalDays;
                            for (int index = 1; index < prices.Count - 1; ++index)
                            {
                                diff = prices[index].EndDate - prices[index].StartDate;
                                price += (((int)diff.TotalDays) * prices[index].Price);
                                days -= (int)diff.TotalDays;
                            }
                            price += ((days - 1) * prices[prices.Count - 1].Price);

                        }
                        for (int index = 0; index < AllRooms.Count; ++index)
                            if (AllRooms[index].RoomType == t.RoomType)
                                AllRooms[index].TodayPrice = price;
                    }
                }

            }
        }

    }
}
