﻿using Hotel.Models.DataAccesLayer;
using Hotel.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Type = Hotel.Models.EntityLayer.Type;

namespace Hotel.Models.BusinessLogicLayer
{
    public class ReservationBLL
    {
        public ObservableCollection<Reservation> ReservationList { get; set; }
        ReservationDAL reservation = new ReservationDAL();
        RoomBLL roomBLL = new RoomBLL();
        UserBLL userBLL = new UserBLL();
        public Offer SelectedOffer;
        public string Username { get; set; }
        public ReservationBLL()
        {
            ReservationList = new ObservableCollection<Reservation>();
        }
        public ObservableCollection<Reservation> GetAllReservations()
        {
            return reservation.GetAllReservations();
        }
        public ObservableCollection<Type> AvailableTypes(DateTime startDate, DateTime endDate)
        {
            ObservableCollection<Room> availableRooms = roomBLL.GetAllAvailableRooms(startDate, endDate);
            ObservableCollection<Type> result = new ObservableCollection<Type>();
            foreach (Room room in availableRooms)
            {
                bool valid = true;
                Type t = new Type() { RoomType = room.RoomType, TypeName = room.RoomTypeName };
                foreach (Type type in result)
                    if (type.RoomType == t.RoomType)
                    {
                        valid = false;
                        if (valid == false)
                            break;
                    }
                if (valid == true)
                    result.Add(t);
            }
            return result;

        }
        public ObservableCollection<ReservationRoomType> GetReservationRoomTypesByReservationID(int reservationID)
        {
            return reservation.GetReservationRoomTypesByReservationID(reservationID);
        }
        public ObservableCollection<Service> GetServicesByResevationID(int reservationId)
        {
            return reservation.GetServicesByResevationID(reservationId);
        }
        public int GetNumberOfAvailableRoomType(DateTime sDate, DateTime eDate, int roomType)
        {
            return reservation.GetNumberOfAvailableRoomType(sDate, eDate, roomType);
        }
        public void AddReservation(Reservation reservation)
        {
            if (SelectedOffer != null)
                if (SelectedOffer.OfferID != 0)
                    reservation.OfferID = SelectedOffer.OfferID;
            this.reservation.AddReservation(reservation);
            ReservationList.Add(reservation);
            this.reservation.AddUserHistory(Username, reservation.ReservationID);
        }
        public ObservableCollection<Reservation> GetAllReservationsForUser(int userID)
        {
            return reservation.GetAllReservationsForUser(userID);
        }
        public int GetUserIDByName(string username)
        {
            return userBLL.GetUserIDByName(username);
        }
        public void UpdateStatus(Reservation r)
        {
            reservation.UpdateStatus(r);
        }
        public int GetOfferPriceFromReservation(int resID)
        {
            return reservation.GetOfferPriceFromReservation(resID);
        }
    }
}
