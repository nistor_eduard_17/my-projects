﻿using Hotel.Models.DataAccesLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Type = Hotel.Models.EntityLayer.Type;

namespace Hotel.Models.BusinessLogicLayer
{
    internal class TypeBLL
    {
        public ObservableCollection<Type> TypesList { get; set; }
        TypeDAL typeDAL = new TypeDAL();

        public TypeBLL()
        {
            TypesList = new ObservableCollection<Type>();
        }
        public ObservableCollection<Type> GetAllTypes()
        {
            return typeDAL.GetAllTypes();
        }
        public string GetRoomTypeName(int roomType)
        {
            return typeDAL.GetRoomTypeName(roomType);
        }
    }
}
