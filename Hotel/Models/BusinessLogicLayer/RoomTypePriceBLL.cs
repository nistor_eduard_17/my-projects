﻿using Hotel.Models.DataAccesLayer;
using Hotel.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Type = Hotel.Models.EntityLayer.Type;

namespace Hotel.Models.BusinessLogicLayer
{
    public class RoomTypePriceBLL
    {
        public ObservableCollection<RoomTypePrice> RTPriceList { get; set; }
        RoomTypePriceDAL rtPrice = new RoomTypePriceDAL();

        public RoomTypePriceBLL()
        {
            RTPriceList = new ObservableCollection<RoomTypePrice>();
        }
        public ObservableCollection<RoomTypePrice> GetAllPrices()
        {
            return rtPrice.GetAllPrices();
        }
        public void AddRoomTypePrice(RoomTypePrice rtp)
        {
            if (rtp != null)
                if (rtp.CurrentType != null && rtp.StartDate != new DateTime() && rtp.EndDate != new DateTime() && rtp.Price != 0)
                {
                    if (RTPriceList.FirstOrDefault(p => p.EndDate == rtp.EndDate && p.StartDate == rtp.StartDate && p.CurrentType.TypeName == rtp.CurrentType.TypeName && p.Price == rtp.Price) == null)
                    {
                        rtPrice.AddRoomTypePrice(rtp);
                        RTPriceList.Add(rtp);
                    }
                }
        }
        public Type GetRoomTypeByTypeID(int typeID)
        {
            return rtPrice.GetRoomTypeByTypeID(typeID);
        }
        public void ModifyRoomTypePrice(RoomTypePrice rtp)
        {
            RTPriceList = rtPrice.GetAllPrices();
            if (rtp.CurrentType != null && rtp.StartDate != new DateTime() && rtp.EndDate != new DateTime() && rtp.Price != 0)
            {
                if (RTPriceList.FirstOrDefault(p => p.EndDate == rtp.EndDate && p.StartDate == rtp.StartDate && p.CurrentType.TypeName == rtp.CurrentType.TypeName && p.Price == rtp.Price) == null)
                {
                    rtPrice.ModifyRoomTypePrice(rtp);
                    for (int index = 0; index < RTPriceList.Count; ++index)
                        if (RTPriceList[index].CurrentType.RoomType == rtp.CurrentType.RoomType)
                        {
                            RTPriceList[index] = rtp;
                            break;
                        }
                }
            }
        }
        public void DeleteRoomTypePrice(RoomTypePrice rtp)
        {
            RTPriceList.Remove(rtp);
            rtPrice.DeleteRoomPrice(rtp);
        }
        public int GetTodayRoomTypePrice(int roomType)
        {
            return rtPrice.GetTodayRoomTypePrice(roomType);
        }
        public DateTime GetLastAvailableDate()
        {
            return rtPrice.GetLastAvailableDate();
        }
        public ObservableCollection<RoomTypePrice> GetRoomTypePriceForPeriod(DateTime sDate, DateTime eDate, int roomType)
        {
            return rtPrice.GetRoomTypePriceForPeriod(sDate, eDate, roomType);
        }
    }
}
