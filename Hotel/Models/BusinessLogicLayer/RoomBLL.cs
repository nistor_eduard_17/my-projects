﻿using Hotel.Models.DataAccesLayer;
using Hotel.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Models.BusinessLogicLayer
{
    public class RoomBLL
    {
        public ObservableCollection<Room> RoomsList { get; set; }
        RoomDAL roomDAL = new RoomDAL();

        public RoomBLL()
        {
            RoomsList = new ObservableCollection<Room>();
        }
        public ObservableCollection<Room> GetAllRooms()
        {
            return roomDAL.GetAllRooms();
        }
        public ObservableCollection<Room> GetAllAvailableRooms(DateTime sDate, DateTime eDate)
        {
            return roomDAL.GetAllAvailableRooms(sDate, eDate);
        }
        public ObservableCollection<string> GetRoomImagesByRoomID(int roomID)
        {
            return roomDAL.GetRoomImagesByRoomID(roomID);
        }
        public ObservableCollection<Option> GetRoomOptionsByRoomID(int roomID)
        {
            return roomDAL.GetRoomOptionsByRoomID(roomID);
        }
        public void AddRoom(Room room)
        {
            roomDAL.AddRoom(room);
            RoomsList.Add(room);
        }
        public void AddRoomOption(Room room)
        {
            roomDAL.AddRoomOption(room);
        }
        public void AddRoomImage(Room room)
        {
            roomDAL.AddRoomImage(room);
        }
        public void ModifyRoom(Room room)
        {
            roomDAL.ModifyRoom(room);
        }
        public void DeleteRoom(Room room)
        {
            roomDAL.DeleteRoom(room);
        }
        public void DeleteImage(Room room, string image)
        {
            roomDAL.DeleteImage(room, image);
        }

    }
}
