﻿using Hotel.Models.DataAccesLayer;
using Hotel.Models.EntityLayer;
using System.Collections.ObjectModel;
using System.Linq;

namespace Hotel.Models.BusinessLogicLayer
{
    public class UserBLL
    {
        public ObservableCollection<User> UsersList { get; set; }
        UserDAL userDAL = new UserDAL();

        public UserBLL()
        {
            UsersList = new ObservableCollection<User>();
        }
        public ObservableCollection<User> GetAllUsers()
        {
            return userDAL.GetAllUsers();
        }
        public void AddUser(User user)
        {
            userDAL.AddUser(user);
            UsersList.Add(user);
        }
        public int GetUser(User user)
        {
            return userDAL.GetUser(user);
        }
        public string GetUserType(int userID)
        {
            if (userID != -1)
                return UsersList.FirstOrDefault(p => p.UserID == userID).UserType;
            return "";
        }
        public int GetUserIDByName(string username)
        {
            return userDAL.GetUserIDByName(username);
        }

    }
}

