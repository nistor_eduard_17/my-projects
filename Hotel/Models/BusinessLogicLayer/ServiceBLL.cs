﻿using Hotel.Models.DataAccesLayer;
using Hotel.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Models.BusinessLogicLayer
{
    public class ServiceBLL
    {
        public ObservableCollection<Service> ServicesList { get; set; }
        ServiceDAL serviceDAL = new ServiceDAL();

        public ServiceBLL()
        {
            ServicesList = new ObservableCollection<Service>();
        }
        public ObservableCollection<Service> GetAllServices()
        {
            return serviceDAL.GetAllServices();
        }
        public void AddService(Service service)
        {
            if (service != null)
                if (ServicesList.FirstOrDefault(p => p.ServiceType.ToLower() == service.ServiceType.ToLower()) == null)
                    if (service.ServiceType != "")
                    {
                        serviceDAL.AddSerivce(service);
                        ServicesList.Add(service);
                    }

        }
        public void DeleteService(Service service)
        {
            serviceDAL.DeleteService(service);
            ServicesList.Remove(service);
        }
        public void ModifyService(Service service)
        {
            ServicesList = serviceDAL.GetAllServices();
            if (ServicesList.FirstOrDefault(p => p.ServiceType.ToLower() == service.ServiceType.ToLower() && p.Price == service.Price) == null)
                if (service.ServiceType != "")
                {
                    serviceDAL.ModifyService(service);
                    for (int index = 0; index < ServicesList.Count; index++)
                        if (ServicesList[index].ServiceID == service.ServiceID)
                        {
                            ServicesList[index] = service;
                            break;
                        }
                }
        }
        public Service GetService(int serviceID)
        {
            return serviceDAL.GetService(serviceID);
        }
    }
}
