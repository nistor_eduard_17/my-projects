﻿using Hotel.Models.DataAccesLayer;
using Hotel.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Models.BusinessLogicLayer
{
    public class OptionBLL
    {
        public ObservableCollection<Option> OptionsList { get; set; }

        OptionDAL optionDAL = new OptionDAL();

        public OptionBLL()
        {
            OptionsList = new ObservableCollection<Option>();
        }
        public ObservableCollection<Option> GetAllOptions()
        {
            return optionDAL.GetAllOptions();
        }
        public void AddOption(Option option)
        {
            if (OptionsList.FirstOrDefault(p => p.OptionType.ToLower() == option.OptionType.ToLower()) == null)
                if (option.OptionType != "")
                {
                    optionDAL.AddOption(option);
                    OptionsList.Add(option);
                }
        }
        public void ModifyOption(Option option)
        {
            OptionsList = optionDAL.GetAllOptions();
            if (OptionsList.FirstOrDefault(p => p.OptionType.ToLower() == option.OptionType.ToLower()) == null)
                if (option.OptionType != "")
                {
                    optionDAL.ModifyOption(option);
                    for (int index = 0; index < OptionsList.Count; index++)
                        if (OptionsList[index].OptionID == option.OptionID)
                        {
                            OptionsList[index] = option;
                            break;
                        }
                }
        }
        public void DeleteOption(Option option)
        {
            optionDAL.DeleteOption(option);
            OptionsList.Remove(option);
        }
        public Option GetOption(int optionID)
        {
            return optionDAL.GetOption(optionID);
        }
    }
}
