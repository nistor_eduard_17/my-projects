﻿using Hotel.Models.DataAccesLayer;
using Hotel.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Models.BusinessLogicLayer
{
    public class OfferBLL
    {
        public ObservableCollection<Offer> OffersList { get; set; }
        public ObservableCollection<Service> InterfaceServices { get; set; }

        OfferDAL offerDAL = new OfferDAL();

        public OfferBLL()
        {
            OffersList = new ObservableCollection<Offer>();
            InterfaceServices = new ObservableCollection<Service>();
        }
        public ObservableCollection<Offer> GetAllOffers()
        {
            return offerDAL.GetAllOffers();
        }
        public void AddOffer(Offer o)
        {
            OffersList = offerDAL.GetAllOffers();
            if (o != null)
                if (o.OfferName != "" && o.RoomType.TypeName != "" && o.StartDate != new DateTime() && o.EndDate != new DateTime() && o.Price != 0 && o.Services != null)
                {
                    if (OffersList.FirstOrDefault(p => p.OfferName == o.OfferName && p.RoomType.RoomType == o.RoomType.RoomType && p.StartDate == o.StartDate &&
                     p.EndDate == o.EndDate && p.Price == o.Price && Compare2Obs(p.Services, o.Services) == true) == null)
                    {
                        offerDAL.AddOffer(o);
                        OffersList.Add(o);
                    }
                }
        }
        private bool Compare2Obs(ObservableCollection<Service> obj1, ObservableCollection<Service> obj2)
        {
            if (obj1.Count != obj2.Count)
                return false;
            else
            {
                for (int index = 0; index < obj1.Count; ++index)
                    if (obj1[index].ServiceID != obj2[index].ServiceID)
                        return false;
            }
            return true;
        }
        public void ModifyOffer(Offer o)
        {
            OffersList = offerDAL.GetAllOffers();
            if (o.OfferName != "" && o.RoomType.TypeName != "" && o.StartDate != new DateTime() && o.EndDate != new DateTime() && o.Price != 0 && o.Services != null)
            {
                if (OffersList.FirstOrDefault(p => p.OfferName == o.OfferName && p.RoomType.RoomType == o.RoomType.RoomType && p.StartDate == o.StartDate &&
                 p.EndDate == o.EndDate && p.Price == o.Price && Compare2Obs(p.Services, o.Services) == true) == null)
                {
                    offerDAL.ModifyOffer(o);
                    for (int index = 0; index < OffersList.Count; index++)
                        if (OffersList[index].OfferID == o.OfferID)
                        {
                            OffersList[index] = o;
                            break;
                        }
                }
            }
        }
        public void RemoveServiceFromInterface(Service s)
        {
            InterfaceServices.Remove(s);
        }
        public void DeleteOffer(Offer offer)
        {
            offerDAL.DeleteOffer(offer);
            OffersList.Remove(offer);
        }
        public ObservableCollection<Service> GetOfferServices(int offerID)
        {
            return offerDAL.GetOfferServices(offerID);
        }
    }
}
