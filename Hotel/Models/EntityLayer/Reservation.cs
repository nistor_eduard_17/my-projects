﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Models.EntityLayer
{
    public class Reservation : BasePropertyChanged
    {
        private int reservationID;
        private int offerID;
        private DateTime startDate;
        private DateTime endDate;
        private string status;
        private ObservableCollection<ReservationRoomType> roomTypes;
        private ObservableCollection<string> allStatuses;
        public ObservableCollection<string> AllStatuses { get => allStatuses; set { allStatuses = value; NotifyPropertyChanged("AllStatuses"); } }

        private ObservableCollection<Service> services;
        private int totalPrice;
        private int numberOfRooms;

        public int ReservationID { get => reservationID; set { reservationID = value; NotifyPropertyChanged("ReservationID"); } }
        public int OfferID { get => offerID; set { offerID = value; NotifyPropertyChanged("OfferID"); } }
        public DateTime StartDate { get => startDate; set { startDate = value; NotifyPropertyChanged("StartDate"); } }
        public DateTime EndDate { get => endDate; set { endDate = value; NotifyPropertyChanged("EndDate"); } }
        public string Status { get => status; set { status = value; NotifyPropertyChanged("Status"); } }
        public ObservableCollection<ReservationRoomType> RoomTypes { get => roomTypes; set { roomTypes = value; NotifyPropertyChanged("RoomTypes"); } }
        public ObservableCollection<Service> Services { get => services; set { services = value; NotifyPropertyChanged("Services"); } }
        public int TotalPrice { get => totalPrice; set { totalPrice = value;NotifyPropertyChanged("TotalPrice"); } }
        public int NumberOfRooms { get => numberOfRooms; set { numberOfRooms = value;NotifyPropertyChanged("NumberOfRooms"); } }

    }
}
