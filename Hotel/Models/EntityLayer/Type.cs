﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Models.EntityLayer
{
    public class Type : BasePropertyChanged
    {
        private int roomType { get; set; }
        private string typeName { get; set; }
        private bool deleted { get; set; }
        public int RoomType
        {
            get => roomType;
            set
            {
                roomType = value;
                NotifyPropertyChanged("RoomType");
            }
        }
        public string TypeName
        {
            get => typeName;
            set
            {
                typeName = value;
                NotifyPropertyChanged("Type");
            }
        }
        public bool Deleted
        {
            get => deleted;
            set
            {
                deleted = value;
                NotifyPropertyChanged("Deleted");
            }
        }
    }
}
