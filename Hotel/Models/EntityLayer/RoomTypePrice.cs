﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Models.EntityLayer
{
    public class RoomTypePrice:BasePropertyChanged
    {
        private int id;
        private int price;
        private Type currentType;
        private bool deleted;
        private DateTime startDate;
        private DateTime endDate;

        public int ID
        {
            get => id;
            set
            {
                id = value;
                NotifyPropertyChanged("ID");
            }
        }
        public int Price
        {
            get => price;
            set
            {
                price = value;
                NotifyPropertyChanged("Price");
            }
        }
        public Type CurrentType
        {
            get => currentType;
            set
            {
                currentType = value;
                NotifyPropertyChanged("CurrentType");
            }
        }
        public bool Deleted
        {
            get => deleted;
            set
            {
                deleted = value;
                NotifyPropertyChanged("Deleted");
            }
        }
        public DateTime StartDate
        {
            get => startDate;
            set
            {
               startDate = value;
                NotifyPropertyChanged("StartDate");
            }
        }
        public DateTime EndDate
        {
            get => endDate;
            set
            {
                endDate = value;
                NotifyPropertyChanged("EndDate");
            }
        }
    }
}
