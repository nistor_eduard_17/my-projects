﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Models.EntityLayer
{
    public class Offer : BasePropertyChanged
    {
        private int offerID;
        private string offerName;
        private Type roomType;
        private DateTime startDate;
        private DateTime endDate;
        private bool deleted;
        private int price;
        private ObservableCollection<Service> services;
        public int OfferID
        {
            get => offerID;
            set
            {
                offerID = value;
                NotifyPropertyChanged("OfferID");
            }
        }
        public Type RoomType
        {
            get => roomType;
            set
            {
                roomType = value;
                NotifyPropertyChanged("RoomType");
            }
        }
        public int Price
        {
            get => price;
            set
            {
                price = value;
                NotifyPropertyChanged("Price");
            }
        }
        public DateTime StartDate
        {
            get => startDate;
            set
            {
                startDate = value;
                NotifyPropertyChanged("StartDate");
            }
        }
        public DateTime EndDate
        {
            get => endDate;
            set
            {
                endDate = value;
                NotifyPropertyChanged("EndDate");
            }
        }
        public bool Deleted
        {
            get => deleted;
            set
            {
                deleted = value;
                NotifyPropertyChanged("Deleted");
            }
        }
        public ObservableCollection<Service>Services
        {
            get => services;
            set
            {
                services = value;
                NotifyPropertyChanged("Services");
            }
        }
        public string OfferName { get => offerName;
        set
            {
                offerName = value;
                NotifyPropertyChanged("OfferName");
            }
        }
    }
}
