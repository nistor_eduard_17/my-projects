﻿namespace Hotel.Models.EntityLayer
{
    public class Service : BasePropertyChanged
    {
        private int serviceID;
        private bool deleted;
        private string serviceType;
        private int price;

        public int ServiceID
        {
            get => serviceID;
            set
            {
                serviceID = value;
                NotifyPropertyChanged("ServiceID");
            }
        }
        public string ServiceType
        {
            get => serviceType;
            set
            {
                serviceType = value;
                NotifyPropertyChanged("ServiceType");
            }
        }
        public int Price
        {
            get => price;
            set
            {
                price = value;
                NotifyPropertyChanged("Price");
            }
        }
        public bool Deleted
        {
            get => deleted;
            set
            {
                deleted = value;
                NotifyPropertyChanged("Deleted");
            }
        }
    }
}
