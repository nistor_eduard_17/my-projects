﻿namespace Hotel.Models.EntityLayer
{
    public class Option : BasePropertyChanged
    {
        private int optionID;
        private bool deleted;
        private string optionType;

        public int OptionID
        {
            get => optionID;
            set
            {
                optionID = value;
                NotifyPropertyChanged("OptionID");
            }
        }
        public string OptionType
        {
            get => optionType;
            set
            {
                optionType = value;
                NotifyPropertyChanged("OptionType");
            }
        }
        public bool Deleted
        {
            get => deleted;
            set
            {
                deleted = value;
                NotifyPropertyChanged("Deleted");
            }
        }
    }
}
