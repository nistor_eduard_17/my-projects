﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Models.EntityLayer
{
    public class Room : BasePropertyChanged
    {
        private int roomID;
        private bool deleted;
        private int roomType;
        private string roomTypeName;
        private int todayPrie;
        public int RoomID
        {
            get => roomID;
            set
            {
                roomID = value;
                NotifyPropertyChanged("RoomID");
            }
        }
        public int RoomType
        {
            get => roomType;
            set
            {
                roomType = value;
                NotifyPropertyChanged("RoomType");
            }
        }
        public string RoomTypeName
        {
            get => roomTypeName;
            set
            {
                roomTypeName = value;
                NotifyPropertyChanged("RoomTypeName");
            }
        }

        public bool Deleted
        {
            get => deleted;
            set
            {
                deleted = value;
                NotifyPropertyChanged("Deleted");
            }
        }
        public int TodayPrice
        {
            get => todayPrie;
            set
            {
                todayPrie = value;
                NotifyPropertyChanged("TodayPrice");
            }
        }
        public Room()
        {
            Images=new ObservableCollection<string>();
            Options = new ObservableCollection<Option>();
        }
        public ObservableCollection<string> Images { get; set; }
        public ObservableCollection<Option> Options { get; set; }
    }
}

