﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Models.EntityLayer
{
    public class ReservationRoomType:BasePropertyChanged
    {
        private Type roomType;
        private int number;
        private bool deleted;
        public Type RoomType
        {
            get => roomType;
            set
            {
                roomType = value;
                NotifyPropertyChanged("RoomType");
            }
        }
        public int Number
        {
            get => number;
            set
            {
                number = value;
                NotifyPropertyChanged("Number");
            }
        }
        public bool Deleted
        {
            get => deleted;
            set
            {
                deleted = value;
                NotifyPropertyChanged("Deleted");
            }
        }
    }
}
