﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Models.EntityLayer
{
    public class Bill:BasePropertyChanged
    {
        private string name;
        private int numberOfNights;
        private int quantity;
        private int totalPrice;
        public string Name
        {
            get => name;
            set
            { name=value;
                NotifyPropertyChanged("Name");
            }
        }
        public int Quantity
        {
            get => quantity;
            set
            {
                quantity=value;
                NotifyPropertyChanged("Quantity");
            }
        }
        public int NumberOfNights
        {
            get => numberOfNights;
            set
            {
                numberOfNights = value;
                NotifyPropertyChanged("NumberOfNights");
            }
        }
        public int TotalPrice { get => totalPrice; set { totalPrice = value; NotifyPropertyChanged("TotalPrice"); } }

    }
}
