﻿using Hotel.Models.BusinessLogicLayer;
using Hotel.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Models.DataAccesLayer
{
    public class ReservationDAL
    {
        RoomBLL roomBLL = new RoomBLL();
        public ObservableCollection<Reservation> GetAll(string procedure_name)
        {
            using (SqlConnection con = DALHelper.Connection)
            {
                ObservableCollection<Reservation> result = new ObservableCollection<Reservation>();
                SqlCommand cmd = new SqlCommand(procedure_name, con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Reservation reservation = new Reservation();
                    reservation.ReservationID = (int)reader[0];
                    reservation.RoomTypes = GetReservationRoomTypesByReservationID(reservation.ReservationID);
                    reservation.Services = GetServicesByResevationID(reservation.ReservationID);
                    reservation.OfferID = reader[1] == DBNull.Value ? 0 : (int)reader[1];
                    reservation.StartDate = (DateTime)reader[2];
                    reservation.EndDate = (DateTime)reader[3];
                    reservation.Status = reader[4].ToString();
                    result.Add(reservation);
                }
                return result;
            }
        }
        public ObservableCollection<Reservation> GetAllReservations()
        {
            return GetAll("GetAllReservations");
        }
        public ObservableCollection<Reservation> GetAllReservationsForUser(int userID)
        {
            using (SqlConnection con = DALHelper.Connection)
            {
                ObservableCollection<Reservation> result = new ObservableCollection<Reservation>();
                SqlCommand cmd = new SqlCommand("GetAllReservationsForUser", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter userParam = new SqlParameter("@accountID", userID);
                cmd.Parameters.Add(userParam);
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Reservation reservation = new Reservation();
                    reservation.ReservationID = (int)reader[0];
                    reservation.RoomTypes = GetReservationRoomTypesByReservationID(reservation.ReservationID);
                    reservation.Services = GetServicesByResevationID(reservation.ReservationID);
                    reservation.OfferID = reader[1] == DBNull.Value ? 0 : (int)reader[1];
                    reservation.StartDate = (DateTime)reader[2];
                    reservation.EndDate = (DateTime)reader[3];
                    reservation.Status = reader[4].ToString();
                    result.Add(reservation);
                }
                return result;
            }
        }
        public ObservableCollection<ReservationRoomType> GetReservationRoomTypesByReservationID(int reservationID)
        {
            using (SqlConnection con = DALHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("GetReservationRoomTypesByReservationID", con);
                cmd.CommandType = CommandType.StoredProcedure;
                ObservableCollection<ReservationRoomType> result = new ObservableCollection<ReservationRoomType>();
                SqlParameter paramReservationID = new SqlParameter("@reservationID", reservationID);
                cmd.Parameters.Add(paramReservationID);
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ReservationRoomType rrt = new ReservationRoomType();
                    TypeBLL typeBLL = new TypeBLL();
                    rrt.RoomType = new EntityLayer.Type() { RoomType = (int)reader[0], TypeName = typeBLL.GetRoomTypeName((int)reader[0]) };
                    rrt.Number = (int)reader[1];
                    rrt.Deleted = Convert.ToBoolean(reader[2]);
                    result.Add(rrt);
                }
                return result;
            }
        }
        public ObservableCollection<Service> GetServicesByResevationID(int reservationId)
        {
            using (SqlConnection con = DALHelper.Connection)
            {
                ObservableCollection<Service> result = new ObservableCollection<Service>();
                SqlCommand cmd = new SqlCommand("GetServicesByResevationID", con);
                SqlParameter paramReservation = new SqlParameter("ReservationID", reservationId);
                cmd.Parameters.Add(paramReservation);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Service s = new Service();
                    ServiceBLL serviceBLL = new ServiceBLL();
                    s = serviceBLL.GetService((int)reader[0]);
                    s.Deleted = Convert.ToBoolean(reader[1]);
                    if (s.Deleted == false)
                        result.Add(s);
                }
                return result;
            }
        }
        public int GetNumberOfAvailableRoomType(DateTime sDate, DateTime eDate, int roomType)
        {
            using (SqlConnection con = DALHelper.Connection)
            {
                int value = 0;
                SqlCommand cmd = new SqlCommand("GetNumberOfAvailableRoomType", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramStartDate = new SqlParameter("@startDate", sDate);
                SqlParameter paramEndDate = new SqlParameter("@endDate", eDate);
                SqlParameter paramRoomType = new SqlParameter("@roomType", roomType);
                cmd.Parameters.Add(paramStartDate);
                cmd.Parameters.Add(paramEndDate);
                cmd.Parameters.Add(paramRoomType);
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    value = (int)reader[0];
                }
                return value;
            }
        }

        public void AddReservation(Reservation reservation)
        {
            using (SqlConnection con = DALHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("AddReservation", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter offerID;
                if (reservation.OfferID == 0)
                    offerID = new SqlParameter("@offerID", DBNull.Value);
                else
                    offerID = new SqlParameter("@offerID", reservation.OfferID);
                SqlParameter startDate = new SqlParameter("@startDate", reservation.StartDate);
                SqlParameter endDate = new SqlParameter("@endDate", reservation.EndDate);
                SqlParameter status = new SqlParameter("@status", reservation.Status);
                SqlParameter reservationID = new SqlParameter("@reservationID", SqlDbType.Int);
                reservationID.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(offerID);
                cmd.Parameters.Add(startDate);
                cmd.Parameters.Add(endDate);
                cmd.Parameters.Add(status);
                cmd.Parameters.Add(reservationID);
                con.Open();
                cmd.ExecuteNonQuery();
                reservation.ReservationID = (int)reservationID.Value;
            }

            ObservableCollection<Room> availableRooms = roomBLL.GetAllAvailableRooms(reservation.StartDate, reservation.EndDate);
            foreach (var elem in reservation.RoomTypes)
            {
                DALHelper.CRUDHelper("AddReservationRoomType", new Tuple<string, int>("@reservationID", reservation.ReservationID),
                    new Tuple<string, int>("@roomType", elem.RoomType.RoomType), new Tuple<string, int>("@number", elem.Number));
                var filteredAvaialableRooms = availableRooms.Where(p => p.RoomType == elem.RoomType.RoomType).ToList();
                for (int index = 0; index < elem.Number; ++index)
                    DALHelper.CRUDHelper("AddReservationRoom", new Tuple<string, int>("@reservationID", reservation.ReservationID),
                        new Tuple<string, int>("@roomID", filteredAvaialableRooms[index].RoomID));
            }
            foreach (Service s in reservation.Services)
                DALHelper.CRUDHelper("AddReservationService", new Tuple<string, int>("@reservationID", reservation.ReservationID),
                    new Tuple<string, int>("@serviceID", s.ServiceID));
        }
        public void AddUserHistory(string username, int reservationID)
        {
            using (SqlConnection con = DALHelper.Connection)
            {
                UserBLL userBLL = new UserBLL();
                int value = userBLL.GetUserIDByName(username);
                DALHelper.CRUDHelper("AddUserHistory", new Tuple<string, int>("@accountID", value), new Tuple<string, int>("@reservationID", reservationID));
            }
        }
        public void UpdateStatus(Reservation reservation)
        {
            using (SqlConnection con = DALHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("UpdateReservationStatus", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter reservationID = new SqlParameter("@reservationID", reservation.ReservationID);
                SqlParameter status = new SqlParameter("@status", reservation.Status);
                cmd.Parameters.Add(reservationID);
                cmd.Parameters.Add(status);
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }
        public int GetOfferPriceFromReservation(int reservationID)
        {
            using (SqlConnection con = DALHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("GetOfferPriceFromReservation", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter resID = new SqlParameter("@reservationID", reservationID);
                cmd.Parameters.Add(resID);
                con.Open();
                return (int)cmd.ExecuteScalar();
            }
        }
    }
}
