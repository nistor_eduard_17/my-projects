﻿using Hotel.Models.BusinessLogicLayer;
using Hotel.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Type = Hotel.Models.EntityLayer.Type;

namespace Hotel.Models.DataAccesLayer
{
    public class OfferDAL
    {
        public ObservableCollection<Offer> GetAllOffers()
        {
            using (SqlConnection con = DALHelper.Connection)

            {
                SqlCommand cmd = new SqlCommand("GetAllOffers", con);
                ObservableCollection<Offer> result = new ObservableCollection<Offer>();
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Offer o = new Offer();
                    o.OfferID = (int)reader[0];
                    o.RoomType = GetRoomTypeByTypeID((int)reader[1]);
                    o.Price = (int)reader[2];
                    o.StartDate = (DateTime)reader[3];
                    o.EndDate = (DateTime)reader[4];
                    o.OfferName = reader[5].ToString();
                    o.Deleted = Convert.ToBoolean(reader[6]);
                    o.Services = GetOfferServices(o.OfferID);
                    if (o.Deleted == false)
                        result.Add(o);
                }
                reader.Close();
                return result;
            }
        }
        public Type GetRoomTypeByTypeID(int typeID)
        {
            using (SqlConnection con = DALHelper.Connection)
            {
                Type t = new Type();
                SqlCommand cmd = new SqlCommand("GetRoomTypeByTypeID", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramRoomType = new SqlParameter("@RoomType", typeID);
                cmd.Parameters.Add(paramRoomType);
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    t.RoomType = (int)reader[0];
                    t.TypeName = reader[1].ToString();
                    t.Deleted = Convert.ToBoolean(reader[2]);

                }
                return t;
            }
        }
        public ObservableCollection<Service> GetOfferServices(int offerID)
        {
            using (SqlConnection con = DALHelper.Connection)
            {
                ObservableCollection<Service> result = new ObservableCollection<Service>();
                SqlCommand cmd = new SqlCommand("GetOfferServices", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramRoomType = new SqlParameter("@OfferID", offerID);
                cmd.Parameters.Add(paramRoomType);
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Service service = new Service();
                    ServiceBLL serviceBLL = new ServiceBLL();
                    service = serviceBLL.GetService((int)reader[0]);
                    if (Convert.ToBoolean(reader[1]) == false)
                        result.Add(service);

                }
                return result;
            }
        }        
        public void AddOffer(Offer offer)
        {
            using (SqlConnection con = DALHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("AddOffer", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sDate = new SqlParameter("@StartDate", offer.StartDate);
                SqlParameter eDate = new SqlParameter("@EndDate", offer.EndDate);
                SqlParameter price = new SqlParameter("@Price", offer.Price);
                SqlParameter roomType = new SqlParameter("@RoomType", offer.RoomType.RoomType);
                SqlParameter offerName = new SqlParameter("@name", offer.OfferName);
                SqlParameter paramIdOffer = new SqlParameter("@OfferID", SqlDbType.Int);
                paramIdOffer.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(sDate);
                cmd.Parameters.Add(eDate);
                cmd.Parameters.Add(price);
                cmd.Parameters.Add(roomType);
                cmd.Parameters.Add(offerName);
                cmd.Parameters.Add(paramIdOffer);
                con.Open();
                cmd.ExecuteNonQuery();
                offer.OfferID = (int)paramIdOffer.Value;
            }
            foreach (Service s in offer.Services)
                DALHelper.CRUDHelper("AddOfferService", new Tuple<string, int>("@OfferID", offer.OfferID), new Tuple<string, int>("@ServiceID", s.ServiceID));
        }
        public void ModifyOffer(Offer offer)
        {
            ObservableCollection<Service> previousServices = GetOfferServices(offer.OfferID);
            DALHelper.CRUDHelperForOffer("UpdateOffer", new Tuple<string, string>("@name", offer.OfferName),
                new Tuple<string, DateTime>("@startDate", offer.StartDate), new Tuple<string, DateTime>("@endDate", offer.EndDate),
              new Tuple<string, int>("@price", offer.Price), new Tuple<string, int>("@roomType", offer.RoomType.RoomType), new Tuple<string, int>("@offerID", offer.OfferID));
            if (offer.Services.Count >= previousServices.Count)
                foreach (Service s in offer.Services)
                {
                    DALHelper.CRUDHelper("UpdateOfferService", new Tuple<string, int>("@OfferID", offer.OfferID),
                        new Tuple<string, int>("@ServiceID", s.ServiceID), new Tuple<string, int>("@Deleted", 0));
                }
            else
            {
                foreach (Service s in previousServices)
                {
                    if (offer.Services.FirstOrDefault(p => p.ServiceID == s.ServiceID) != null)
                    {
                        DALHelper.CRUDHelper("UpdateOfferService", new Tuple<string, int>("@OfferID", offer.OfferID),
                                               new Tuple<string, int>("@ServiceID", s.ServiceID), new Tuple<string, int>("@Deleted", 0));
                    }
                    else
                    {
                        DALHelper.CRUDHelper("UpdateOfferService", new Tuple<string, int>("@OfferID", offer.OfferID),
                                               new Tuple<string, int>("@ServiceID", s.ServiceID), new Tuple<string, int>("@Deleted", 1));
                    }
                }
            }
        }
        public void DeleteOffer(Offer offer)
        {
            DALHelper.CRUDHelper("DeleteOffer", new Tuple<string, int>("@OfferID", offer.OfferID));
        }
    }
}
;