﻿using Hotel.Models.BusinessLogicLayer;
using Hotel.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Hotel.Models.DataAccesLayer
{
    public class RoomDAL
    {
        public ObservableCollection<Room> GetAllRooms()
        {
            SqlConnection con = DALHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllRooms", con);
                ObservableCollection<Room> result = new ObservableCollection<Room>();
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                RoomTypePriceBLL price = new RoomTypePriceBLL();
                TypeBLL typeBLL = new TypeBLL();
                while (reader.Read())
                {
                    Room r = new Room();
                    r.RoomID = (int)(reader[0]);//reader.GetInt(0);
                    r.RoomType = (int)(reader[1]);//reader[1].ToString()
                    r.RoomTypeName = typeBLL.GetRoomTypeName(r.RoomType);
                    r.Deleted = (bool)(reader[2]);
                    r.TodayPrice = price.GetTodayRoomTypePrice(r.RoomType);
                    r.Options = GetRoomOptionsByRoomID(r.RoomID);
                    r.Images = GetRoomImagesByRoomID(r.RoomID);
                    if (r.Deleted == false)
                        result.Add(r);
                }
                reader.Close();
                return result;
            }
            finally
            {
                con.Close();
            }
        }
        public ObservableCollection<Room> GetAllAvailableRooms(DateTime startDate, DateTime endDate)
        {
            SqlConnection con = DALHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllAvailableRooms", con);
                ObservableCollection<Room> result = new ObservableCollection<Room>();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sDate = new SqlParameter("@startDate", startDate);
                SqlParameter eDate = new SqlParameter("@endDate", endDate);
                cmd.Parameters.Add(sDate);
                cmd.Parameters.Add(eDate);
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                RoomTypePriceBLL price = new RoomTypePriceBLL();
                TypeBLL typeBLL = new TypeBLL();
                while (reader.Read())
                {
                    Room r = new Room();
                    r.RoomID = (int)(reader[0]);//reader.GetInt(0);
                    r.RoomType = (int)(reader[1]);//reader[1].ToString()
                    r.RoomTypeName = typeBLL.GetRoomTypeName(r.RoomType);
                    r.Deleted = (bool)(reader[2]);
                    r.TodayPrice = price.GetTodayRoomTypePrice(r.RoomType);
                    r.Options = GetRoomOptionsByRoomID(r.RoomID);
                    r.Images = GetRoomImagesByRoomID(r.RoomID);
                    if (r.Deleted == false)
                        result.Add(r);
                }
                reader.Close();
                return result;
            }
            finally
            {
                con.Close();
            }
        }
        public ObservableCollection<string> GetRoomImagesByRoomID(int roomID)
        {
            SqlConnection con = DALHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("GetRoomImagesByRoomID", con);
                ObservableCollection<string> result = new ObservableCollection<string>();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramRoomID = new SqlParameter("@RoomID", roomID);
                cmd.Parameters.Add(paramRoomID);
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    if (Convert.ToByte(reader[1]) == 0)
                        result.Add(reader[0].ToString());
                }
                reader.Close();
                return result;
            }
            finally
            {
                con.Close();
            }
        }
        public ObservableCollection<Option> GetRoomOptionsByRoomID(int roomID)
        {
            SqlConnection con = DALHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("GetRoomOptionsByRoomID", con);
                ObservableCollection<Option> result = new ObservableCollection<Option>();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramRoomID = new SqlParameter("@RoomID", roomID);
                cmd.Parameters.Add(paramRoomID);
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    OptionBLL optionBLL = new OptionBLL();
                    Option o = optionBLL.GetOption((int)(reader[0]));
                    o.Deleted = Convert.ToBoolean(reader[1]);
                    if (o.Deleted == false)
                        result.Add(o);
                }
                reader.Close();
                return result;
            }
            finally
            {
                con.Close();
            }
        }
        public void AddRoomOption(Room room)
        {

            foreach (Option o in room.Options)
            {
                DALHelper.CRUDHelper("AddRoomOption", new Tuple<string, int>("@RoomID", room.RoomID), new Tuple<string, int>("@OptionID", o.OptionID));
            }
        }
        public void AddRoomImage(Room room)
        {
            foreach (string image in room.Images)
            {

                DALHelper.CRUDHelperForImages("AddRoomImage", new Tuple<string, string>("@ImageID", image), new Tuple<string, int>("@RoomID", room.RoomID));
            }
        }
        public void CreateUpdateRoom(Room room, string operation_name, string procedure_name, params Tuple<string, int>[] procedure_params)
        {
            SqlConnection con = DALHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand(procedure_name, con);
                cmd.CommandType = CommandType.StoredProcedure;
                foreach (var elem in procedure_params)
                {
                    SqlParameter param = new SqlParameter(elem.Item1, elem.Item2);
                    cmd.Parameters.Add(param);
                }
                if (operation_name == "Create")
                {
                    SqlParameter paramIdRoom = new SqlParameter("@RoomID", SqlDbType.Int);
                    paramIdRoom.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(paramIdRoom);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    room.RoomID = (int)paramIdRoom.Value;
                    AddRoomOption(room);
                    AddRoomImage(room);
                    return;
                }
                con.Open();
                cmd.ExecuteNonQuery();
                //UpdateRoomOption(room);
                //UpdateRoomImage(room);

            }
            finally
            {
                con.Close();
            }
        }
        public void AddRoom(Room room)
        {
            CreateUpdateRoom(room, "Create", "AddRoom", new Tuple<string, int>("@RoomType", room.RoomType));
        }
        public void DeleteRoom(Room room)
        {
            DALHelper.CRUDHelper("DeleteRoom", new Tuple<string, int>("@RoomID", room.RoomID));
        }
        public void DeleteImage(Room room, string image)
        {
            DALHelper.CRUDHelperForImages("DeleteImage", new Tuple<string, string>("@ImageID", image), new Tuple<string, int>("@RoomID", room.RoomID));

        }
        public void ModifyRoom(Room room)
        {
            ObservableCollection<Room> rooms = GetAllRooms();
            Room previousRoom = rooms.FirstOrDefault(p => p.RoomID == room.RoomID);
            DALHelper.CRUDHelper("UpdateRoom", new Tuple<string, int>("@RoomID", room.RoomID), new Tuple<string, int>("@RoomType", room.RoomType));
            if (room.Images.Count >= previousRoom.Images.Count)
                foreach (string i in room.Images)
                {
                    DALHelper.CRUDHelperForImages("UpdateRoomImage", new Tuple<string, string>("@ImageID", i), new Tuple<string, int>("@RoomID", room.RoomID), new Tuple<string, int>("@Deleted", 0));
                }
            else
            {
                foreach (string i in previousRoom.Images)
                {
                    if (room.Images.Contains(i))
                    {
                        DALHelper.CRUDHelperForImages("UpdateRoomImage", new Tuple<string, string>("@ImageID", i), new Tuple<string, int>("@RoomID", room.RoomID), new Tuple<string, int>("@Deleted", 0));
                    }
                    else
                    {
                        DALHelper.CRUDHelperForImages("UpdateRoomImage", new Tuple<string, string>("@ImageID", i), new Tuple<string, int>("@RoomID", room.RoomID), new Tuple<string, int>("@Deleted", 1));
                    }
                }
            }

            if (room.Options.Count >= previousRoom.Options.Count)
                foreach (Option o in room.Options)
                    DALHelper.CRUDHelper("UpdateRoomOption", new Tuple<string, int>("@RoomID", room.RoomID), new Tuple<string, int>("@OptionID", o.OptionID), new Tuple<string, int>("@Deleted", 0));
            else
            {
                foreach (Option o in previousRoom.Options)
                {
                    if (room.Options.FirstOrDefault(p => p.OptionID == o.OptionID) != null)
                        DALHelper.CRUDHelper("UpdateRoomOption", new Tuple<string, int>("@RoomID", room.RoomID), new Tuple<string, int>("@OptionID", o.OptionID), new Tuple<string, int>("@Deleted", 0));

                    else
                        DALHelper.CRUDHelper("UpdateRoomOption", new Tuple<string, int>("@RoomID", room.RoomID), new Tuple<string, int>("@OptionID", o.OptionID), new Tuple<string, int>("@Deleted", 1));

                }
            }
        }

    }
}
