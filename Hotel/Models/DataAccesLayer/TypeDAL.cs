﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Type = Hotel.Models.EntityLayer.Type;

namespace Hotel.Models.DataAccesLayer
{
    internal class TypeDAL
    {
        public ObservableCollection<Type> GetAllTypes()
        {
            SqlConnection con = DALHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllTypes", con);
                ObservableCollection<Type> result = new ObservableCollection<Type>();
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Type t = new Type();
                    t.RoomType = (int)(reader[0]);//reader.GetInt(0);
                    t.TypeName = reader[1].ToString();//reader[1].ToString();
                    result.Add(t);
                }
                reader.Close();
                return result;
            }
            finally
            {
                con.Close();
            }
        }
        public string GetRoomTypeName(int roomType)
        {
            string name = "";
            using (SqlConnection con = DALHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("GetRoomTypeName", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramRoomType = new SqlParameter("@roomType", roomType);
                cmd.Parameters.Add(paramRoomType);
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                    name = reader[0].ToString();
            }
            return name;
        }
    }
}
