﻿using Hotel.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Models.DataAccesLayer
{
    public class OptionDAL
    {
        public ObservableCollection<Option> GetAllOptions()
        {
            SqlConnection con = DALHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllOptions", con);
                ObservableCollection<Option> result = new ObservableCollection<Option>();
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Option o = new Option();
                    o.OptionID = (int)(reader[0]);//reader.GetInt(0);
                    o.OptionType = reader[1].ToString();//reader[1].ToString();
                    o.Deleted = (bool)reader[2];
                    if (o.Deleted == false)
                        result.Add(o);
                }
                reader.Close();
                return result;
            }
            finally
            {
                con.Close();
            }
        }
        public void AddOption(Option option)
        {
            using (SqlConnection con = DALHelper.Connection)
            {

                SqlCommand cmd = new SqlCommand("AddOption", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramOptionType = new SqlParameter("@OptionType", option.OptionType);

                SqlParameter paramIdOption = new SqlParameter("@OptionID", SqlDbType.Int);
                paramIdOption.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(paramOptionType);
                cmd.Parameters.Add(paramIdOption);
                con.Open();
                cmd.ExecuteNonQuery();
                option.OptionID = (int)paramIdOption.Value;

            }
        }
        public void ModifyOption(Option option)
        {
            DALHelper.CRUDHelperForOptions("UpdateOption", new Tuple<string, string>("@OptionName", option.OptionType), new Tuple<string, int>("@OptionID", option.OptionID));
        }
        public void DeleteOption(Option option)
        {
            DALHelper.CRUDHelper("DeleteOption", new Tuple<string, int>("@OptionID", option.OptionID));
        }
        public Option GetOption(int optionID)
        {
            SqlConnection con = DALHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("GetOption", con);
                Option o = new Option();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramOptionID = new SqlParameter("@OptionID", optionID);
                cmd.Parameters.Add(paramOptionID);
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    o.OptionID = (int)(reader[0]);
                    o.OptionType = reader[1].ToString();
                    o.Deleted = (bool)reader[2];
                }
                reader.Close();
                return o;
            }
            finally
            {
                con.Close();
            }
        }
    }

}
