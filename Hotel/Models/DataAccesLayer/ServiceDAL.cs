﻿using Hotel.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Models.DataAccesLayer
{
    public class ServiceDAL
    {
        public ObservableCollection<Service> GetAllServices()
        {
            SqlConnection con = DALHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllServices", con);
                ObservableCollection<Service> result = new ObservableCollection<Service>();
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Service s = new Service();
                    s.ServiceID = (int)(reader[0]);//reader.GetInt(0);
                    s.ServiceType = reader[1].ToString();//reader[1].ToString();
                    s.Price = (int)reader[2];
                    s.Deleted = (bool)reader[3];
                    if (s.Deleted == false)
                        result.Add(s);
                }
                reader.Close();
                return result;
            }
            finally
            {
                con.Close();
            }
        }
        public void AddSerivce(Service service)
        {
            using (SqlConnection con = DALHelper.Connection)
            {

                SqlCommand cmd = new SqlCommand("AddService", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramServiceType = new SqlParameter("@ServiceType", service.ServiceType);
                SqlParameter paramPrice = new SqlParameter("@Price", service.Price);

                SqlParameter paramIdService = new SqlParameter("@ServiceID", SqlDbType.Int);
                paramIdService.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(paramServiceType);
                cmd.Parameters.Add(paramPrice);
                cmd.Parameters.Add(paramIdService);
                con.Open();
                cmd.ExecuteNonQuery();
                service.ServiceID = (int)paramIdService.Value;

            }
        }
        public void DeleteService(Service service)
        {
            DALHelper.CRUDHelper("DeleteService", new Tuple<string, int>("@ServiceID", service.ServiceID));
        }
        public void ModifyService(Service service)
        {
            DALHelper.CRUDHelperFirstParamStrStr("UpdateService", new Tuple<string, string>("@ServiceName", service.ServiceType),
                new Tuple<string, int>("@ServicePrice", service.Price),
                new Tuple<string, int>("@ServiceID", service.ServiceID));
        }
        public Service GetService(int serviceID)
        {
            SqlConnection con = DALHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("GetService", con);
                Service s = new Service();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramServiceID = new SqlParameter("@ServiceID", serviceID);
                cmd.Parameters.Add(paramServiceID);
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    s.ServiceID = (int)(reader[0]);
                    s.ServiceType = reader[1].ToString();
                    s.Price = (int)(reader[2]);
                }
                reader.Close();
                return s;
            }
            finally
            {
                con.Close();
            }
        }
    }
}
