﻿using Hotel.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Type = Hotel.Models.EntityLayer.Type;

namespace Hotel.Models.DataAccesLayer
{
    public class RoomTypePriceDAL
    {
        public ObservableCollection<RoomTypePrice> GetAllPrices()
        {
            SqlConnection con = DALHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllPrices", con);
                ObservableCollection<RoomTypePrice> result = new ObservableCollection<RoomTypePrice>();
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    RoomTypePrice rtp = new RoomTypePrice();
                    rtp.ID = (int)(reader[0]);
                    rtp.CurrentType = GetRoomTypeByTypeID((int)reader[1]);
                    rtp.Price = (int)reader[2];
                    rtp.StartDate = (DateTime)reader[3];
                    rtp.EndDate = (DateTime)reader[4];
                    rtp.Deleted = Convert.ToBoolean(reader[5]);
                    if (rtp.Deleted == false)
                        result.Add(rtp);

                }
                reader.Close();
                return result;
            }
            finally
            {
                con.Close();
            }
        }
        public Type GetRoomTypeByTypeID(int typeID)
        {
            using (SqlConnection con = DALHelper.Connection)
            {
                Type t = new Type();
                SqlCommand cmd = new SqlCommand("GetRoomTypeByTypeID", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramRoomType = new SqlParameter("@RoomType", typeID);
                cmd.Parameters.Add(paramRoomType);
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    t.RoomType = (int)reader[0];
                    t.TypeName = reader[1].ToString();
                    t.Deleted = Convert.ToBoolean(reader[2]);

                }
                return t;
            }
        }
        public void AddRoomTypePrice(RoomTypePrice rtp)
        {
            DALHelper.CRUDHelperForDates("AddRoomTypePrice", new Tuple<string, DateTime>("@startDate", rtp.StartDate), new Tuple<string, DateTime>("@EndDate", rtp.EndDate),
                new Tuple<string, int>("@RoomType", rtp.CurrentType.RoomType), new Tuple<string, int>("@Price", rtp.Price));

        }
        public void ModifyRoomTypePrice(RoomTypePrice rtp)
        {
            DALHelper.CRUDHelperForDates("ModifyRoomTypePrice", new Tuple<string, DateTime>("@startDate", rtp.StartDate), new Tuple<string, DateTime>("@EndDate", rtp.EndDate),
                           new Tuple<string, int>("@RoomType", rtp.CurrentType.RoomType), new Tuple<string, int>("@Price", rtp.Price), new Tuple<string,int>("@id",rtp.ID));
        }
        public void DeleteRoomPrice(RoomTypePrice rtp)
        {
            DALHelper.CRUDHelper("DeleteRoomPrice", new Tuple<string, int>("@ID", rtp.ID));
        }
        public int GetTodayRoomTypePrice(int roomType)
        {
            using (SqlConnection con = DALHelper.Connection)
            {
                int price = 0;
                SqlCommand cmd = new SqlCommand("GetTodayRoomTypePrice", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter param = new SqlParameter("@RoomType", roomType);
                cmd.Parameters.Add(param);
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    price = (int)reader[0];
                }
                return price;
            }

        }
        public ObservableCollection<RoomTypePrice> GetRoomTypePriceForPeriod(DateTime sDate, DateTime eDate,int roomType)
        {
            using(SqlConnection con=DALHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("GetRoomTypePriceForPeriod", con);
                cmd.CommandType = CommandType.StoredProcedure;
                ObservableCollection<RoomTypePrice> result = new ObservableCollection<RoomTypePrice>();
                SqlParameter paramStartDate = new SqlParameter("@startDate", sDate);
                SqlParameter paramEndDate = new SqlParameter("@endDate", eDate);
                SqlParameter paramRoomType = new SqlParameter("@roomType", roomType);
                cmd.Parameters.Add(paramStartDate);
                cmd.Parameters.Add(paramEndDate);
                cmd.Parameters.Add(paramRoomType);
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while(reader.Read())
                {
                    RoomTypePrice rtp = new RoomTypePrice();
                    rtp.ID = (int)(reader[0]);
                    rtp.CurrentType = GetRoomTypeByTypeID((int)reader[1]);
                    rtp.Price = (int)reader[2];
                    rtp.StartDate = (DateTime)reader[3];
                    rtp.EndDate = (DateTime)reader[4];
                    rtp.Deleted = Convert.ToBoolean(reader[5]);
                    if (rtp.Deleted == false)
                        result.Add(rtp);
                }
                return result;
                    
            }
        }
        public DateTime GetLastAvailableDate()
        {
            using( SqlConnection con=DALHelper.Connection)
            {
                DateTime time = new DateTime();
                SqlCommand cmd = new SqlCommand("GetLastAvailableDate", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while(reader.Read())
                {
                    time= (DateTime)reader[0];
                }
                return time;
            }
        }
    }
}
