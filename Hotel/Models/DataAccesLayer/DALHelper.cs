﻿using System;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Hotel.Models.DataAccesLayer
{
    public static class DALHelper
    {
        private static readonly string connectionString = ConfigurationManager.ConnectionStrings["myConStr"].ConnectionString;
        public static SqlConnection Connection
        {
            get
            {
                return new SqlConnection(connectionString);
            }
        }
        public static void CRUDHelper(string procedure_name, params Tuple<string, int>[] procedure_params)
        {
            SqlConnection con = DALHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand(procedure_name, con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                for (int index = 0; index < procedure_params.Length; index++)
                {
                    SqlParameter param;
                    if (procedure_params[index].Item1 == "@Deleted")
                        param = new SqlParameter("@Deleted", Convert.ToByte(procedure_params[index].Item2));
                    else
                        param = new SqlParameter(procedure_params[index].Item1, procedure_params[index].Item2);
                    cmd.Parameters.Add(param);
                }

                cmd.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
            }
        }
        public static void CRUDHelperForDates(string procedure_name, Tuple<string, DateTime> startDate, Tuple<string, DateTime> endDate, params Tuple<string, int>[] procedure_params)
        {
            SqlConnection con = DALHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand(procedure_name, con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlParameter StartDate = new SqlParameter(startDate.Item1, startDate.Item2);
                SqlParameter EndDate = new SqlParameter(endDate.Item1, endDate.Item2);
                cmd.Parameters.Add(StartDate);
                cmd.Parameters.Add(EndDate);
                for (int index = 0; index < procedure_params.Length; index++)
                {
                    SqlParameter param;
                    if (procedure_params[index].Item1 == "@Deleted")
                        param = new SqlParameter("@Deleted", Convert.ToByte(procedure_params[index].Item2));
                    else
                        param = new SqlParameter(procedure_params[index].Item1, procedure_params[index].Item2);
                    cmd.Parameters.Add(param);
                }

                cmd.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
            }
        }
        public static void CRUDHelperForOptions(string procedure_name, Tuple<string, string> option_name, params Tuple<string, int>[] parameters)
        {
            SqlConnection con = DALHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand(procedure_name, con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();


                SqlParameter nameParam = new SqlParameter(option_name.Item1, option_name.Item2);
                cmd.Parameters.Add(nameParam);
                for (int index = 0; index < parameters.Length; index++)
                {
                    SqlParameter param = new SqlParameter(parameters[index].Item1, parameters[index].Item2);
                    cmd.Parameters.Add(param);

                }
                cmd.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
            }
        }
        public static void CRUDHelperForOffer(string procedure_name, Tuple<string, string> offerName, Tuple<string, DateTime> startDate, Tuple<string, DateTime> endDate, params Tuple<string, int>[] procedure_params)
        {
            SqlConnection con = DALHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand(procedure_name, con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlParameter Name = new SqlParameter(offerName.Item1, offerName.Item2);
                SqlParameter StartDate = new SqlParameter(startDate.Item1, startDate.Item2);
                SqlParameter EndDate = new SqlParameter(endDate.Item1, endDate.Item2);
                cmd.Parameters.Add(Name);
                cmd.Parameters.Add(StartDate);
                cmd.Parameters.Add(EndDate);
                for (int index = 0; index < procedure_params.Length; index++)
                {
                    SqlParameter param;
                    if (procedure_params[index].Item1 == "@Deleted")
                        param = new SqlParameter("@Deleted", Convert.ToByte(procedure_params[index].Item2));
                    else
                        param = new SqlParameter(procedure_params[index].Item1, procedure_params[index].Item2);
                    cmd.Parameters.Add(param);
                }

                cmd.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
            }
        }
        public static void CRUDHelperFirstParamStrStr(string procedure_name, Tuple<string, string> option_name, params Tuple<string, int>[] parameters)
        {
            SqlConnection con = DALHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand(procedure_name, con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();


                SqlParameter nameParam = new SqlParameter(option_name.Item1, option_name.Item2);
                cmd.Parameters.Add(nameParam);
                for (int index = 0; index < parameters.Length; index++)
                {
                    SqlParameter param = new SqlParameter(parameters[index].Item1, parameters[index].Item2);
                    cmd.Parameters.Add(param);

                }
                cmd.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
            }
        }
        public static void CRUDHelperForImages(string procedure_name, Tuple<string, string> image, params Tuple<string, int>[] parameters)
        {
            SqlConnection con = DALHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand(procedure_name, con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();


                SqlParameter imageParam = new SqlParameter(image.Item1, image.Item2);
                cmd.Parameters.Add(imageParam);
                for (int index = 0; index < parameters.Length; index++)
                {
                    SqlParameter param = new SqlParameter(parameters[index].Item1, parameters[index].Item2);
                    cmd.Parameters.Add(param);

                }
                cmd.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
            }
        }
    }
}
