﻿using Hotel.Models.EntityLayer;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Windows;

namespace Hotel.Models.DataAccesLayer
{
    public class UserDAL
    {
        public ObservableCollection<User> GetAllUsers()
        {
            SqlConnection con = DALHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllUsers", con);
                ObservableCollection<User> result = new ObservableCollection<User>();
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    User u = new User();
                    u.UserID = (int)(reader[0]);//reader.GetInt(0);
                    u.UserType = reader[1].ToString();//reader[1].ToString();
                    u.Username = reader[2].ToString();
                    u.Password = reader[3].ToString();
                    result.Add(u);
                }
                reader.Close();
                return result;
            }
            finally
            {
                con.Close();
            }
        }
        public int GetUser(User user)
        {
            using (SqlConnection con = DALHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("GetUser", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramUsername = new SqlParameter("@Username", user.Username);
                SqlParameter paramPassword = new SqlParameter("@Password", user.Password);

                cmd.Parameters.Add(paramUsername);
                cmd.Parameters.Add(paramPassword);
                con.Open();
                if (cmd.ExecuteScalar() != null)
                    return (int)cmd.ExecuteScalar();
            }
            return -1;

        }
        public void AddUser(User user)
        {
            using (SqlConnection con = DALHelper.Connection)
            {
                if (GetUser(user) < 1 && user.UserType != "")
                {
                    SqlCommand cmd = new SqlCommand("AddUser", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter paramUserType = new SqlParameter("@UserType", user.UserType);
                    SqlParameter paramUsername = new SqlParameter("@Username", user.Username);
                    SqlParameter paramPassword = new SqlParameter("@Password", user.Password);

                    SqlParameter paramIdUser = new SqlParameter("@UserID", SqlDbType.Int);
                    paramIdUser.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(paramUserType);
                    cmd.Parameters.Add(paramUsername);
                    cmd.Parameters.Add(paramPassword);
                    cmd.Parameters.Add(paramIdUser);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    user.UserID = (int)paramIdUser.Value;
                    MessageBox.Show("Account created successfully!");
                }
                else
                {
                    if (GetUser(user) < 1)
                        MessageBox.Show("Username or password is wrong!");
                    else
                        MessageBox.Show("Login successfully!");
                }
            }
        }
        public int GetUserIDByName(string username)
        {
            using (SqlConnection con = DALHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("GetUserIDByName", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter name = new SqlParameter("@name", username);
                cmd.Parameters.Add(name);
                con.Open();
                return (int)cmd.ExecuteScalar();
            }
        }
    }
}
