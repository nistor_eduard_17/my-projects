﻿using Hangman.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Hangman.Commands
{
    internal class RelayCommand<T> : ICommand
    {
        private Action<T> commandTask;
        private Action commandTask1;
        private Predicate<object> canExecuteTask;
        public RelayCommand(Action<T> workToDo, Predicate<object> canExecute)
        {
            commandTask = workToDo;
            canExecuteTask = canExecute;
        }

        public RelayCommand(Action workToDo, Predicate<object> canExecute)
        {
            commandTask1 = workToDo;
            canExecuteTask = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return canExecuteTask != null && canExecuteTask(parameter);
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }

            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public void Execute(object parameter)
        {
            //if(commandTask != null)
            //    commandTask((T)parameter);
            //e echivalent
            commandTask?.Invoke((T)parameter);
            commandTask1?.Invoke();
        }

        //private Action<object> commandTask;
        //private Action commandTask1;
        //private Predicate<object> canExecuteTask;

        //public RelayCommand(Action<object> workToDo)
        //{
        //    commandTask = workToDo;
        //}

        //public RelayCommand(Action workToDo, Predicate<object> canExecute)
        //{
        //    commandTask1 = workToDo;
        //    canExecuteTask = canExecute;
        //}

        //public RelayCommand(Action<object> workToDo, Predicate<object> canExecute)
        //{
        //    commandTask = workToDo;
        //    canExecuteTask = canExecute;
        //}

        //private static bool DefaultCanExecute(object parameter)
        //{
        //    return true;
        //}

        //public bool CanExecute(object parameter)
        //{
        //    return canExecuteTask != null && canExecuteTask(parameter);
        //}

        //public event EventHandler CanExecuteChanged
        //{
        //    add
        //    {
        //        CommandManager.RequerySuggested += value;
        //    }

        //    remove
        //    {
        //        CommandManager.RequerySuggested -= value;
        //    }
        //}

        //public void Execute(object parameter)
        //{
        //    commandTask?.Invoke((T)parameter);
        //    commandTask1?.Invoke();
        //}
    }
}
