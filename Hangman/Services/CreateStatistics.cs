﻿using Hangman.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Hangman.Services
{
    internal class CreateStatistics : NotifyPropertyChanged
    {
        public ObservableCollection<Category> categories { get; set; }
        public ObservableCollection<Player> players;

        ReadWrite readWrite;

        public CreateStatistics(ObservableCollection<Category> categories)
        {
            this.categories = categories;
            readWrite = new ReadWrite();
            Read();
            for (int index = 0; index < players.Count; ++index)
            {
                categories.Add(new Category());
                categories[index].Username = players[index].Username;
            }

            CreateStatistic();
        }

        private void Read()
        {
            XmlSerializer xmlser = new XmlSerializer(typeof(PlayerToSerialize));
            FileStream file = new FileStream("players.xml", FileMode.Open);
            var c = (xmlser.Deserialize(file) as PlayerToSerialize).ListOfPlayers;
            if (players == null)
                players = new ObservableCollection<Player>();
            players.Clear();
            foreach (var player in c)
            {
                players.Add(player);
            }
            file.Dispose();
        }

        private void CreateStatistic()
        {
            for (int index = 0; index < players.Count; ++index)
            {
                foreach (GameInfo game in players[index].GameInfo)
                {
                    switch (game.Category)
                    {
                        case "AllCategories":
                            if (game.Lives == 0 || game.SecondsRemaining==0)
                                categories[index].AllCategory.Lost++;
                            if (game.Level == 6)
                                categories[index].AllCategory.Won++;
                            break;
                        case "Animals":
                            if (game.Lives == 0 || game.SecondsRemaining == 0)
                                categories[index].Animals.Lost++;
                            if (game.Level == 6)
                                categories[index].Animals.Won++;
                            break;
                        case "Cars":
                            if (game.Lives == 0 || game.SecondsRemaining == 0)
                                categories[index].Cars.Lost++;
                            if (game.Level == 6)
                                categories[index].Cars.Won++;
                            break;
                        case "Mountains":
                            if (game.Lives == 0 || game.SecondsRemaining == 0)
                                categories[index].Mountains.Lost++;
                            if (game.Level == 6)
                                categories[index].Mountains.Won++;
                            break;
                        case "Movies":
                            if (game.Lives == 0 || game.SecondsRemaining == 0)
                                categories[index].Movies.Lost++;
                            if (game.Level == 6)
                                categories[index].Movies.Won++;
                            break;
                        case "Rivers":
                            if (game.Lives == 0 || game.SecondsRemaining == 0)
                                categories[index].Rivers.Lost++;
                            if (game.Level == 6)
                                categories[index].Rivers.Won++;
                            break;

                    }
                }
            }
        }
    }
}
