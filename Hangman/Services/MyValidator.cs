﻿using Hangman.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Services
{
    static class MyValidator
    {
        public static bool CanExecuteOperation(ObservableCollection<Player> players, string username)
        {
            Player player=new Player();
            if (players != null)
                player = players.FirstOrDefault(p => p.Username == username);
            if (player != null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
