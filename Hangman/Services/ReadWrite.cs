﻿using Hangman.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Serialization;

namespace Hangman.Services
{
    internal class ReadWrite
    {
        public ObservableCollection<Player> players;
        public ReadWrite()
        {
        }

        public ReadWrite(ObservableCollection<Player> players)
        {
            this.players = players;
        }

        public void SerializeObject(PlayerToSerialize entity)
        {
            XmlSerializer xmlser = new XmlSerializer(typeof(PlayerToSerialize));
            FileStream fileStr = new FileStream("players.xml", FileMode.Create);
            xmlser.Serialize(fileStr, entity);
            fileStr.Dispose();
        }

        public void SerializeCategory(CategoryToSerialize entity)
        {
            XmlSerializer xmlser = new XmlSerializer(typeof(CategoryToSerialize));
            FileStream fileStr = new FileStream("statistics.xml", FileMode.Create);
            xmlser.Serialize(fileStr, entity);
            fileStr.Dispose();
        }
        public void DeserializeObject()
        {
            XmlSerializer xmlser = new XmlSerializer(typeof(PlayerToSerialize));
            FileStream file = new FileStream("players.xml", FileMode.Open);
            var c = (xmlser.Deserialize(file) as PlayerToSerialize).ListOfPlayers;
            if (players == null)
                players = new ObservableCollection<Player>();
            players.Clear();
            foreach (var player in c)
            {
                players.Add(player);
            }
            file.Dispose();
        }

        public void ReadWords(string category, List<string> Words)
        {
            if (category == "")
            {
                MessageBox.Show("Please choose a category!");
                return;
            }
            {
                string path = "../../Categories/" + category + ".txt";
                StreamReader sr = new StreamReader(path);

                string line, word;
                while ((line = sr.ReadLine()) != null)
                    Words.Add(line);
            }
        }
        public void Read(ObservableCollection<Player> Players)
        {

            StreamReader sr = new StreamReader("../../players.txt");

            string line, username = "", imagePath = "";

            while ((line = sr.ReadLine()) != null)
            {
                if (username.Length == 0)
                    username = line;
                else
                    imagePath = line;

                if (username != "" && imagePath != "")
                {
                    Player player = new Player(username)
                    {
                        ImagePath = imagePath
                    };
                    Players.Add(player);
                    username = "";
                    imagePath = "";
                }
            }
            sr.Close();
        }
        public void Write(ObservableCollection<Category> Categories)
        {
            using (StreamWriter sw = new StreamWriter("../../statistics.txt"))
            {
                sw.Write("");
            }
            foreach (Category category in Categories)
            {
                using (StreamWriter sw = File.AppendText("../../statistics.txt"))
                {
                    sw.WriteLine(category.Username);
                    sw.WriteLine("All Categories: ");
                    sw.Write("Won: " + category.AllCategory.Won + "  Lost: " + category.AllCategory.Lost + '\n');
                    sw.WriteLine("Animals: ");
                    sw.Write("Won: " + category.Animals.Won + "  Lost: " + category.Animals.Lost + '\n');
                    sw.WriteLine("Cars: ");
                    sw.Write("Won: " + category.Cars.Won + "  Lost: " + category.Cars.Lost + '\n');
                    sw.WriteLine("Mountains: ");
                    sw.Write("Won: " + category.Mountains.Won + "  Lost: " + category.Mountains.Lost + '\n');
                    sw.WriteLine("Movies: ");
                    sw.Write("Won: " + category.Movies.Won + "  Lost: " + category.Movies.Lost + '\n');
                    sw.WriteLine("Rivers: ");
                    sw.Write("Won: " + category.Rivers.Won + "  Lost: " + category.Rivers.Lost + "\n\n");

                }
            }
        }
    }
}
