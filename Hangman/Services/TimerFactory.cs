﻿using Hangman.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Models
{
    internal class TimerFactory : ITimerFactory
    {
        public ITimer CreateTimer() => new Timer();
    }
}
