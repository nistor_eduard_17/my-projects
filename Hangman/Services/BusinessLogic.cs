﻿using Hangman.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Services
{
    internal class BusinessLogic
    {
        private ObservableCollection<Player> Players;
        private ReadWrite readWrite;
        public BusinessLogic()
        {
            Players = new ObservableCollection<Player>();
        }
        public BusinessLogic(ObservableCollection<Player> players)
        {
            this.Players = players;
        }

        public int Size()
        {
            return Players.Count;
        }

        public Player First()
        {
            return Players[0];
        }
        public bool UsernameAlreadyExists(string username)
        {
            if (Players != null)
                foreach (Player player in Players)
                    if (player.Username == username)
                        return true;
            return false;
        }
    }
}
