﻿using Hangman.Models;
using Hangman.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Hangman.Services
{
    internal class Game : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private readonly Player CurrentPlayer;
        private readonly ITimerFactory timerFactory;
        private readonly ITimer timer;
        private GameInfo GameInfo;
        private int index;

        private int OldIndex { get; set; }
        private int Index
        {
            get => index;
            set
            {
                index = value;
                OnPropertyChanged("Index");
            }
        }
        public ObservableCollection<MyButtons> ButtonsContent { get; set; }
        public ObservableCollection<MyButtons> WordButtons { get; set; }
        public Game(Player player, int index)
        {
            CurrentPlayer = player;
            if (player.GameInfo.Count > 0)
                GameInfo = player.GameInfo[index];
            else
                GameInfo = new GameInfo(player.Username);

            Index = index;
            this.timerFactory = new TimerFactory();
            this.timer = timerFactory.CreateTimer();
            this.timer.Interval = TimeSpan.FromSeconds(1);
            this.timer.Tick += new EventHandler(UpdateState);
            CreateButtons();

        }

        public Player Player => CurrentPlayer;
        public int SecondsRemaining
        {
            get => GameInfo.SecondsRemaining;
            set
            {
                GameInfo.SecondsRemaining = value;
                OnPropertyChanged("SecondsRemaining");
            }
        }
        public string WordToGuess
        {
            get { return GameInfo.WordToGuess; }
            set
            {
                GameInfo.WordToGuess = value;
                OnPropertyChanged("WordToGuess");
            }
        }
        public string Category
        {
            get => GameInfo.Category;
            set
            {
                if (GameInfo.Category != value)
                    Level = 1;
                GameInfo.Category = value;
                OnPropertyChanged("Category");
            }
        }
        public List<char> LettersLeft
        {
            get => GameInfo.LettersLeft;
            set
            {
                GameInfo.LettersLeft = value;
                OnPropertyChanged("LettersLeft");
            }
        }
        public int Level
        {
            get { return GameInfo.Level; }
            set
            {

                GameInfo.Level = value;

                OnPropertyChanged("Level");
            }
        }
        public int Lives
        {
            get
            {
                return GameInfo.Lives;
            }
            set
            {
                GameInfo.Lives = value;
                if (value == 0)
                    Save();
                OnPropertyChanged("Lives");
            }
        }
        public string HangmanImage
        {
            get
            {
                return GameInfo.HangmanImage;
            }
            set
            {
                GameInfo.HangmanImage = value;
                OnPropertyChanged("HangmanImage");
            }
        }
        public List<char> GuessUntilNow
        {
            get
            {
                return GameInfo.GuessUntilNow;
            }
        }
        public string WordState
        {
            get => GameInfo.WordState;
            set
            {
                GameInfo.WordState = value;
                OnPropertyChanged("WordState");
            }
        }
        public List<char> AllLetters()
        {
            List<char> letters = new List<char>();
            for (char c = 'A'; c <= 'Z'; ++c)
                letters.Add(c);
            return letters;
        }

        private void UpdateState(object sender, EventArgs e)
        {
            SecondsRemaining--;
            if (SecondsRemaining == 0)
            {
                IsGameOver();
                Save();
                this.timer.Stop();
                this.timer.IsEnabled = false;
            }
        }
        public void SetWord(string category)
        {
            if (category != "")
            {
                GenerateWord generateWord = new GenerateWord(category);
                GameInfo.WordToGuess = generateWord.GetRandomWord();
                GameInfo.GuessUntilNow = new List<char>();
                Category = category;
                foreach (char c in WordToGuess)
                {
                    GameInfo.GuessUntilNow.Add(' ');
                    WordButtons.Add(new MyButtons() { Content = c, IsEnabled = true });
                }
                UpdateWordState();
            }

        }

        private void ResetButtons()
        {
            foreach (char c in AllLetters())
                ButtonsContent[c - 'A'].IsEnabled = true;
            WordButtons.Clear();
        }
        public void Reset()
        {
            ResetButtons();
            SetWord(GameInfo.Category);
            Lives = 6;
            HangmanImage = "/Hangman;component/Images/hangmanPost.jpg";
            GameInfo.LettersLeft.Clear();
            GameInfo.LettersLeft = AllLetters();
            SecondsRemaining = 30;
            this.timer.Stop();
            this.timer.IsEnabled = false;


        }

        private bool ExistsAnyChange(GameInfo game1, GameInfo game2)
        {
            if (game1.Level == game2.Level && game1.Lives == game2.Lives && game1.GuessUntilNow == game2.GuessUntilNow &&
                game1.LettersLeft == game2.LettersLeft)
                return false;
            return true;
        }
        public void DeleteLetterAndCheck(char letter)
        {
            ButtonsContent[letter - 'A'].IsEnabled = false;
            GameInfo.LettersLeft.Remove(letter);
            Check(letter);
        }
        public void SetIndex()
        {
            Level = 1;
            OldIndex = Index;
            Index = -1;
        }
        public void StopGame()
        {
            StopTimer();
            ReadWrite rw = new ReadWrite();
            GameInfo initialGameInfo = new GameInfo();
            rw.DeserializeObject();
            if (Index != -1 && Index < CurrentPlayer.GameInfo.Count)
            {
                initialGameInfo = rw.players.Single(player => player.Username == CurrentPlayer.Username).GameInfo[Index];
                if (initialGameInfo != null)
                {
                    if (index < CurrentPlayer.GameInfo.Count)
                        CurrentPlayer.GameInfo[Index] = initialGameInfo;
                    return;
                }

                if (index < CurrentPlayer.GameInfo.Count)
                    CurrentPlayer.GameInfo[Index] = initialGameInfo;
            }
            else
            {
                if (OldIndex != -1)
                {
                    initialGameInfo = rw.players.Single(player => player.Username == CurrentPlayer.Username).GameInfo[OldIndex];
                    CurrentPlayer.GameInfo[OldIndex] = initialGameInfo;
                }

            }
        }


        public void StopTimer()
        {
            this.timer.Stop();
            this.timer.IsEnabled = false;
        }
        public void Save()
        {
            ReadWrite readWrite = new ReadWrite();
            ObservableCollection<Player> players = new ObservableCollection<Player>();
            readWrite.DeserializeObject();
            players = readWrite.players;

            if (GameInfo.WordState != null)
                GameInfo.WordState = GameInfo.WordState.Remove(0, GameInfo.WordState.Length);

            foreach (char c in GuessUntilNow)
                GameInfo.WordState += c;

            if (Index != -1)
                players[0].GameInfo[Index] = GameInfo;
            else
                players[0].GameInfo.Add(GameInfo);
            this.timer.Stop();
            this.timer.IsEnabled = false;
            readWrite.SerializeObject(new PlayerToSerialize() { ListOfPlayers = players });
        }
        public bool IsGameOver()
        {
            if (Lives == 0 || GameInfo.SecondsRemaining == 0)
            {
                this.timer.Stop();
                MessageBox.Show("Game over! \n The word was: " + WordToGuess);
                for (int index = 0; index < 26; ++index)
                    ButtonsContent[index].IsEnabled = false;
                return true;
            }
            return false;
        }
        public int LevelCount()
        {
            if (Level <= 5)
                return Level;
            else
            {
                WordButtons.Clear();
                ButtonsContent.Clear();
                MessageBox.Show("Congratulation! You won the game!");
                Save();
                return -1;
            }
        }
        public bool IsWin()
        {
            for (int index = 0; index < GameInfo.WordToGuess.Length; ++index)
                if (GameInfo.WordToGuess[index] != GuessUntilNow[index])
                    return false;
            Level++;
            Reset();
            return true;
        }
        private void ChangeGuessUntilNow()
        {
            if (WordState != null)
                for (int index = 0; index < WordState.Length; ++index)
                    GuessUntilNow[index] = WordState[index];
        }
        private void CreateButtons()
        {
            ButtonsContent = new ObservableCollection<MyButtons>();
            WordButtons = new ObservableCollection<MyButtons>();

            foreach (char c in AllLetters())
            {
                bool enabled = false;
                if (LettersLeft.Contains(c))
                    enabled = true;
                ButtonsContent.Add(new MyButtons() { Content = c, IsEnabled = enabled });
            }

            if (WordState != null)
            {
                foreach (char c in WordState)
                    WordButtons.Add(new MyButtons() { Content = c, IsEnabled = true });
                UpdateWordState();
            }
        }
        private void UpdateWordState()
        {
            if (WordState != null)
            {
                WordState = WordState.Remove(0, WordState.Length);
            }
            for (int index = 0; index < GameInfo.GuessUntilNow.Count; ++index)
                if (GuessUntilNow[index] == ' ')
                {
                    if (WordToGuess != "")
                        if (WordToGuess[index] == ' ')
                            WordState += ' ';
                        else
                            WordState += '_';
                }
                else
                    WordState += GuessUntilNow[index];

            UpdateWordButtons();
            ChangeGuessUntilNow();
        }

        private void UpdateWordButtons()
        {
            if (WordButtons.Count == 0)
                CreateButtons();
            for (int index = 0; index < WordState.Length; ++index)
                WordButtons[index].Content = WordState[index];
        }
        public void Check(char c)
        {
            if (this.timer.IsEnabled == false)
            {
                this.timer.Start();
            }
            bool found = false;

            for (int index = 0; index < GameInfo.GuessUntilNow.Count; ++index)
                if (WordToGuess[index] == c)
                {
                    GameInfo.GuessUntilNow[index] = c;
                    found = true;
                }
            UpdateWordState();
            if (found == false)
                UpdateLivesAndImage();
        }
        private void UpdateLivesAndImage()
        {
            Lives--;
            switch (Lives)
            {
                case 5:
                    HangmanImage = "/Hangman;component/Images/head.jpg";
                    break;
                case 4:
                    HangmanImage = "/Hangman;component/Images/body.jpg";
                    break;
                case 3:
                    HangmanImage = "/Hangman;component/Images/left_arm.jpg";
                    break;
                case 2:
                    HangmanImage = "/Hangman;component/Images/right_arm.jpg";
                    break;
                case 1:
                    HangmanImage = "/Hangman;component/Images/left_leg.jpg";
                    break;
                case 0:
                    HangmanImage = "/Hangman;component/Images/right_leg.jpg";
                    break;
            }
        }
    }
}
