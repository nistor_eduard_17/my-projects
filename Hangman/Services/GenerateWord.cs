﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Services
{
    internal class GenerateWord
    {
        ReadWrite readWrite;
        List<string> words;
        private string category;
        public GenerateWord(string category)
        {
            readWrite = new ReadWrite();
            words = new List<string>();
            this.category = category;
            GetListOfWords();
        }

        private void GetListOfWords()
        {
            readWrite.ReadWords(category, words);
        }

        public string GetRandomWord()
        {
            var random = new Random();
            return words[random.Next(words.Count)];
        }

    }
}
