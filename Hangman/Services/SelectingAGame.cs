﻿using Hangman.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Services
{
    public class SelectingAGame : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public ObservableCollection<GameInfo> Games { get; set; }

        public Player currentPlayer { get; set; }
        ReadWrite readWrite = new ReadWrite();
        ObservableCollection<Player> players = new ObservableCollection<Player>();
        public SelectingAGame(ObservableCollection<GameInfo> games,Player player)
        {
            readWrite.DeserializeObject();
            players = readWrite.players;
            currentPlayer = player;
            Games = games;
        }

        public void SaveAddedGames()
        {
            readWrite.SerializeObject(new PlayerToSerialize() { ListOfPlayers = players });
        }
        public void AddNewGame(GameInfo gameInfo)
        {
            Games.Add(gameInfo);
            players[0].GameInfo.Add(gameInfo);
            //currentPlayer.GameInfo.Add(gameInfo);
            SaveAddedGames();
        }

        public Player UpdateUser(Player player)
        {
            return players[0];
        }

        public int ActualPositionInGames(int interfaceIndex)
        {
            GameInfo game = new GameInfo();
            game = Games[interfaceIndex];
            for (int index = 0; index < players[0].GameInfo.Count; ++index)
                if (players[0].GameInfo[index].Category == game.Category &&
                    players[0].GameInfo[index].Level == game.Level &&
                    players[0].GameInfo[index].Lives == game.Lives &&
                    players[0].GameInfo[index].WordToGuess == game.WordToGuess &&
                    players[0].GameInfo[index].WordState == game.WordState)
                    return index;
            return 0;
        }

    }
}
