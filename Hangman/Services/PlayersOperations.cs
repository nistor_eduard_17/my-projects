﻿using Hangman.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Services
{
    internal class PlayersOperations
    {
        private ObservableCollection<Player> Players { get; set; }

        private ObservableCollection<Category> Categories { get; set; }

        ReadWrite readWrite;

        public PlayersOperations(ObservableCollection<Player> Players)
        {
            this.Players = Players;
            readWrite = new ReadWrite();
        }

        public PlayersOperations(ObservableCollection<Category> Categories)
        {
            this.Categories = Categories;
            readWrite = new ReadWrite();
        }

        public void ShowStatistics(string message="")
        {
            CategoryToSerialize categories = new CategoryToSerialize()
            {
                ListOfCategory = Categories
            };
            readWrite.SerializeCategory(categories);
        }

        public void Add(Player player)
        {
            Player p = new Player(player.Username)
            {
                ImagePath = player.ImagePath
            };
            Players.Add(p);
            SelectPlayer(p);
            OpenWindow();
        }
        public void Delete(string player)
        {
            Players.Remove(Players.First(p => p.Username == player));

            PlayerToSerialize players = new PlayerToSerialize()
            {
                ListOfPlayers = Players
            };
            readWrite.SerializeObject(players);
        }
        public void SelectPlayer(Player player)
        {
            int position = Players.IndexOf(player);
            Players.Move(position, 0);
 
            PlayerToSerialize players= new PlayerToSerialize()
            {
                ListOfPlayers=Players
            };
            readWrite.SerializeObject(players);
            OpenWindow();
        }

        private void OpenWindow()
        {
            MainWindow newWindow = new MainWindow();
            newWindow.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            newWindow.Show();
            System.Windows.Application.Current.Windows[0].Close();
        }
        private string ChangePath(string imagePath, bool option)
        {
            DirectoryInfo dir = new DirectoryInfo("../../Images/Avatar");
            int numberOfPictures = dir.GetFiles().Length;
            string pictureName = Path.GetFileNameWithoutExtension(imagePath);
            int pictureNumber = Int32.Parse(pictureName[pictureName.Length - 1].ToString());
            int initialPictureNumber = pictureNumber;
            if (option)
            {
                if (pictureNumber == 1)
                    pictureNumber = numberOfPictures;
                else
                    pictureNumber--;
            }
            else
            {
                if (pictureNumber == numberOfPictures)
                    pictureNumber = 1;
                else
                    pictureNumber++;

            }
            imagePath = imagePath.Replace((char)(initialPictureNumber + '0'), (char)(pictureNumber + '0'));
            return imagePath;
        }
        public void PreviousPicture(Player player)
        {
            player.ImagePath=ChangePath(player.ImagePath, true);
        }
        public void NextPicture(Player player)
        {
            player.ImagePath = ChangePath(player.ImagePath, false);
        }
    }
}
