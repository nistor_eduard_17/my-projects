﻿using Hangman.Models;
using Hangman.Services;
using Hangman.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hangman.Views
{
    /// <summary>
    /// Interaction logic for PlayerSelection.xaml
    /// </summary>
    public partial class PlayerSelection : Window
    {
        public PlayerSelection()
        {
            InitializeComponent();
        }
        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectionCancel.Visibility = Visibility.Visible;
            if (CreateAccountGroup.Visibility == Visibility.Visible && listOfPlayers.SelectedItem != null)
                CreateAccountGroup.Visibility = Visibility.Hidden;
            if (listOfPlayers.SelectedIndex == -1)
                CreateAccountGroup.Visibility = Visibility.Visible;
        }
        private void SelectionCancel_Click(object sender, RoutedEventArgs e)
        {
            CreateAccountGroup.Visibility = Visibility.Visible;
            listOfPlayers.SelectedItem = null;
            SelectionCancel.Visibility = Visibility.Hidden;

        }
        private void Username_SelectionChanged(object sender, RoutedEventArgs e)
        {
            if (SelectAccountGroup.Visibility == Visibility.Visible && username.Text.Length > 0)
                SelectAccountGroup.Visibility = Visibility.Collapsed;
            else
                if (SelectAccountGroup.Visibility == Visibility.Collapsed && username.Text.Length == 0)
                SelectAccountGroup.Visibility = Visibility.Visible;
        }
    }
}
