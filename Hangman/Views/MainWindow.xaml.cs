﻿using Hangman.Models;
using Hangman.Services;
using Hangman.ViewModels;
using Hangman.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Hangman
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        readonly BusinessLogic bl;
        public MainWindow()
        {
            InitializeComponent();
            bl = new BusinessLogic((this.DataContext as UserVM).Players);
        }
        private void ChangePlayer_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            PlayerSelection playerSelection = new PlayerSelection();
            playerSelection.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            playerSelection.Show();
            this.Close();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (CurrentUser.Content != null)
                if (CurrentUser.Content.ToString().Length != 0)
                    PlayBtn.IsEnabled = true;
        }
        private void PlayBtn_Click(object sender, RoutedEventArgs e)
        {
            SelectGame selectGame = new SelectGame(bl.First());
            selectGame.AddBtn.Visibility=Visibility.Visible;
            this.Close();
            selectGame.Show();
        }
        private void ExitBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
