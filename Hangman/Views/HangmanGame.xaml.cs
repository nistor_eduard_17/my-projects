﻿using Hangman.Models;
using Hangman.Services;
using Hangman.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hangman.Views
{
    /// <summary>
    /// Interaction logic for HangmanGame.xaml
    /// </summary>
    public partial class HangmanGame : Window
    {
        private Game hangmanGame;
        private int Index { get; set; }
        public HangmanGame(Player player, int index)
        {
            InitializeComponent();
            hangmanGame = new Game(player, index);
            Index = index;
            DataContext = new HangmanGameVM(player, hangmanGame);
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (hangmanGame.IsGameOver() == true)
                return;

            if (hangmanGame.IsWin() == false)
                hangmanGame.DeleteLetterAndCheck(((sender as Button).Content.ToString()[0]));

            if (hangmanGame.IsWin())
                if (hangmanGame.LevelCount() != -1)
                    UpdateWord(hangmanGame.Category);
                else
                    return;
        }
        private void UpdateWord(string category)
        {
            hangmanGame.SetWord(category);
            hangmanGame.Reset();
        }
        private void SaveGame_Click(object sender, RoutedEventArgs e)
        {
            hangmanGame.Save();
        }
        private void Category_Click(object sender, RoutedEventArgs e)
        {
            MenuItem menuItem = (sender as MenuItem).Parent as MenuItem;
            foreach (MenuItem item in menuItem.Items)
                if (item.IsChecked)
                    item.IsChecked = false;
            (sender as MenuItem).IsChecked = true;

            UpdateWord((sender as MenuItem).Name);
        }
        private void TextBox_SelectionChanged(object sender, RoutedEventArgs e)
        {
            if (Seconds.Text == "0")
                if (hangmanGame.IsGameOver() == true)
                {
                    return;
                }
        }
        private void NewGame_Click(object sender, RoutedEventArgs e)
        {
            UpdateWord(hangmanGame.Category);
            hangmanGame.SetIndex();
        }
        private void OpenGame_Click(object sender, RoutedEventArgs e)
        {
            hangmanGame.StopGame();
            SelectGame selectGame = new SelectGame(hangmanGame.Player);
            selectGame.Show();
            this.Close();
        }
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            About about = new About();
            about.Show();
        }
        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            hangmanGame.StopTimer();
            MainWindow newWindow = new MainWindow();
            newWindow.Show();
            this.Close();
        }
        private void Statistics_Click(object sender, RoutedEventArgs e)
        {
            hangmanGame.StopTimer();
            Statistics statistics = new Statistics();
            statistics.Show();
        }
    }
}
