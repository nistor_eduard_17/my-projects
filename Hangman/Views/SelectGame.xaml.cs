﻿using Hangman.Models;
using Hangman.Services;
using Hangman.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hangman.Views
{
    /// <summary>
    /// Interaction logic for SelectGame.xaml
    /// </summary>
    public partial class SelectGame : Window
    {
        Player currentPlayer;
        SelectingAGame selectGame;
        public SelectGame(Player player)
        {
            InitializeComponent();
            currentPlayer = player;
            DataContext = new SelectGameVM(player);
            selectGame = new SelectingAGame((this.DataContext as SelectGameVM).Games,player);
        }

        private void listOfGames_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listOfGames.SelectedIndex != -1)
            {
                HangmanGame hangmanGame = new HangmanGame(selectGame.UpdateUser(currentPlayer),selectGame.ActualPositionInGames(listOfGames.SelectedIndex));
                hangmanGame.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                hangmanGame.Show();
                this.Close();
            }
        }

        private void AddBtn_Click(object sender, RoutedEventArgs e)
        {
            selectGame.AddNewGame(new GameInfo(currentPlayer.Username));

        }
    }
}
