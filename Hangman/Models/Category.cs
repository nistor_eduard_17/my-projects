﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Hangman.Models
{
    [Serializable]
    public class Category : NotifyPropertyChanged
    {
        private int won;
        private string username;
        private int lost;
        private Category allCategory;
        private Category animals;
        private Category cars;
        private Category mountains;
        private Category movies;
        private Category rivers;

        public Category()
        {
            Won = Lost = 0;
        }
        
        [XmlAttribute]
        public string Username { get => username; set { username = value;OnPropertyChanged("Username"); } }

        [XmlAttribute]
        public int Won { get => won; set { won = value; OnPropertyChanged("Won"); } }

        [XmlAttribute]
        public int Lost { get => lost; set { lost = value; OnPropertyChanged("Lost"); } }

        [XmlElement]
        public Category AllCategory
        {
            get
            {
                if (allCategory == null)
                    allCategory = new Category();
                return allCategory;
            }
            set { allCategory = value; OnPropertyChanged("AllCategory"); }
        }

        [XmlIgnore]
        public Category Animals
        {
            get
            {
                if (animals == null)
                    animals = new Category();
                return animals;
            }
            set { animals = value; OnPropertyChanged("Animals"); }
        }
       [XmlIgnore]
        public Category Cars
        {
            get
            {
                if (cars == null)
                    cars = new Category();
                return cars;
            }
            set { cars = value; OnPropertyChanged("Cars"); }
        }
       
        [XmlIgnore]
        public Category Mountains
        {
            get
            {
                if (mountains == null)
                    mountains = new Category();
                return mountains;
            }
            set { mountains = value; OnPropertyChanged("Mountains"); }
        }
        [XmlIgnore]
        public Category Movies
        {
            get
            {
                if (movies == null)
                    movies = new Category();
                return movies;
            }
            set { movies = value; OnPropertyChanged("Movies"); }
        }
        [XmlIgnore]
        public Category Rivers
        {
            get
            {
                if (rivers == null)
                    rivers = new Category();
                return rivers;
            }
            set { rivers = value; OnPropertyChanged("Rivers"); }
        }

    }
}
