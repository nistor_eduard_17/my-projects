﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Hangman.Models
{
    [Serializable]
    public class GameInfo : NotifyPropertyChanged
    {

        private string usernname;
        private string category;
        private string hangmanImage;
        private int level;
        private int lives;
        private int secondsRemaining;
        private string wordToGuess;
        private List<char> guessUntilNow;
        private List<char> lettersLeft;
        private string wordState;
        public GameInfo()
        {
        }
        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public GameInfo(GameInfo obj)
        {
            Username = obj.Username;
            Category = obj.Category;
            HangmanImage = obj.HangmanImage;
            Level = obj.Level;
            Lives=obj.Lives;
            SecondsRemaining=obj.SecondsRemaining;
            WordState = obj.WordState;
            WordToGuess = obj.WordToGuess;
            LettersLeft=obj.LettersLeft;
            GuessUntilNow=obj.GuessUntilNow;
        }

        public GameInfo(string username, string word = "", string category = "")
        {
            Level = 1;
            Lives = 6;
            LettersLeft = new List<char>();
            for (char c = 'A'; c <= 'Z'; c++)
                LettersLeft.Add(c);
            Username = username;
            SecondsRemaining = 30;
            HangmanImage = "/Hangman;component/Images/hangmanPost.jpg";
            WordToGuess = word;
            Category = category;
            GuessUntilNow = new List<char>();
            if (word.Length == 0)
                GuessUntilNow.Add(' ');
            foreach (char c in word)
            {
                GuessUntilNow.Add(' ');
                WordState += c;
            }
            

        }

        [XmlAttribute]
        public string Username
        {
            get { return usernname; }
            set
            {
                usernname = value;
                OnPropertyChanged("Username");
            }
        }

        [XmlAttribute]
        public int Level
        {
            get { return level; }
            set
            {
                level = value;
                OnPropertyChanged("Level");
            }
        }

        [XmlAttribute]
        public int Lives
        {
            get { return lives; }
            set
            {
                if (value >= 0)
                    lives = value;
                OnPropertyChanged("Lives");
            }
        }

        [XmlAttribute]
        public string WordToGuess
        {
            get { return wordToGuess; }
            set
            {
                wordToGuess = value;
                OnPropertyChanged("WordToGuess");
            }
        }

        [XmlAttribute]
        public string Category
        {
            get { return category; }
            set
            {
                category = value;
                OnPropertyChanged("Category");
            }
        }

        [XmlAttribute]
        public string HangmanImage
        {
            get { return hangmanImage; }
            set
            {
                hangmanImage = value;
                OnPropertyChanged("HangmanImage");
            }
        }

        [XmlAttribute]
        public List<char> LettersLeft
        {
            get
            {
                return lettersLeft;
            }
            set
            {
                lettersLeft = value;
                OnPropertyChanged("LettersLeft");
            }
        }

        [XmlAttribute]
        public List<char> GuessUntilNow
        {
            get
            {
                return guessUntilNow;
            }

            set
            {
                guessUntilNow = value;
                OnPropertyChanged("GuessUntilNow");
            }
        }
        [XmlAttribute]
        public int SecondsRemaining
        {
            get
            {
                return secondsRemaining;
            }
            set
            {
                secondsRemaining = value;
                OnPropertyChanged("SecondsRemaining");
            }
        }

        [XmlAttribute]
        public string WordState
        {
            get
            {
                return wordState;
            }
            set
            {
                wordState = value;
                OnPropertyChanged("SecondsRemaining");
            }
        }
    }
}
