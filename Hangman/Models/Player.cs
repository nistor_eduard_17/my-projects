﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Hangman.Models
{
    [Serializable]
    public class Player : NotifyPropertyChanged
    {
        private string username;
        private string imagePath;
        public Player()
        {
            GameInfo = new ObservableCollection<GameInfo>();
        }
        public Player(string username)
        {
            Username = username;
            ImagePath = "/Hangman;component/Images/Avatar/avatar1.png";
        }

        [XmlElement]
        public string ImagePath
        {
            get { return imagePath; }

            set
            {
                imagePath = value;
                OnPropertyChanged("ImagePath");
            }

        }

        [XmlElement]
        public string Username
        {
            get { return username; }

            set
            {
                username = value;
                OnPropertyChanged("Username");
            }
        }
        [XmlElement]
        public ObservableCollection<GameInfo> GameInfo { get; set; }
    }
}
