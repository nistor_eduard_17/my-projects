﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Hangman.Models
{
    [Serializable]
    public class CategoryToSerialize
    {
        [XmlArray]
        public ObservableCollection<Category> ListOfCategory { get; set; }
    }
}
