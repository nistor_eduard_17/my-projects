﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Models
{
    internal class MyButtons : NotifyPropertyChanged
    {
        private char content;
        private bool isEnabled;
        public char Content
        {
            get => content; 
            set
            {
                content = value;
                OnPropertyChanged("Content");
            }
        }
        public bool IsEnabled
        {
            get => isEnabled;
            set
            {
                isEnabled = value;
                OnPropertyChanged("IsEnabled");
            }
        }
    }
}
