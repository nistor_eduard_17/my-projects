﻿using Hangman.Commands;
using Hangman.Models;
using Hangman.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Hangman.ViewModels
{
    internal class UserVM : NotifyPropertyChangedVM
    {
        public ObservableCollection<Player> Players { get; set; }
        public Player Player { get; set; }

        private Player selectedPlayer;
        private ReadWrite readWrite;
        private PlayersOperations operation;
        public UserVM()
        {
            Players = new ObservableCollection<Player>() { };
            Player = new Player("");
            readWrite = new ReadWrite();
            readWrite.DeserializeObject();
            Players = readWrite.players;

            operation = new PlayersOperations(this.Players);
        }
        public string Username
        {
            get
            {
                if (Player.Username == "")
                    CanExecuteCommand = false;
                return Player.Username;
            }
            set
            {
                if (value != null)
                    CanExecuteCommand = CanExecuteCommand = MyValidator.CanExecuteOperation(Players, value);
                Player.Username = value;
                OnPropertyChanged("Username");

            }
        }

        public Player SelectedPlayer
        {
            get
            {
                if (selectedPlayer == null)
                    CanExecuteSelectedCommand = false;
                return selectedPlayer;
            }
            set
            {
                selectedPlayer = value;
                CanExecuteSelectedCommand = true;
                OnPropertyChanged("SelectedPlayer");
            }
        }

        private bool canExecuteSelectedCommand = false;
        public bool CanExecuteSelectedCommand
        {
            get
            {
                return canExecuteSelectedCommand;
            }

            set
            {
                if (canExecuteSelectedCommand == value)
                {
                    return;
                }
                canExecuteSelectedCommand = value;
            }
        }

        private bool canExecuteCommand = false;
        public bool CanExecuteCommand
        {
            get
            {
                return canExecuteCommand;
            }

            set
            {
                if (canExecuteCommand == value)
                {
                    return;
                }
                canExecuteCommand = value;
            }
        }
        public string ImagePath
        {
            get
            {
                return Player.ImagePath;
            }
            set
            {
                Player.ImagePath = value;
                OnPropertyChanged("ImagePath");
            }
        }

        public ICommand addUserCommand;
        public ICommand AddUserCommand
        {
            get
            {
                if (addUserCommand == null)
                {
                    addUserCommand = new RelayCommand<Player>(operation.Add, param => CanExecuteCommand);
                }
                return addUserCommand;
            }
        }

        public ICommand deleteUserCommand;
        public ICommand DeleteUserCommand
        {
            get
            {
                if (deleteUserCommand == null)
                {
                    deleteUserCommand = new RelayCommand<string>(operation.Delete, param => true);
                }
                return deleteUserCommand;
            }
        }

        public ICommand previousPictureCommand;
        public ICommand PreviousPictureCommand
        {
            get
            {
                if (previousPictureCommand == null)
                {
                    previousPictureCommand = new RelayCommand<Player>(operation.PreviousPicture, param => true);
                }
                return previousPictureCommand;
            }
        }

        public ICommand nextPictureCommand;
        public ICommand NextPictureCommand
        {
            get
            {
                if (nextPictureCommand == null)
                {
                    nextPictureCommand = new RelayCommand<Player>(operation.NextPicture, param => true);
                }
                return nextPictureCommand;
            }
        }

        public ICommand selectedUserCommand;
        public ICommand SelectedUserCommand
        {
            get
            {
                if (selectedUserCommand == null)
                {
                    selectedUserCommand = new RelayCommand<Player>(operation.SelectPlayer, param => CanExecuteSelectedCommand);
                }
                return selectedUserCommand;
            }
        }

    }
}
