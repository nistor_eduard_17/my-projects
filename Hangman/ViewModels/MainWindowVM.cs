﻿using Hangman.Models;
using Hangman.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.ViewModels
{
    internal class MainWindowVM
    {
        public ObservableCollection<Player> Players { get; set; }
        private ReadWrite readWrite;
        public MainWindowVM()
        {
            Players = new ObservableCollection<Player>();
            readWrite = new ReadWrite();
            //readWrite.DeserializeObject();
        }
    }
}
