﻿using Hangman.Commands;
using Hangman.Models;
using Hangman.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Hangman.ViewModels
{
    internal class StatisticsVM
    {
        public ObservableCollection<Category> Categories { get; set; }
        public ObservableCollection<Player> Players;
        CreateStatistics statistics;
        private ReadWrite write;
        public StatisticsVM()
        {
            this.Categories = new ObservableCollection<Category>();
            statistics = new CreateStatistics(this.Categories);
            Players = statistics.players;
            write = new ReadWrite();
            foreach(Category categ in Categories)
            {
                
            }
            write.Write(Categories);
        }
    }
}
