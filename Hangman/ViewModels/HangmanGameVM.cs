﻿using Hangman.Models;
using Hangman.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Hangman.ViewModels
{
    internal class HangmanGameVM : NotifyPropertyChangedVM
    {
        public Player CurrentPlayer { get; set; }
        public Game Game { get; set; }
        public HangmanGameVM(Player player, Game game)
        {
            CurrentPlayer = player;
            Game = game;
        }

        public string Username
        {
            get
            {
                return CurrentPlayer.Username;
            }
            set
            {
                CurrentPlayer.Username = value;

            }
        }
        public string ImagePath
        {
            get
            {
                return CurrentPlayer.ImagePath;
            }
            set
            {
                CurrentPlayer.ImagePath = value;
            }
        }

        public string WordToGuess
        {
            get
            {
                return Game.WordToGuess;
            }
            set
            {
                Game.WordToGuess = value;
                OnPropertyChanged("WordToGuess");
            }
        }

        public int Lives
        {
            get
            {
                return Game.Lives;
            }
            set
            {
                Game.Lives = value;
                OnPropertyChanged("Lives");
            }
        }
        public string HangmanImage
        {
            get
            {
                return Game.HangmanImage;
            }
            set
            {
                Game.HangmanImage = value;
                OnPropertyChanged("HangmanImage");
            }
        }
    }
}
