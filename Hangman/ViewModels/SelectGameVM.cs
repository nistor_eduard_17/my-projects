﻿using Hangman.Models;
using Hangman.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.ViewModels
{
    public class SelectGameVM : NotifyPropertyChangedVM
    {
        public ObservableCollection<GameInfo> Games { get; set; }
        public ObservableCollection<GameInfo> AllGames { get; set; }
        ReadWrite readWrite;
        public SelectGameVM(Player player)
        {
            readWrite = new ReadWrite();
            readWrite.DeserializeObject();
            AllGames = readWrite.players[0].GameInfo;
            Games = new ObservableCollection<GameInfo>();
            foreach (GameInfo gameInfo in AllGames)
                if (gameInfo.Lives != 0 && gameInfo.Level <= 5 && gameInfo.SecondsRemaining != 0)
                    Games.Add(gameInfo);
        }

        public void AddNewGame(GameInfo gameInfo)
        {
            this.AllGames.Add(gameInfo);
            Games.Add(gameInfo);

        }
    }
}
